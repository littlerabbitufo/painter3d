﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateFollowMovement : MonoBehaviour
{
    public Transform picker;

    private Vector3 prePos;

    private float timeToThrow;
    // Start is called before the first frame update
    void Start()
    {
        prePos = picker.position;        
        ThrowObject.OnCrack += OnThrow;
    }

    void OnThrow(ThrowObject obj)
    {
        picker.rotation = Quaternion.identity;
        //timeToThrow = 3f;
    }

    // Update is called once per frame
    void Update()
    {
        if(picker.GetComponent<Character>().enabled)
        {
            Vector3 currentForward = picker.position - prePos;
            prePos = picker.position;
            picker.forward = Vector3.Lerp(picker.forward, currentForward, Time.deltaTime * 5f);
        }
    }
}
