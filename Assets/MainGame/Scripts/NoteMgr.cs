﻿using System;
using System.Collections;
using System.Collections.Generic;
using Amanotes.Data;
using UnityEngine;

[System.Serializable]
public class Pattern
{
    public PoolObject prefab;
    //public Transform positions;
    Queue<PoolObject> available = new Queue<PoolObject>();
    List<PoolObject> objects = new List<PoolObject>();

    public void Init()
    {
        // for (int i = 0; i < positions.childCount; i++)
        // {
        //     var t = positions.GetChild(i);
        //     t.gameObject.SetActive(true);
        // }
    }

    public GameObject SpawnById(int id)
    {
        var obj = available.Count > 0 ? available.Dequeue() : null;
        if (obj == null)
        {
            obj = GameObject.Instantiate<PoolObject>(prefab);
            obj.name = prefab.name;
        }

        //obj.Set(positions.GetChild(id));

        return obj.gameObject;
    }

    public GameObject SpawnRandom(Vector3 delta, Vector3 rot)
    {
        var obj = available.Count > 0 ? available.Dequeue() : null;
        if (obj == null)
        {
            obj = GameObject.Instantiate<PoolObject>(prefab);
            objects.Add(obj);
            obj.name = prefab.name;
        }

        // positions.position = delta;
        // positions.eulerAngles = rot;
        obj.Set(delta, Quaternion.Euler(rot));
        //transList.RemoveAt(id);
        return obj.gameObject;
    }

    public void Return(PoolObject obj)
    {
        available.Enqueue(obj);
        //Transform holder = obj.holder;
        //holder.gameObject.SetActive(true);
        //Debug.Log("Obj return " + obj.name);
    }

    public void ReturnAll()
    {
        foreach (var obj in objects)
            if (obj != null && obj.isActiveAndEnabled)
            {
                obj.gameObject.SetActive(false);
                Return(obj);
            }
    }
}

public class NoteMgr : MonoBehaviour
{
    [SerializeField] float _boundX = 12f;
    [SerializeField] float _timeStep = 12f;

    public Pattern[] patterns;
    //public float X;
    public Vector2 bombThreshold;
    public Vector2 throwConfig;
    List<NoteData> commands;
    List<Transform> envs;
    NoteData currentCmd;
    Character player;
    float freqSample;
    float samplesDiv;
    int id;
    int milestoneId;
    int fixedId;
    List<Vector3> milestone;
    AudioSource audioSource;
    DrawTexture drawTexture;
    //float point_x = 0;
    internal static event Action<float> OnProgress;
    internal static event Action OnWin;
    internal static event Action OnLose;
    internal static event Action OnReset;

    float _previousXCube = 0f;
    float _previousCubeTime = 0f;
    float audioTime = 0f;
    int live;

    // Use this for initialization
    void Awake()
    {
        enabled = false;

        foreach (Pattern p in patterns) p.Init();
        player = FindObjectOfType<Character>();
        drawTexture = FindObjectOfType<DrawTexture>();
        PoolObject.OnReturn += ReturnObject;

        envs = new List<Transform>();
        milestone = new List<Vector3>();
        var plane = GetPattern("plane");
        var tunel = GetPattern("tunel");
        for (int i = 0; i < 5; i++)
        {
            var obj = plane.SpawnRandom(Vector3.zero, Vector3.zero);
            envs.Add(obj.transform);
            obj = tunel.SpawnRandom(Vector3.zero, Vector3.zero);
            envs.Add(obj.transform);
        }

        CrackedObject.OnCrack += OnCracked;
        ThrowObject.OnCrack += OnCracked;
        DeathTrigger.OnCrack += OnDeath;
    }

    private void OnDeath(DeathTrigger obj)
    {
        if (player.IsInvincible()) return;

        live--;
        if (live <= 0)
        {
            player.enabled = false;
            audioTime = audioSource.time;
            Invoke("FinishAnim", 1);
        }
        else
        {
            player.OnObstacle(live);
        }
    }

    public void SetNotes(List<NoteData> notes, AudioSource audio)
    {
        commands = notes;
        if (audio) audioSource = audio;
        StartCoroutine(player.Revive());

        var length = audioSource.clip.length * .2f;
        var ms = new Vector3(0, player.pos.x, 0);
        milestone.Clear();
        milestone.Add(ms);

        var a = length - throwConfig.x;

        for (int i = 0; i < notes.Count; i++)
        {
            var n = notes[i];
            if (n.timeAppear > a)
            {
                if (ms.y == player.pos.x)
                {
                    if (i < notes.Count - 1) n = notes[i - 1];
                    ms.z += (n.timeAppear - ms.x) * ms.y;
                    ms.x = n.timeAppear;

                    ms.y = player.pos.y;
                    a += throwConfig.x;
                }
                else
                {
                    ms.z += (n.timeAppear - ms.x) * ms.y;
                    ms.x = n.timeAppear;

                    ms.y = player.pos.x;
                    a += length - throwConfig.x;

                    if (a > length * 4) a = notes[notes.Count - 1].timeAppear - 0.01f;
                }
                milestone.Add(ms);
            }
        }

        milestone.Add(new Vector3(10000, 0, 5000));
        ms = Vector3.zero;
        for (int i = 0; i < envs.Count; i++)
        {
            ms.x = milestone[i].z;
            ms.y = milestone[i + 1].z - (i < envs.Count - 1 ? ms.x : 0);
            envs[i].position = new Vector3(0, 0, ms.x);
            envs[i].localScale = new Vector3(30, 1, ms.y);
        }

        //cheat
        if (audio)
        {
            envs[0].GetComponent<LoopPlane>().Crack();
            drawTexture.Init(notes.Count);
        }

        live = 3;
        player.OnObstacle(live);
    }

    public void Init(int i)
    {
        if (i == 0)
        {
            id = 0;
            currentCmd = commands[id++];

            fixedId = 0;
            milestoneId = 0;
            //point_x = 0;
            audioTime = 0f;
        }

        audioSource.time = audioTime;
        audioSource.Play();

        var clip = audioSource.clip;
        freqSample = 1f / clip.frequency;
        samplesDiv = 1f / clip.samples;

        player.enabled = true;
        enabled = true;
    }

    public int GetNoteIndex()
    {
        return fixedId;
    }

    public float GetPercentCompleted()
    {
        return audioSource.timeSamples * samplesDiv;
    }

    Pattern GetPattern(string name)
    {
        foreach (Pattern p in patterns)
            if (p.prefab.name == name) return p;

        return null;
    }

    void OnDisable()
    {
        if (player)
        {
            this._previousCubeTime = 0f;
            this._previousXCube = 0f;

            OnReset();
            foreach (Pattern p in patterns) p.ReturnAll();
        }
    }

    void FinishAnim()
    {
        OnLose();
    }

    public void ReturnObject(PoolObject obj)
    {
        GetPattern(obj.name).Return(obj);
        //AddScore(1, 0, 0);
    }

    void OnCracked(PoolObject co)
    {
        fixedId++;
    }

    public void ExtraBall(Transform trans, Material mat, bool safeZone)
    {
        var pattern = GetPattern("Extra");
        int ballNum = UnityEngine.Random.Range(4, trans.childCount);
        for (int i = 1; i < ballNum; i++)
        {
            var pos = trans.GetChild(i).position;
            if (pos.x > _boundX || pos.x < -_boundX) continue;
            var obj = pattern.SpawnRandom(trans.GetChild(i).position, Vector3.zero);
            obj.GetComponentInChildren<MeshRenderer>().material.CopyPropertiesFromMaterial(mat);
        }

        if (!safeZone) return;

        var death = GetPattern("death");
        var p = trans.position;
        var l = Mathf.Max(p.x + 15f - 11f, 0);
        var r = Mathf.Max(15f - p.x - 11f, 0);
        if (l > 3)
        {
            var left = death.SpawnRandom(Vector3.zero, Vector3.zero).transform;
            var scale = left.localScale;
            scale.x = l;
            left.localScale = scale;
            left.position = new Vector3(-15f + l * .5f, p.y, p.z);
        }
        if (r > 3)
        {
            var right = death.SpawnRandom(Vector3.zero, Vector3.zero).transform;
            var scale = right.localScale;
            scale.x = r;
            right.localScale = scale;
            right.position = new Vector3(15f - r * .5f, p.y, p.z);
        }
    }

    public float getMilestoneDelay(int id)
    {
        return milestone[milestoneId].x - audioSource.timeSamples * freqSample;
    }

    // Update is called once per frame
    void Update()
    {
        float time = audioSource.timeSamples * freqSample + bombThreshold.x;
        while (time > currentCmd.timeAppear)
        {
            var ms = milestone[milestoneId];
            var next_ms = milestone[milestoneId + 1];
            var isCube = ms.y == player.pos.x;
            if (!isCube || currentCmd.timeAppear > ms.x + 1f)
            {
                float newPosX = 0f;
                if (isCube && currentCmd.timeAppear > 2f)
                {
                    if (this._previousCubeTime == 0)
                    {
                        newPosX = UnityEngine.Random.Range(-this._boundX, this._boundX);
                    }
                    else
                    {
                        float timeDuration = time - this._previousCubeTime;
                        float sign = this._previousXCube > 0f ? -1 : 1;
                        newPosX = this._previousXCube + (sign * timeDuration * this._timeStep);

                        newPosX = Mathf.Clamp(newPosX, -this._boundX, this._boundX);
                    }
                    this._previousCubeTime = currentCmd.timeAppear;
                    this._previousXCube = newPosX;
                }

                var obj = GetPattern(isCube ? "Cube" : "Throw").SpawnRandom(
                    new Vector3(isCube ? newPosX : 0,
                    2f, (currentCmd.timeAppear - ms.x + 0.01f) * ms.y + ms.z),
                    Vector3.zero).transform;

                if (isCube)
                {
                    var mat = obj.GetComponentInChildren<MeshRenderer>().material;
                    drawTexture.SetUp(mat);
                    //obj.GetComponent<CrackedObject>().Init();
                    ExtraBall(obj, mat, currentCmd.timeAppear < next_ms.x - 1f);
                }
                else drawTexture.DrawRandom();
            }
            else drawTexture.DrawRandom();
            // else
            // {
            //     Vector3 newPos = this.drawTexture.transform.position;
            //     newPos.z = ms.z + 16f;

            //     this.drawTexture.transform.position = newPos;
            // }
            if (id >= commands.Count)
            {
                currentCmd = new NoteData() { timeAppear = 10000 };
                return;
            }
            else currentCmd = commands[id++];

            if (currentCmd.timeAppear >= next_ms.x)
            {
                milestoneId++;
                //Debug.Log(next_ms);
            }
        }

        // var t = point_x + Time.deltaTime * throwConfig.y;
        // if (Mathf.Abs(t) > X)
        // {
        //     t = point_x;
        //     throwConfig.y *= -1f;
        // }

        // point_x = t;

        var pc = GetPercentCompleted();
        OnProgress(pc);
        //if (pc > .999f) OnWin();
    }

    static public void OnFinishEndingAnim()
    {
        OnWin();
    }
}
