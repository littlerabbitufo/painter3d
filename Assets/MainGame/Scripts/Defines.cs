﻿using UnityEngine;
using System.Collections.Generic;

namespace Defines
{
	static class Config
	{
		public static FirebaseConfig Firebase = new FirebaseConfig();
		public static FirebaseConfig FirebaseDefault = new FirebaseConfig();

		//AudioDelayTime
		#if UNITY_EDITOR
			public static float AudioDelayTime = 0f;
		#elif UNITY_ANDROID
			public static float AudioDelayTime = 0.05f;
		#elif UNITY_IOS
			public static float AudioDelayTime = 0f;
		#else
			public static float AudioDelayTime = 0f;
		#endif

		public static float NoteAppearTime = 2;
		public static float NoteHideTime = 1;
		public static float NoteAppearDistance = 50;
		public static bool IsTestBuild = false;
	}

    [System.Serializable]
	public class SongInfo
	{
		public string Id;
		public string Name;
		public string Path;
		public bool Locked;
		public bool Completed;
		public int Score = 0;
		public int Star = 0;
		public int Percent = 0;
		public int Diamond = 0;
		public string Tag = "hidden";

	}

	[System.Serializable]
	public class SongData
	{
        public List<SongInfo> List;
	}
}


