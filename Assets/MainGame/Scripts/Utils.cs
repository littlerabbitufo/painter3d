﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utils
{
	public static float MoveToTarget(float value, float valueTarget, float speed, float dt)
	{
		speed = Mathf.Abs(speed);
		return (value < valueTarget)? (Mathf.Min(value + speed*dt, valueTarget)) : ((value > valueTarget)? (Mathf.Max(value - speed*dt, valueTarget)) : (value));
	}

	public static float GetXByInputPosition(Transform transform, Vector3 inputPosition)
	{
		//
		// Debug.Log("-----");
		// Debug.Log(Screen.width);
		// Debug.Log(Screen.height);
		// Debug.Log(inputPosition);

		Ray ray = Camera.main.ScreenPointToRay(inputPosition);
		// Debug.Log("-----");
		// Debug.Log(ray.origin);
		// Debug.Log(ray.direction);

		//ngoai suy targetX =]]
		Vector3 p1 = ray.origin;
		Vector3 p2 = ray.origin + ray.direction;
		return (transform.position.z - p1.z)/(p2.z - p1.z)*(p2.x - p1.x) - p1.x;
	}

	public static float Bezier(float t, float[] array, int i1, int i2)
	{
		var length = i2 - i1 + 1;
		if(length > 2)
			return (1 - t)*Bezier(t, array, i1, i2-1) + t*Bezier(t, array, i1+1, i2);
		else if(length >= 2)
			return (1 - t)*array[i1] + t*array[i2];
		else if(length >= 1)
			return array[i1];
		else
			return 0f;
	}

	public static float Bezier(float t, float[] array, int i1)
	{
		return Bezier(t, array, i1, array.Length - 1);
	}

	public static float Bezier(float t, float[] array)
	{
		return Bezier(t, array, 0, array.Length - 1);
	}

	public static Color HexToColor(string hex)
    {
        Color color;
        ColorUtility.TryParseHtmlString(hex, out color);

        return color;
    }
}
