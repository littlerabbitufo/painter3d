﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Amanotes.Data;
using Defines;

public class NoteBase : MonoBehaviour
{
    enum STATE
    {
        IDLE,
        WAITING,
        MOVE,
        HIDE,
        DEAD,
    }

    public delegate void OnBeat(NoteBase note);
    public static event OnBeat onBeat;

    STATE mState;
    NoteData mNoteData;
    Timer mTimer = new Timer();

    void Start()
    {

    }

    void Update()
    {
        float deltaTime = Time.deltaTime;
        switch (mState)
        {
            case STATE.IDLE:
                break;

            case STATE.WAITING:
                mTimer.Update(deltaTime);
                if (mTimer.GetTime() <= Config.NoteAppearTime)
                {
                    SetState(STATE.MOVE);
                }
                break;
            case STATE.MOVE:
                mTimer.Update(deltaTime);
                if (!mTimer.IsDone())
                {
                    UpdatePosition(mTimer.GetTimePercent());
                }
                else
                {
                    if (onBeat != null)
                    {
                        onBeat(this);
                    }
                    SetState(STATE.HIDE);
                }
                break;

            case STATE.HIDE:
                mTimer.Update(deltaTime);
                if (mTimer.IsDone())
                {
                    SetState(STATE.DEAD);
                }
                break;

            case STATE.DEAD:
                break;

        }
    }

    void SetState(STATE state)
    {
        float deltaTime = Time.deltaTime;

        mState = state;
        switch (mState)
        {
            case STATE.IDLE:
                break;

            case STATE.WAITING:
                mTimer.SetDuration(Config.NoteAppearTime);
                UpdatePosition(1);
                break;

            case STATE.MOVE:
                mTimer.SetDuration(Config.NoteAppearTime - mTimer.GetOverhead());
                break;

            case STATE.HIDE:
                mTimer.SetDuration(Config.NoteHideTime - mTimer.GetOverhead());
                break;
        }
    }

    void UpdatePosition(float percent)
    {
        transform.position = new Vector3(0, transform.position.y, Config.NoteAppearDistance * percent);
    }

    public void Spawn(NoteData noteData)
    {
        mNoteData = noteData;
        SetState(STATE.WAITING);
    }

    public void Dead()
    {
        SetState(STATE.DEAD);
    }

    public bool IsDead()
    {
        return mState == STATE.DEAD;
    }

    public NoteData GetNoteData()
    {
        return mNoteData;
    }
}
