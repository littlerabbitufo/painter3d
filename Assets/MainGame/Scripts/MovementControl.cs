﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementControl : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            var halfWidth = Screen.width * .5f;
            var X = 10f;
            var x = (Input.mousePosition.x - halfWidth) / halfWidth * X;
            if (x < -X)
            {
                x = -X;
            }
            else if (x > X)
            {
                x = X;
            }
            transform.position = new Vector3(x, transform.position.y, transform.position.z);
        }
    }
}
