namespace Defines
{
    public class ConfigAds
    {
        public float DelayBeforeFirstFS = 30;
        public float DelayBetweenFS = 30;
        public float MaxGamesBetweenFS = 3;
        public float DelayBetweenRVAndFS = 15;
        public int MaxFailBetweenFS = 1;
        public bool FreeFSWhenUnlockBall = false;
    }

    public class ConfigDiamond
    {
        public int VideoEarning = 20;
        public int Multiply = 5;
        public int LevelEarningMin = 8;
        public int LevelEarningMax = 12;
    }

    public class ConfigGift
    {
        public int ProgressCount = 4;
    }

    public class ConfigLife
    {
        public int Max = 3;
        public int ComboBonus = 9999;
        public float DelayBonus = 5;
    }

	public class ConfigSuggestSong
	{
		public string Order = "video_new,video,diamond_new,diamond,uncomplete";
	}

    public class ConfigDifficult
    {
        public float BallFlyTimeDecreaseRate = 0f;
        public float BallAlignmentDuration = 0.4f;
        public float BallFlyTimeMin = 1.5f;
        public float BallFlyTimeMax = 2f;
        public float BasketScale = 1.1f;
        public float DiamondFlyTime = 2f;
        public float SkipThrowBallDuration = 1f;
        public bool EndlessMode = false;
        public float EndlessBallFlyTimeDecreaseRate = 0.5f;
        public float EndlessAudioPitchIncreaseRate = 0.2f;
    }

    public class ConfigIAP
    {
        public int DiamondSmall = 120;
        public int DiamondMedium = 250;
        public int DiamondLarge = 400;
        public int DiamondMaximum = 800;
    }

    public class ConfigObstacle
    {
        public int Max = 10;//number of obstacle in 1 game
        public float ThrowDuration = 0.35f;//only throw obstacle at note has duration >= this value
        //
        public float SkipPercent = 0.2f;//(0->1) percent of total note will be skiped
        public int SkipNote = 5;//number of note will be skiped
        public float PercentDurationOffset = 0.6f;//(0->1) to control time appear of obstacle compare to the previous & next ball
        public float OffsetXMin = 1.5f;//offset min of obstacle with ball
        public float OffsetXMax = 3f;//offset max of obstacle with ball
    }

    public class ConfigChallenge
    {
        public float BallAlignmentDuration = 0.1f;
        public float BallFlyTimeMin = 1f;
        public float BallFlyTimeMax = 1.5f;
        public float DiamondFlyTime = 1.5f;
        public float BasketScale = 0.85f;
        public int BombMax = 2;
        public int DiamondBonusMax = 10;
        public int TimeCountDown = 3600;
    }

    public class ConfigLuckyEvent
    {
        public int ImportSongCount = 2;
        public int StartSongCount = 4;
        public int[] DiamondPercent = new int[9]{
            100,
            95, 95,
            75, 75, 75,
            50, 50,
            0
        };
    }

    public class GeneralConfig
    {
        public bool UsePauseButton = true;
        public bool UsePauseBackKey = true;
        public bool UseAutoSearch = true;
        public bool UseImportSongFreeTrial = true;
    }

	public class FirebaseConfig
	{
		public ConfigAds Ads = new ConfigAds();
		public ConfigDiamond Diamond = new ConfigDiamond();
		public ConfigGift Gift = new ConfigGift();
		public ConfigLife Life = new ConfigLife();
		public ConfigSuggestSong SuggestSong = new ConfigSuggestSong();
        public ConfigDifficult Difficult = new ConfigDifficult();
        public ConfigIAP IAP = new ConfigIAP();
        public ConfigObstacle Obstacle = new ConfigObstacle();
        public ConfigChallenge Challenge = new ConfigChallenge();
        public ConfigLuckyEvent LuckyEvent = new ConfigLuckyEvent();
        public GeneralConfig General = new GeneralConfig();
	}
}