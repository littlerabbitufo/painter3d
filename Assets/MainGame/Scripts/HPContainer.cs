﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class HPContainer : MonoBehaviour
{
    [SerializeField] List<Image> HeartsList;
    [SerializeField] Image RedSplashImg;
    int _totalHearts = 3;

    void Awake()
    {
        this._totalHearts = HeartsList.Count;
    }

    public void OnHeartUpdate(int live)
    {
        if(live == _totalHearts)
        {
            HeartsList.ForEach( x => { 
                Color defaultColor = x.color;
                defaultColor.a = 1f;
                x.color = defaultColor;

                x.transform.localScale = Vector3.one;
            });

            return;
        }

        Image heart = HeartsList[live];
        Tween scaleTween = heart.rectTransform.DOScale(Vector3.one * 1.2f, 0.5f);
        scaleTween.onComplete = ()=>{ heart.rectTransform.localScale = Vector3.one; };
        Color color = heart.color;
        color.a = 0.2f;
        heart.DOColor(color, 0.2f);

        Color redColor = RedSplashImg.color;
        redColor.a = 0.235f;
        Tween redSplashTween = RedSplashImg.DOColor(redColor, 0.25f).SetEase(Ease.InBounce);
        redSplashTween.onComplete = ()=>{
            redColor.a = 0f;
            RedSplashImg.DOColor(redColor, 0.25f).SetEase(Ease.InBounce);
        };
    }
}
