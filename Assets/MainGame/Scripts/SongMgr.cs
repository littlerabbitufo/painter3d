﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding.Serialization.JsonFx;
using Defines;

public class SongMgr : MonoBehaviour
{
    private List<SongInfo> SongList;
    private List<SongInfo> SongListDefault;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadSong(string data)
    {
        SongData songData = JsonReader.Deserialize<SongData>(data);
        SongList = songData.List;

        SongData songDataDefault = JsonReader.Deserialize<SongData>(data);
        SongListDefault = songDataDefault.List;
    }

    public void SetSong(int index, SongInfo song)
    {
        SongList[index].Locked = song.Locked;
        SongList[index].Score = song.Score;
        SongList[index].Star = song.Star;
        SongList[index].Percent = song.Percent;
        SongList[index].Completed = song.Completed;
        if (song.Tag != "")
        {
            SongList[index].Tag = song.Tag;
        }
    }

    public SongInfo GetSong(int index)
    {
        return SongList[index];
    }

    public List<SongInfo> GetSongs()
    {
        return SongList;
    }

    public SongInfo GetSongDefaultById(string id)
    {
        for (int i = 0; i < SongListDefault.Count; i++)
        {
            if (SongListDefault[i].Id == id)
            {
                return SongListDefault[i];
            }
        }
        return null;
    }

    public int GetSongIndexById(string id)
    {
        for (int i = 0; i < SongList.Count; i++)
        {
            if (SongList[i].Id == id)
            {
                return i;
            }
        }

        return 0;
    }

    public SongInfo GetSuggestSongs(SongInfo songInfo)
    {
        string[] orderList = Config.Firebase.SuggestSong.Order.Split(',');
		List<SongInfo> suggestSongList = new List<SongInfo>();
        List<SongInfo> result = new List<SongInfo>();

        for (int i = 0; i < SongList.Count; i++)
        {
            suggestSongList.Add(SongList[i]);
        }

        for (int i = 0; i < orderList.Length; i++)
        {
            if (orderList[i] == "video_new")
            {
                for (int j = 0; j < suggestSongList.Count; j++)
                {
                    if (suggestSongList[j].Locked && suggestSongList[j].Diamond == 0 && suggestSongList[j].Tag.ToLower() == "new")
                    {
                        return suggestSongList[j];
                    }
                }
            }
            else if (orderList[i] == "diamond_new")
            {
                for (int j = 0; j < suggestSongList.Count; j++)
                {
                    if (suggestSongList[j].Locked && suggestSongList[j].Diamond > 0 && suggestSongList[j].Tag.ToLower() == "new")
                    {
                        return suggestSongList[j];
                    }
                }
            }
            else if (orderList[i] == "video")
            {
                for (int j = 0; j < suggestSongList.Count; j++)
                {
                    if (suggestSongList[j].Locked && suggestSongList[j].Diamond == 0 && suggestSongList[j].Tag.ToLower() != "new")
                    {
                        return suggestSongList[j];
                    }
                }
            }
            else if (orderList[i] == "diamond")
            {
                for (int j = 0; j < suggestSongList.Count; j++)
                {
                    if (suggestSongList[j].Locked && suggestSongList[j].Diamond > 0 && suggestSongList[j].Tag.ToLower() != "new")
                    {
                        return suggestSongList[j];
                    }
                }
            }
            else if (orderList[i] == "uncomplete")
            {
                for (int j = 0; j < suggestSongList.Count; j++)
                {
                    if (!suggestSongList[j].Locked && !suggestSongList[j].Completed)
                    {
                        return suggestSongList[j];
                    }
                }
            }
        }

        return SongList[UnityEngine.Random.Range(0, SongList.Count)];
    }

    public int GetCompleteSongCount()
    {
        int count = 0;
        for (int i = 0; i < SongList.Count; i++)
        {
            if (SongList[i].Completed)
            {
                count++;
            }
        }

        return count;
    }
}
