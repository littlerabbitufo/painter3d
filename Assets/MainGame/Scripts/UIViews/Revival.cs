﻿
using UnityEngine;
using UnityEngine.UI;
using Doozy.Engine.Progress;
using Doozy.Engine.UI;
using DG.Tweening;
using Amanotes.Utils;
using Defines;

public class Revival : MonoBehaviour
{

	// Use this for initialization
	[SerializeField] private Progressor ProgressBar;
	[SerializeField] private Text TextPercent;
	[SerializeField] private Progressor ProgressCountdown;
	[SerializeField] private UIButton ButtonWatchAds;
	[SerializeField] private UIButton ButtonNoThanks;

	Timer mTimerContinue;
	SongInfo mSongInfo;
	float mProgressValue = 0;
	bool mIsStarted = false;

	void Start()
	{

	}

	void OnEnable()
	{
		if (!mIsStarted)
		{
			mIsStarted = true;
			return;
		}

		if (UserPropertiesTracker.Instance)
        {
            UserPropertiesTracker.Instance.AddScreen("Revival");
			if (IsShowContinue())
			{
				ProgressCountdown.gameObject.SetActive(true);
				ButtonWatchAds.gameObject.SetActive(true);
				ButtonNoThanks.gameObject.SetActive(false);

				mTimerContinue = new Timer();
				mTimerContinue.SetDuration(3);

				if (mSongInfo != null)
				{
					GameMgr.Instance.TrackingRevivalImpression(mSongInfo, "you_lose");
				}
			}
			else
			{
				ProgressCountdown.gameObject.SetActive(false);
				ButtonWatchAds.gameObject.SetActive(false);
				ButtonNoThanks.gameObject.SetActive(true);

				mTimerContinue = null;
			}
        }
		// ProgressBar.SetValue(mProgressValue);
	}

	void OnDisable()
	{

	}

	// Update is called once per frame
	void Update()
	{
		if (mTimerContinue != null)
		{
			float percent = mTimerContinue.GetTimePercent();

			mTimerContinue.Update(Time.deltaTime);
			ProgressCountdown.SetValue(percent * ProgressCountdown.MaxValue);
			if (mTimerContinue.JustFinished())
			{
				ButtonNoThanks.gameObject.SetActive(true);
			}
		}
	}

	public void SetInfo(SongInfo songInfo, float percent, int score)
	{
		if (percent < 0)
		{
			percent = 0;
		}

		DOTween.To(() => 0, value =>
		{
			ProgressBar.SetValue(value);
			TextPercent.text = (Mathf.Floor(value * 100f) + "% complete").ToUpper();
		}, percent, 3);

		mSongInfo = songInfo;
	}


	bool IsShowContinue()
	{
		return (AdHelper.Instance.RewardVideoAvailable);
	}
}
