﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Doozy.Engine.UI;
using DG.Tweening;
using Defines;


public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject SongPrefab;
    [SerializeField] private GameObject Privacy;
    [SerializeField] private AudioSource MainAudio;
    [SerializeField] private GameObject Disk;
    [SerializeField] private GameObject DiskLight;
    [SerializeField] private ParticleSystem Particle;
    [SerializeField] private UIButton ButtonQuickPlay;
    [SerializeField] private Text QuickPlayTitle;
    [SerializeField] private Transform ScrollView;

    private List<GameObject> mObjectSongs;
    private SelectSong mSongCurrent;
    private float m_AverageMin = 9999;
    private float m_AverageMax = 0;
    private int mDiamondDisplay;
    private int mCheatIndex = 0;
    bool mIsStarted = false;

    void Awake()
    {
        SelectSong.onPlay += OnPlay;
        SelectSong.onSelect += OnSelect;
        SelectSong.onListen += OnListen;
    }

    void Start()
    {
        if (GameMgr.Instance.Profiles.IsFirstLaunch)
        {
            GameMgr.Instance.Profiles.IsFirstLaunch = false;
            UnLockSong(0);
        }

        ButtonQuickPlay.OnClick.OnTrigger.Event.AddListener(() =>
        {
            if (mSongCurrent != null)
            {
                OnPlay(mSongCurrent);
            }
        });

        GetComponent<LocalNotification>().AddNotification(GameMgr.Instance.GetNotificationSong());
    }

    void OnEnable()
    {
        if (!mIsStarted)
        {
            return;
        }

        if (UserPropertiesTracker.Instance)
        {
            UserPropertiesTracker.Instance.AddScreen("MainMenu");
        }

        int index = GameMgr.Instance.GetLastSongIndex();
        if (!GameMgr.Instance.Profiles.IsFirstLaunch)
        {
            mObjectSongs[index].GetComponent<SelectSong>().Listen();
        }
        QuickPlayTitle.text = mObjectSongs[index].GetComponent<SelectSong>().GetSongInfo().Name.ToUpper();
    }

    void OnDisable()
    {
    }

    void Update()
    {
        UpdateEffect();
    }

    public void CreateSongList(List<SongInfo> songs)
    {
        if (!mIsStarted)
        {
            mObjectSongs = new List<GameObject>();

            int i = 0;
            foreach (SongInfo songInfo in songs)
            {
                int index = i;
                GameObject songObject = Instantiate(SongPrefab, ScrollView);
                SelectSong song = songObject.GetComponent<SelectSong>();

                song.SetSongInfo(songInfo);
                if (GameMgr.Instance.Profiles.IsSongLocked(index))
                {
                    song.Lock();
                }
                else
                {
                    song.UnLock();
                }

                mObjectSongs.Add(songObject);
                i++;
            }

            //Instantiate(Privacy, ScrollView);
            mIsStarted = true;
        }
    }

	public void TagSelected(int index)
    {
        SelectSong song = mObjectSongs[index].GetComponent<SelectSong>();
        if (song.GetSongInfo().Tag.ToLower() == "new")
        {
            // GameMgr.Instance.Profiles.TagSelected(index);
            song.SetTag("hidden");
        }
    }

    public void UnLockSong(int index)
    {
        SelectSong song = mObjectSongs[index].GetComponent<SelectSong>();
        song.UnLock();

        GameMgr.Instance.Profiles.UnlockSong(song.GetSongInfo());
        // GameMgr.Instance.SetSongIndex(index);

        GameEventMgr.SendEvent("select_song");
    }

    public void RefreshSongProgressInfo(int index)
    {
        SelectSong song = mObjectSongs[index].GetComponent<SelectSong>();
        SongInfo songInfo = song.GetSongInfo();

        song.SetStar(songInfo.Star);
    }

    void UpdateEffect()
	{
		float[] spectrumData = new float[512];
		float average = 0;

        MainAudio.GetSpectrumData(spectrumData, 0, FFTWindow.Blackman);
		foreach (float value in spectrumData)
		{
			average += value;
		}
		average /= spectrumData.Length;
        average *= 1000;

        if (average < m_AverageMin) m_AverageMin = average;
        if (average > m_AverageMax) m_AverageMax = average;

        if (average > 0.1)
        {
            float range = m_AverageMax - m_AverageMin;
            if (range == 0) range = 1;
            float scale = 0.9f + (average / range) * 0.5f;

            Disk.transform.Rotate(0, 0, -1f);
		    DiskLight.transform.localScale = new Vector3(scale, scale, 1);
        }
        else
        {
            Disk.transform.Rotate(0, 0, 0);
            DiskLight.transform.localScale = new Vector3(1, 1, 1);
        }
	}
    void OnPlay(SelectSong selectSong)
    {
        SongInfo songInfo = selectSong.GetSongInfo();
        GameMgr.Instance.SetSong(songInfo);
        selectSong.Play("Main Menu");
    }

    void OnSelect(SelectSong selectSong)
    {
        // if (!selectSong.GetSongInfo().Locked)
        // {
        //     objectSong.transform.DOScale(new Vector3(0.8f, 0.8f, 1), 0.15f).SetEase(Ease.InSine).OnComplete(() =>
        //     {
        //         objectSong.transform.DOScale(new Vector3(0.85f, 0.85f, 1), 0.15f).SetEase(Ease.InSine).OnComplete(() =>
        //         {
        //             Play(objectSong);
        //         });
        //     });
        // }
    }

    void OnListen(SelectSong selectSong)
    {
        SongInfo songInfo = selectSong.GetSongInfo();
        string mp3Path = songInfo.Path + ".mp3";

        StreamingAssetLoader.Instance.LoadAudioClip(
            mp3Path, "Audios",
            progress =>
            {

            },
            audioClip =>
            {
                MainAudio.Stop();
                if (MainAudio.clip != null)
                {
                    MainAudio.clip.UnloadAudioData();
                }

                MainAudio.time = 0;
                MainAudio.clip = audioClip;
                MainAudio.loop = true;
                MainAudio.Play();

                Particle.Play();

                mSongCurrent = selectSong;
            }
        );

        m_AverageMin = 9999;
        m_AverageMax = 0;
    }
}
