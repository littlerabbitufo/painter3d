﻿using UnityEngine;
using Doozy.Engine.Progress;
using DG.Tweening;

public class IngameProgress : MonoBehaviour
{
    [SerializeField] private Progressor ProgessBar;
    [SerializeField] private ParticleSystem Flyer;
    [SerializeField] private ParticleSystem[] Stars;

    int mStar = 0;
    float mProgressValue;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (mProgressValue >= 0)
        {
            float currentY = 500f * mProgressValue - 250f;
            ProgessBar.SetValue(mProgressValue);
            Flyer.transform.localPosition = new Vector3(0f, currentY, 0f);

            while (mProgressValue >= (float)(0.4f + mStar * 0.3f))
            {
                Stars[mStar++].Play();
                GetComponent<AudioSource>().Play();
                if (mStar == Stars.Length)
                {
                    break;
                }
            }
            mProgressValue = -1;
        }
    }

    public void SetProgress(float value)
    {
        if (value == 0)
        {
            mStar = 0;
        }
        mProgressValue = value;
    }

    public int GetStars()
    {
        return mStar;
    }
}
