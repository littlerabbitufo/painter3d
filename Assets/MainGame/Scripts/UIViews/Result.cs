﻿using UnityEngine;
using UnityEngine.UI;
using Doozy.Engine.UI;
using DG.Tweening;
using Defines;

public class Result : MonoBehaviour
{
    [SerializeField] GameObject SuggestSong;
    [SerializeField] Text ObjectTitle;
    [SerializeField] GameObject ObjectScore;
    [SerializeField] GameObject[] ObjectStars;
    [SerializeField] GameObject ObjectFlow1;
    [SerializeField] GameObject ObjectFlow2;

    Timer[] mTimerStars;
    int mStar = 0;
    int mScore = 0;
    int mDiamondTarget = 0;
    int mDiamondDisplay = 0;
	bool mIsStarted = false;

	void Start()
	{

	}

	void OnEnable()
    {
		if (!mIsStarted)
		{
			mIsStarted = true;
			return;
		}

		if (UserPropertiesTracker.Instance)
        {
            UserPropertiesTracker.Instance.AddScreen("Result");
        }

        mTimerStars = new Timer[ObjectStars.Length];
        for (int i = 0; i < ObjectStars.Length; i++)
        {
            ObjectStars[i].GetComponent<ParticleSystem>().Stop();
            if (i < mStar)
            {
                mTimerStars[i] = new Timer();
                mTimerStars[i].SetDuration(1f + i * 0.5f);
            }
        }
    }

    void OnDisable()
    {
		// SuggestSong.GetComponent<SelectSong>().m_Play.OnClick.OnTrigger.Event.RemoveAllListeners();
        // SuggestSong.GetComponent<UIButton>().OnClick.OnTrigger.Event.RemoveAllListeners();
    }

	void Update()
	{
        for (int i = 0; i < ObjectStars.Length; i++)
        {
            if (mTimerStars[i] != null)
            {
                mTimerStars[i].Update(Time.deltaTime);
                if (mTimerStars[i].JustFinished())
                {
                    ObjectStars[i].GetComponent<ParticleSystem>().Play();
                    ObjectStars[i].GetComponent<AudioSource>().Play();
                }
            }
        }

#if UNITY_ANDROID
        if (Input.GetKeyDown(KeyCode.Escape))
		{
            if (UIPopup.AnyPopupVisible)
            {
                UIPopup popup = UIPopup.LastShownPopup;
                if (popup.HideOnBackButton)
                {
                    popup.Hide();
                }
            }
            else
            {
                GameEventMgr.SendEvent("home_finish");
            }
        }
#endif
	}

    public void SetInfo(string title, int score, int star, int diamond, SongInfo suggestSongInfo)
    {
        mStar = star;
        mScore = score;
        mDiamondTarget = 0;
        mDiamondDisplay = diamond;

        ObjectScore.GetComponent<AudioSource>().Play();
        DOTween.To(() => 0, value =>
        {
            ObjectScore.GetComponent<Text>().text = "" + value;
        }, score, 2)
        .OnComplete(() =>
        {
            ObjectScore.GetComponent<AudioSource>().Stop();
        });
        ObjectTitle.text = title.ToUpper();

        SelectSong song = SuggestSong.GetComponent<SelectSong>();
        song.SetSongInfo(suggestSongInfo);

        if (suggestSongInfo.Locked)
        {
            song.Lock();
        }
        else
        {
            song.UnLock();
        }

        bool showRetry = star < 3;
        ObjectFlow1.SetActive(showRetry);
        ObjectFlow2.SetActive(!showRetry);

        // song.m_Play.OnClick.OnTrigger.Event.AddListener(() => Play(suggestSongInfo));
        SuggestSong.GetComponent<UIButton>().OnClick.OnTrigger.Event.AddListener(() => PlayFree(suggestSongInfo));

        GameMgr.Instance.TrackingSongSuggestShow(suggestSongInfo, "result");
    }

    public void SetDiamondTarget(int diamond)
    {
        mDiamondTarget = diamond;
    }

    public int GetScore()
    {
        return mScore;
    }


	void Play(SongInfo songInfo)
    {
        GameMgr.Instance.TrackingSongSuggestClick(songInfo, "result");
		GameMgr.Instance.SetSong(songInfo);
        SuggestSong.GetComponent<SelectSong>().SetTag("hidden");
        SuggestSong.GetComponent<SelectSong>().Play("Result");
    }

    void PlayFree(SongInfo songInfo)
    {
        SuggestSong.transform.DOScale(new Vector3(0.8f, 0.8f, 1), 0.15f).SetEase(Ease.InSine).OnComplete(() =>
        {
            SuggestSong.transform.DOScale(new Vector3(0.85f, 0.85f, 1), 0.15f).SetEase(Ease.InSine).OnComplete(() =>
            {
                Play(songInfo);
            });
        });
    }
}
