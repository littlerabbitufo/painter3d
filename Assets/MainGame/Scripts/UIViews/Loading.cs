﻿using UnityEngine;
using UnityEngine.UI;
using Doozy.Engine.UI;
using Doozy.Engine.Progress;

public class Loading : MonoBehaviour
{
    [SerializeField] private Progressor LoadingBar;
    [SerializeField] private UIButton ButtonReload;
    [SerializeField] private Text TextInfo;
	[SerializeField] private GameObject ObjectError;

    bool mIsStarted = false;
    void Start()
    {
        ButtonReload.OnClick.OnTrigger.Event.AddListener(Reload);
    }

    void OnEnable()
    {
        if (!mIsStarted)
        {
            mIsStarted = true;
            return;
        }
        if (UserPropertiesTracker.Instance)
        {
            UserPropertiesTracker.Instance.AddScreen("Loading");
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    void Reload()
    {
        GameMgr.Instance.GameReload();
    }

    public void SetProgressInfo(string info, float progress)
    {
        LoadingBar.SetProgress(progress);

        TextInfo.text = info + " ... " + Mathf.Floor(progress * 100) + "%";
        TextInfo.gameObject.SetActive(true);
    }

    public void ShowLoadingError()
    {
        TextInfo.gameObject.SetActive(false);
        ObjectError.SetActive(true);
    }

    public void HideLoadingError()
    {
        TextInfo.gameObject.SetActive(false);
        ObjectError.SetActive(false);
    }

}
