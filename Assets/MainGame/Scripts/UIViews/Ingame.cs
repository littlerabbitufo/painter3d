﻿using UnityEngine;
using UnityEngine.UI;
using Doozy.Engine.UI;
using DG.Tweening;
using Defines;

public class Ingame : MonoBehaviour
{
    [SerializeField] private GameObject ObjectTutorial;
    [SerializeField] private Text TextScoreContainer;
    [SerializeField] private IngameProgress IngameProgress;
    [SerializeField] private UIButton ButtonGiveUp;

    bool mIsStarted = false;

    void Start()
    {
        SetCombo(0);
        ButtonGiveUp.OnClick.OnTrigger.Event.AddListener(() =>
        {
            if (!GameMgr.Instance.IsGamePause())
            {
                GameMgr.Instance.TrackingButtonClick("Pause at Ingame");
                GameMgr.Instance.Pause();
                UIPopup.GetPopup("GiveUp").Show();
            }
        });
    }

    void OnEnable()
    {
        if (!mIsStarted)
        {
            mIsStarted = true;
            return;
        }

        if (UserPropertiesTracker.Instance)
        {
            UserPropertiesTracker.Instance.AddScreen("Ingame");
        }

        ButtonGiveUp.gameObject.SetActive(Config.Firebase.General.UsePauseButton);
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_ANDROID
        if (Input.GetKeyDown(KeyCode.Escape))
		{
           if (!GameMgr.Instance.IsGamePause())
            {
                if (Config.Firebase.General.UsePauseBackKey)
                {
                    GameMgr.Instance.Pause();
                    UIPopup.GetPopup("GiveUp").Show();
                }
            }
            else
            {
                if (Config.Firebase.General.UsePauseBackKey)
                {
                    GameMgr.Instance.Resume();
                    UIPopup.HidePopup("GiveUp");
                }
            }
        }
#endif
    }

    void OnApplicationPause(bool pauseStatus)
	{
        if (Config.Firebase.General.UsePauseButton)
        {
            if (pauseStatus)
            {
                if (!GameMgr.Instance.IsGamePause())
                {
                    GameMgr.Instance.Pause();
                    UIPopup.GetPopup("GiveUp").Show();
                }
            }
        }
	}

    public void SetScore(int score)
    {
        TextScoreContainer.text = score.ToString();
    }

    public void SetCombo(int combo)
    {
    }

    public void SetProgress(float value)
    {
        IngameProgress.SetProgress(value);
    }

    public void ShowTutorial()
    {
        ObjectTutorial.SetActive(true);
    }

    public void HideTutorial()
    {
        ObjectTutorial.SetActive(false);
    }

    public int GetStars()
    {
        return IngameProgress.GetStars();
    }
}
