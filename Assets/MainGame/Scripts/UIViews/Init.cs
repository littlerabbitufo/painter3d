﻿using UnityEngine;
using Doozy.Engine.Nody;
using System.Collections.Generic;
using Pathfinding.Serialization.JsonFx;
using Defines;

public class Init : MonoBehaviour
{
    enum STATE
    {
        NONE,
        INIT_DOOZY,
        INIT_FIREBASE_REMOTE_CONFIG,
        WAITING,
    }

    [SerializeField] private SongMgr SongMgr;
    [SerializeField] private ProfileMgr ProfileMgr;
    [SerializeField] private GraphController MainGraph;

    STATE mState;
    Timer mTimerWaiting = new Timer();

    float time = 0;
    float Delay = 3;
    bool mIsRemoteConfigFetchCompleted = false;
    bool mIsFinished = false;

    void Awake()
    {
        mTimerWaiting.SetDuration(3);

        FirebaseManager.onRemoteConfigFetchComplete += OnRemoteConfigFetchComplete;
        FirebaseManager.onCheckFirebaseDependencyFinish += CheckFirebaseDependencyFinish;
    }

    void Start()
    {
        //UserPropertiesTracker.Instance.AddScreen("Init");
        SetState(STATE.INIT_DOOZY);
    }

    void CheckFirebaseDependencyFinish(bool result)
    {
        if (result)
        {
            SetDefaultValueForRemoteConfig();
        }
    }

    void SetDefaultValueForRemoteConfig()
    {
        Development.Log("Init->SetDefaultValueForRemoteConfig", LogType.Warning);
        Dictionary<string, object> defaults = new Dictionary<string, object>();

        string json = JsonWriter.Serialize(Config.Firebase);
        defaults.Add("GameConfig", json);

        TextAsset songs = Resources.Load("Songs") as TextAsset;
        defaults.Add("Songs", songs);

        Firebase.RemoteConfig.FirebaseRemoteConfig.SetDefaults(defaults);
    }

    void OnRemoteConfigFetchComplete(bool result)
    {
        Development.Log("Init->OnRemoteConfigFetchComplete:" + result, LogType.Warning);

        string config = (string)Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("GameConfig").StringValue;
        Config.Firebase = JsonReader.Deserialize<FirebaseConfig>(config);
        Config.FirebaseDefault = JsonReader.Deserialize<FirebaseConfig>(config);

        string songs = (string)Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Songs").StringValue;
        SongMgr.LoadSong(songs);
        // TextAsset data = Resources.Load("Songs") as TextAsset;
        // SongMgr.LoadSong(data.text);

        ProfileMgr.Init();
        GameMgr.Instance.UIViewMainMenu.CreateSongList(SongMgr.GetSongs());
        mIsRemoteConfigFetchCompleted = true;
    }

    void Update()
    {
        mTimerWaiting.Update(Time.deltaTime);
        switch (mState)
        {
            case STATE.INIT_DOOZY:
                if (MainGraph.Initialized)
                {
                    SetState(STATE.INIT_FIREBASE_REMOTE_CONFIG);
                }
                break;

            case STATE.INIT_FIREBASE_REMOTE_CONFIG:
                if (mIsRemoteConfigFetchCompleted)
                {
                    SetState(STATE.WAITING);
                }
                break;

            case STATE.WAITING:
                if (mTimerWaiting.IsDone())
                {
                    SetState(STATE.NONE);
                    GameEventMgr.SendEvent("init_completed");
                }
                break;
        }
    }

    void SetState(STATE state)
    {
        mState = state;
        switch (mState)
        {
            case STATE.INIT_DOOZY:
                break;

            case STATE.INIT_FIREBASE_REMOTE_CONFIG:
                break;

            case STATE.WAITING:
                break;
        }
    }
}

