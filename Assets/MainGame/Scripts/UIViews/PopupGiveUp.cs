﻿using UnityEngine;
using Doozy.Engine.UI;

public class PopupGiveUp : MonoBehaviour
{
    [SerializeField] private UIButton ButtonGiveUp;
    [SerializeField] private UIButton ButtonBack;

    void Start()
    {
        ButtonGiveUp.OnClick.OnTrigger.Event.AddListener(OnGiveUp);
        ButtonBack.OnClick.OnTrigger.Event.AddListener(OnBack);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnBack()
    {
        UIPopup.HidePopup("GiveUp");
        GameMgr.Instance.Resume();
        GameMgr.Instance.TrackingButtonClick("No at GiveUp");
    }

    void OnGiveUp()
    {
        UIPopup.HidePopup("GiveUp");
        GameMgr.Instance.Resume();
        GameMgr.Instance.GiveUp();
        GameMgr.Instance.TrackingButtonClick("Yes at GiveUp");
    }
}
