﻿using UnityEngine;
using UnityEngine.UI;
using Doozy.Engine.UI;
using Defines;
using Amanotes.Utils;
using System.Collections.Generic;

public class SelectSong : MonoBehaviour
{
    [SerializeField] private Text Title;
    [SerializeField] private GameObject ObjectPlay;
    [SerializeField] private GameObject ObjectUnlock;
    [SerializeField] private Text ObjectWatchAds;
    [SerializeField] private GameObject ObjectStarContainer;
    [SerializeField] private GameObject[] m_ObjectStars;
    [SerializeField] private GameObject ObjectTag;
    [SerializeField] private Text ObjectTagText;
    [SerializeField] public UIButton ButtonPlay;

    public delegate void OnPlay(SelectSong selectSong);
    public delegate void OnSelect(SelectSong selectSong);
    public delegate void OnListen(SelectSong selectSong);

    public static event OnPlay onPlay;
    public static event OnSelect onSelect;
    public static event OnListen onListen;

    Timer mTrackerImpression = new Timer();
    SongInfo mSongInfo;

    bool mIsLocked = true;

    void Start()
    {
        mTrackerImpression.SetDuration(2);
        ButtonPlay.OnClick.OnTrigger.Event.AddListener(() =>
        {
            if (onPlay != null)
            {
                onPlay(this);
            }
        });
    }

    void Update()
    {

    }

    public void SetSongInfo(SongInfo songInfo)
    {
        mSongInfo = songInfo;
        Title.text = mSongInfo.Name.ToUpper();
        SetStar(songInfo.Star);
        SetTag(songInfo.Tag);
    }

    public SongInfo GetSongInfo()
    {
        return mSongInfo;
    }

    public void SetTag(string tag)
    {
        if (tag.Trim() == "hidden")
        {
            ObjectTag.SetActive(false);
        }
        else
        {
            ObjectTag.SetActive(true);
            ObjectTagText.text = tag;
        }
    }

    public void ResetTrackerImpression()
    {
        mTrackerImpression.Reset();
    }

    public void Lock()
    {
        ObjectPlay.SetActive(false);
        ObjectUnlock.SetActive(true);
        ObjectWatchAds.text = "WATCH A VIDEO";
        ObjectStarContainer.SetActive(false);
        mIsLocked = true;
    }

    public void UnLock()
    {
        ObjectUnlock.SetActive(false);
        ObjectStarContainer.SetActive(true);
        ObjectWatchAds.gameObject.SetActive(false);
        mIsLocked = false;
    }

    public void SetStar(int star)
    {
        for (int i = 0; i < 3; i++)
        {
            m_ObjectStars[i].SetActive(i < star);
        }
    }

    public void Play(string location)
    {
        if (!mIsLocked)
        {
            GameEventMgr.SendEvent("select_song");
        }
        else
        {
            GameMgr.Instance.ShowRewardVideoForUnlock(mSongInfo);
        }
    }

    public void Listen()
    {
        if (onListen != null)
        {
            onListen(this);
        }
    }
}
