﻿using UnityEngine;
using Doozy.Engine.UI;

public class PopupLoading : MonoBehaviour
{
    [SerializeField] private GameObject LoadingIcon;
    [SerializeField] private GameObject LoadingMessage;
    [SerializeField] private UIButton ButtonClose;

    void Start()
    {
        LoadingIcon.gameObject.SetActive(true);
        ButtonClose.gameObject.SetActive(false);
        LoadingMessage.gameObject.SetActive(false);

        ButtonClose.OnClick.OnTrigger.Event.AddListener(HideError);
    }

    void Update()
    {
        LoadingIcon.transform.Rotate(new Vector3(0, 0, -5));
    }

    public void ShowError()
    {
        LoadingIcon.gameObject.SetActive(false);
        ButtonClose.gameObject.SetActive(true);
        LoadingMessage.gameObject.SetActive(true);
    }

    void HideError()
    {
        UIPopup.HidePopup("Loading");
    }
}
