﻿using System;
using UnityEngine;
using DG.Tweening;
using Amanotes.Data;
using Amanotes.Utils;
using Doozy.Engine.UI;
using System.Collections.Generic;
using Defines;

public class GameMgr : Singleton<GameMgr>
{
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //--GAME SETTINGS------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    [Header("Game Object")]
    [SerializeField] private Camera MainCamera;
    [SerializeField] private Camera UICamera;
    [SerializeField] private AudioSource MainAudio;
    [SerializeField] private NoteMgr NoteMgr;
    [SerializeField] private SongMgr SongMgr;
    [SerializeField] private ProfileMgr ProfileMgr;

    [SerializeField] public MainMenu UIViewMainMenu;
    [SerializeField] public Loading UIViewLoading;
    [SerializeField] public Ingame UIViewIngame;
    [SerializeField] public Revival UIViewRevival;
    [SerializeField] public Result UIViewResult;

    public ProfileMgr Profiles { get { return ProfileMgr; } }
    public SongMgr Songs { get { return SongMgr; } }

    //CONST
    enum STATE
    {
        NONE,
        LOADING,
        LOADING_FAILED,
        TAP_TO_PLAY,
        PLAYING,
        WIN,
        LOSE,
        RESULT,
    }

    // Ads
    private DateTime mAdsTime;
    private DateTime mSongLoadingTime;
    private Timer mAdsLoadingTimeout;
    private int mAdsNumDead;
    private bool mIsWatchedRV;
    private RewardType mCurrentRewardType;
    private SongInfo mCurrentRewardSongInfo;

    // Tracking
    private float mPlayTime;
    private float mSessionDuration;

    // Game
    private SongInfo mSongCurrent;
    private int mNoteStartIndex = 0;
    private float mPercentCompleted;
    private int mScore;
    private int mDiamondReward;
    private int mFirstTimeDeadCount = 0;
    private bool mIsGiveUp = false;
    private bool mIsGamePause = false;
    private bool mIsShowExitPopup = false;
    private STATE mState;

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //--UNITY FUNCTION-----------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    void Start()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
        Input.multiTouchEnabled = false;

        if (AdHelper.Instance)
        {
            AdHelper.Instance.onIntertitialFailed += OnIntertitialFailed;
            AdHelper.Instance.onIntertitialWatched += OnIntertitialWatched;
            AdHelper.Instance.onRewardVideoFailed += OnRewardVideoFailed;
            AdHelper.Instance.onRewardVideoWatched += OnRewardVideoWatched;
        }

        NoteMgr.OnProgress += GameOnProgress;
        NoteMgr.OnWin += GameWin;
        NoteMgr.OnLose += GameLose;

        Character.OnScore += AddScore;

        mAdsTime = DateTime.Now;
        mAdsNumDead = 0;
        mIsWatchedRV = false;
        mFirstTimeDeadCount = ProfileMgr.IsFirstLaunch ? 0 : 9999;

        MainCamera.enabled = false;
    }

    void AddScore(int score)
    {
        mScore += score;
        UIViewIngame.SetScore(mScore);
    }

    void SetState(STATE value)
    {
        mState = value;
        switch (mState)
        {
            case STATE.LOADING:

                MainCamera.enabled = true;

                string mp3Path = mSongCurrent.Path + ".mp3";
                string binPath = mSongCurrent.Path + ".bin";
                StreamingAssetLoader.Instance.LoadAudioClip(mp3Path, "Audios",
                    progress =>
                    {
                        UIViewLoading.SetProgressInfo("Loading audio", progress);
                    },
                    audioClip =>
                    {
                        if (audioClip != null)
                        {
                            Debug.Log("Load audio success");
                            if (MainAudio.clip != null)
                            {
                                MainAudio.clip.UnloadAudioData();
                            }
                            MainAudio.clip = audioClip;
                            MainAudio.loop = false;
                            MainAudio.volume = 1;
                            MainAudio.DOKill(true);

                            StreamingAssetLoader.Instance.LoadAmanotesContentFile(binPath, "Audios", Difficulty.Easy,
                                progress =>
                                {
                                    UIViewLoading.SetProgressInfo("Loading level", 1);
                                },
                                notes =>
                                {
                                    if (notes != null)
                                    {
                                        Debug.Log("Load bin success");
                                        var audio = mNoteStartIndex > 0 ? null : MainAudio;
                                        NoteMgr.SetNotes(notes, audio);

                                        SetState(STATE.TAP_TO_PLAY);
                                        GameEventMgr.SendEvent("loading_completed");
                                    }
                                    else
                                    {
                                        SetState(STATE.LOADING_FAILED);
                                    }
                                }
                            );
                            /*else
							{
								BeatDetector.GenerateNotes(audioClip, songInfo,
								progress =>
								{
									UIViewLoading.SetProgressInfo("Beat detecting", progress);
								},
								notes =>
								{
									if (notes != null)
									{
										Debug.Log("Load bin success");
										NoteMgr.SetNotes(notes);

										SetState(STATE.TAP_TO_PLAY);
										GameEventMgr.SendEvent("start_game");
									}
									else
									{
										SetState(STATE.LOADING_FAILED);
									}
								});
							}*/
                        }
                        else
                        {
                            SetState(STATE.LOADING_FAILED);
                        }
                    }
                );
                break;

            case STATE.LOADING_FAILED:
                UIViewLoading.ShowLoadingError();
                break;

            case STATE.TAP_TO_PLAY:
                UIViewIngame.ShowTutorial();
                TrackingSongActionPhase(mSongCurrent);
                break;

            case STATE.PLAYING:
                NoteMgr.Init(mNoteStartIndex);
                break;

            case STATE.WIN:
                break;

            case STATE.RESULT:
                MainCamera.enabled = false;
                NoteMgr.enabled = false;

                ProfileMgr.UpdateSongComplete(mSongCurrent);
                ProfileMgr.UpdateSongProcess(mSongCurrent, UIViewIngame.GetStars(), (int)(mPercentCompleted * 100));
                UIViewResult.SetInfo(mSongCurrent.Name, mScore, UIViewIngame.GetStars(), mDiamondReward, SongMgr.GetSuggestSongs(mSongCurrent));

                ShowInterstitial();
                TrackingSongResult(mSongCurrent, (int)(mPercentCompleted * 100), mScore);

                GameEventMgr.SendEvent("result");
                break;

            case STATE.LOSE:
                MainAudio.Stop();
                IncreaseDeadCountAds();
                UIViewRevival.SetInfo(mSongCurrent, mPercentCompleted, mScore);
                TrackingSongFail(mSongCurrent, (int)(mPercentCompleted * 100), mScore);
                if (!mIsWatchedRV && AdHelper.Instance.RewardVideoAvailable && !mIsGiveUp)
                {
                    GameEventMgr.SendEvent("revival");
                }
                else
                {
                    mIsGiveUp = false;
                    SetState(STATE.RESULT);
                }
                break;
        }
    }

    void Update()
    {
        float dt = Time.deltaTime;
        switch (mState)
        {
            case STATE.LOADING:
                // Debug.Log("UPDATE STATE.LOADING");
                break;

            case STATE.LOADING_FAILED:
                break;

            case STATE.TAP_TO_PLAY:
                if (!mIsGamePause && Input.GetMouseButton(0))
                {
                    UIViewIngame.HideTutorial();
                    SetState(STATE.PLAYING);
                }
                break;

            case STATE.PLAYING:
                // Background.UpdateThemes();
                UserPropertiesTracker.Instance.AddPlayTimePass(dt);
                mPlayTime += dt;
                break;

            case STATE.LOSE:
                // Debug.Log("UPDATE STATE.PLAYING");
                UserPropertiesTracker.Instance.AddPlayTimePass(dt);
                mPlayTime += dt;
                break;
        }

        if (mAdsLoadingTimeout != null)
        {
            mAdsLoadingTimeout.Update(dt);
            if (mAdsLoadingTimeout.JustFinished())
            {
                mAdsLoadingTimeout = null;
                UIPopup.HidePopup("Loading");
            }
        }

#if UNITY_ANDROID
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (UIViewMainMenu.gameObject.activeSelf)
            {
                if (UIPopup.AnyPopupVisible)
                {
                    UIPopup popup = UIPopup.LastShownPopup;
                    if (popup.HideOnBackButton)
                    {
                        popup.Hide();
                    }
                }
                else
                {
                    mIsShowExitPopup = !mIsShowExitPopup;
                }
            }
        }
#endif
        mSessionDuration += dt;
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //--GAME FUNCTION-----------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public void Pause()
    {
        if (mState != STATE.TAP_TO_PLAY)
        {
            if (MainAudio)
            {
                MainAudio.Pause();
            }
        }
        mIsGamePause = true;
        Time.timeScale = 0;
    }

    public void Resume()
    {
        if (mState != STATE.TAP_TO_PLAY)
        {
            if (MainAudio)
            {
                MainAudio.Play();
            }
        }
        mIsGamePause = false;
        Time.timeScale = 1;
    }

    public bool IsGamePause()
    {
        return mIsGamePause;
    }

    public void GameInit()
    {
        mScore = 0;
        mDiamondReward = 0;
        mPlayTime = 0;
        mNoteStartIndex = 0;

        // UIViewMainMenu.TagSelected(mSongIndex);
        UIViewIngame.SetScore(0);
        UIViewIngame.SetCombo(0);
        UIViewIngame.SetProgress(0);

        ProfileMgr.LastSongId = mSongCurrent.Id;

        TrackingSongStart(mSongCurrent);
        SetState(STATE.LOADING);
    }

    public void GameOnProgress(float progress)
    {
        if (progress > mPercentCompleted)
        {
            mPercentCompleted = progress;
            UIViewIngame.SetProgress(progress);
        }
    }

    public void GameRetry()
    {
        mScore = 0;
        mDiamondReward = 0;
        mPlayTime = 0;
        mNoteStartIndex = 0;

        UIViewIngame.SetScore(0);
        UIViewIngame.SetCombo(0);
        UIViewIngame.SetProgress(0);

        TrackingButtonClick("retry");
        TrackingSongStart(mSongCurrent);
        SetState(STATE.LOADING);
    }

    public void GameReload()
    {
        TrackingButtonClick("Reload");
        UIViewLoading.HideLoadingError();
        GameInit();
    }

    public void GameWin()
    {
        MainAudio.DOFade(0f, 2f).OnComplete(() =>
        {
            MainAudio.Stop();
            MainAudio.volume = 1;

            SetState(STATE.RESULT);
        });

        TrackingSongEnd(mSongCurrent);
        UIViewIngame.SetProgress(1);
    }

    public void GameLose()
    {
        SetState(STATE.LOSE);
    }

    public void Revival()
    {
        TrackingButtonClick("Revival");
        TrackingSongRevival(mSongCurrent, (int)(mPercentCompleted * 100), mScore);

        mNoteStartIndex = NoteMgr.GetNoteIndex();

        UIViewIngame.SetCombo(0);
        UIViewIngame.SetProgress(mPercentCompleted);

        GameEventMgr.SendEvent("continue_completed");
        SetState(STATE.LOADING);
    }

    public void RevivalNoThanks()
    {
        TrackingButtonClick("no thanks");
        //GameEventMgr.SendEvent("no_thanks");
        SetState(STATE.RESULT);
    }

    public void ResultScreen()
    {
        SetState(STATE.RESULT);
    }

    public void BackToMainMenu(string screen)
    {
        MainAudio.DOKill(true);
        SetState(STATE.NONE);
    }

    public void GiveUp()
    {
        mIsGiveUp = true;
        SetState(STATE.LOSE);
    }

    public void SetSong(SongInfo songInfo)
    {
        mSongCurrent = songInfo;
        ProfileMgr.LastSongId = songInfo.Id;
    }

    public int GetLastSongIndex()
    {
        return SongMgr.GetSongIndexById(ProfileMgr.LastSongId);
    }

    public string GetNotificationSong()
    {
        return SongMgr.GetSuggestSongs(null).Name;
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //--ADS FUNCTION-------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public void ShowRewardVideo()
    {
        if (!Config.IsTestBuild)
        {
            UIPopup popup = UIPopup.GetPopup("Loading");
            popup.Show();

            StreamingAssetLoader.Instance.CheckInternet(status =>
            {
                if (status && AdHelper.Instance.RewardVideoAvailable)
                {
                    mAdsLoadingTimeout = new Timer();
                    mAdsLoadingTimeout.SetDuration(5);
                    AdHelper.Instance.ShowRewardVideo(mCurrentRewardType, OnRewardVideoWatched, OnRewardVideoFailed);
                }
                else if (popup != null && popup.gameObject != null)
                {
                    PopupLoading popupLoading = popup.gameObject.GetComponent<PopupLoading>();
                    if (popupLoading)
                    {
                        popupLoading.ShowError();
                    }
                }
            });
        }
        else
        {
            OnRewardVideoWatched();
        }
    }

    public void ShowRewardVideoForUnlock(SongInfo songInfo)
    {
        mCurrentRewardType = RewardType.UnlockSong;
        mCurrentRewardSongInfo = songInfo;

        ShowRewardVideo();
    }
    public void ShowRewardVideoForContinue()
    {
        mCurrentRewardType = RewardType.Continue;
        ShowRewardVideo();
    }

    public void ShowInterstitial()
    {
        if (CanShowInterstitial())
        {
            if (mFirstTimeDeadCount >= Config.Firebase.Ads.MaxFailBetweenFS - 1)
            {
                Debug.Log("ShowInterstitial");
                AdHelper.Instance.ShowInterstitial(OnIntertitialWatched, OnIntertitialFailed);
            }
            else
            {
                Debug.Log("ShowInterstitial MaxFailBetweenFS");
                mFirstTimeDeadCount++;
            }
        }
    }

    public void IncreaseDeadCountAds()
    {
        mAdsNumDead++;
    }

    public void ResetAdsTimer()
    {
        mAdsTime = DateTime.Now;
        mAdsNumDead = 0;
    }

    private bool CanShowInterstitial()
    {
        if (Config.IsTestBuild)
        {
            return false;
        }

        TimeSpan timeDiff = DateTime.Now - mAdsTime;
        double second = (float)timeDiff.TotalSeconds;

        Debug.Log("Check CanShowInterstitial" + second);

        if (second >= Config.Firebase.Ads.DelayBeforeFirstFS ||
            second >= Config.Firebase.Ads.DelayBetweenFS ||
            mAdsNumDead >= Config.Firebase.Ads.MaxGamesBetweenFS)
        {
            return true;
        }

        if (mIsWatchedRV && second >= Config.Firebase.Ads.DelayBetweenRVAndFS)
        {
            mIsWatchedRV = false;
            return true;
        }

        return false;
    }

    void OnIntertitialFailed()
    {
        if (Config.IsTestBuild)
        {
            return;
        }
    }

    void OnIntertitialWatched()
    {
        ResetAdsTimer();
    }

    void OnRewardVideoFailed()
    {
        UIPopup.HidePopup("Loading");
    }

    void OnRewardVideoWatched()
    {
        switch (mCurrentRewardType)
        {
            case RewardType.UnlockSong:
                int songIndex = SongMgr.GetSongIndexById(mCurrentRewardSongInfo.Id);
                if (songIndex != -1)
                {
                    UIViewMainMenu.UnLockSong(songIndex);
                    UserPropertiesTracker.Instance.AddSongBuy();
                }
                break;

            case RewardType.Continue:
                Revival();
                break;
        }

        mIsWatchedRV = true;
        ResetAdsTimer();
        UIPopup.HidePopup("Loading");
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //--TRACKING FUNCTION--------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public string TrackingGetSongUnlock(string id)
    {
        SongInfo songInfo = SongMgr.GetSongDefaultById(id);
        if (songInfo != null)
        {
            if (!songInfo.Locked)
            {
                return "Default";
            }
            else if (songInfo.Diamond > 0)
            {
                return "Diamond";
            }
            else
            {
                return "Ad";
            }
        }
        return "MySong";
    }

    public void TrackingSongStart(SongInfo songInfo)
    {
        int songIndex = SongMgr.GetSongIndexById(songInfo.Id);
        Dictionary<string, object> paramaters = new Dictionary<string, object>(4);

        paramaters.Add("name", songInfo.Name);
        paramaters.Add("level", songIndex);
        paramaters.Add("unlock", TrackingGetSongUnlock(songInfo.Id));

        UserPropertiesTracker.Instance.AddSongStart();
        UserPropertiesTracker.Instance.AddSessionDuration((int)mSessionDuration); mSessionDuration = 0;
        AnalyticService.instance.LogEvent("song_start", paramaters);

        mSongLoadingTime = DateTime.Now;
    }

    public void TrackingSongEnd(SongInfo songInfo)
    {
        int songIndex = SongMgr.GetSongIndexById(songInfo.Id);
        Dictionary<string, object> paramaters = new Dictionary<string, object>(3);

        paramaters.Add("name", songInfo.Name);
        paramaters.Add("level", songIndex);
        paramaters.Add("unlock", TrackingGetSongUnlock(songInfo.Id));

        UserPropertiesTracker.Instance.AddSongEnd();
        UserPropertiesTracker.Instance.AddSessionDuration((int)mSessionDuration); mSessionDuration = 0;
        AnalyticService.instance.LogEvent("song_end", paramaters);
    }

    public void TrackingSongFail(SongInfo songInfo, int process, int score)
    {
        int songIndex = SongMgr.GetSongIndexById(songInfo.Id);
        Dictionary<string, object> paramaters = new Dictionary<string, object>(6);

        paramaters.Add("name", songInfo.Name);
        paramaters.Add("level", songIndex);
        paramaters.Add("unlock", TrackingGetSongUnlock(songInfo.Id));
        paramaters.Add("process", process);
        paramaters.Add("score", score);
        paramaters.Add("playtime", (int)mPlayTime);

        UserPropertiesTracker.Instance.AddSessionDuration((int)mSessionDuration); mSessionDuration = 0;
        AnalyticService.instance.LogEvent("song_fail", paramaters);
    }

    public void TrackingSongResult(SongInfo songInfo, int process, int score)
    {
        int songIndex = SongMgr.GetSongIndexById(songInfo.Id);
        Dictionary<string, object> paramaters = new Dictionary<string, object>(6);

        paramaters.Add("name", songInfo.Name);
        paramaters.Add("level", songIndex);
        paramaters.Add("unlock", TrackingGetSongUnlock(songInfo.Id));
        paramaters.Add("process", process);
        paramaters.Add("score", score);
        paramaters.Add("playtime", (int)mPlayTime);

        AnalyticService.instance.LogEvent("song_result", paramaters);
    }

    public void TrackingSongSuggestShow(SongInfo songInfo, string screen)
    {
        int songIndex = SongMgr.GetSongIndexById(songInfo.Id);
        Dictionary<string, object> paramaters = new Dictionary<string, object>(4);

        paramaters.Add("name", songInfo.Name);
        paramaters.Add("level", songIndex);
        paramaters.Add("unlock", TrackingGetSongUnlock(songInfo.Id));
        paramaters.Add("screen", screen);

        AnalyticService.instance.LogEvent("song_suggest_show", paramaters);
    }

    public void TrackingSongSuggestClick(SongInfo songInfo, string screen)
    {
        int songIndex = SongMgr.GetSongIndexById(songInfo.Id);
        Dictionary<string, object> paramaters = new Dictionary<string, object>(4);

        paramaters.Add("name", songInfo.Name);
        paramaters.Add("level", songIndex);
        paramaters.Add("unlock", TrackingGetSongUnlock(songInfo.Id));
        paramaters.Add("screen", screen);

        AnalyticService.instance.LogEvent("song_suggest_click", paramaters);
    }

    public void TrackingSongClick(SongInfo songInfo, string screen)
    {
        int songIndex = SongMgr.GetSongIndexById(songInfo.Id);
        Dictionary<string, object> paramaters = new Dictionary<string, object>(4);

        paramaters.Add("name", songInfo.Name);
        paramaters.Add("level", songIndex);
        paramaters.Add("unlock", TrackingGetSongUnlock(songInfo.Id));
        paramaters.Add("screen", screen);

        AnalyticService.instance.LogEvent("song_click", paramaters);
    }

    public void TrackingSongActionPhase(SongInfo songInfo)
    {
        int songIndex = SongMgr.GetSongIndexById(songInfo.Id);
        Dictionary<string, object> paramaters = new Dictionary<string, object>(4);

        TimeSpan timeDiff = DateTime.Now - mSongLoadingTime;
        double second = (float)timeDiff.TotalSeconds;

        paramaters.Add("name", songInfo.Name);
        paramaters.Add("level", songIndex);
        paramaters.Add("unlock", TrackingGetSongUnlock(songInfo.Id));
        paramaters.Add("loadingtime", (int)second);

        AnalyticService.instance.LogEvent("song_ap", paramaters);
    }

    public void TrackingRevivalImpression(SongInfo songInfo, string screen)
    {
        int songIndex = SongMgr.GetSongIndexById(songInfo.Id);
        Dictionary<string, object> paramaters = new Dictionary<string, object>(4);

        paramaters.Add("name", songInfo.Name);
        paramaters.Add("level", songIndex);
        paramaters.Add("unlock", TrackingGetSongUnlock(songInfo.Id));
        paramaters.Add("screen", screen);

        AnalyticService.instance.LogEvent("continue_impression", paramaters);
    }

    public void TrackingSongRevival(SongInfo songInfo, int process, int score)
    {
        int songIndex = SongMgr.GetSongIndexById(songInfo.Id);
        Dictionary<string, object> paramaters = new Dictionary<string, object>(6);

        paramaters.Add("name", songInfo.Name);
        paramaters.Add("level", songIndex);
        paramaters.Add("unlock", TrackingGetSongUnlock(songInfo.Id));
        paramaters.Add("process", process);
        paramaters.Add("score", score);
        paramaters.Add("playtime", (int)mPlayTime);

        AnalyticService.instance.LogEvent("song_revival", paramaters);
    }

    public void TrackingButtonClick(string name)
    {
        Dictionary<string, object> paramaters = new Dictionary<string, object>(1);
        paramaters.Add("name", name);

        AnalyticService.instance.LogEvent("button_click", paramaters);
    }
}
