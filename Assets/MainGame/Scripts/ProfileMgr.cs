﻿using UnityEngine;
using Defines;
using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;

public class Profiles
{
    public string LastSongId = "";
    public bool IsFirstLaunch = true;
    public List<SongInfo> Songs;
}

public class ProfileMgr : MonoBehaviour
{
    [SerializeField] private SongMgr SongMgr;

    private string KEY_PROFILES = "profiles";
    private Profiles mProfiles = new Profiles();
    private bool mIsLoaded = false;

	public string LastSongId
	{
		get { return mProfiles.LastSongId; }
        set { mProfiles.LastSongId = value; Save();}
	}

    public bool IsFirstLaunch
    {
        get { return mProfiles.IsFirstLaunch; }
        set { mProfiles.IsFirstLaunch = value; Save();}
    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Init()
    {
        if (PlayerPrefs.HasKey(KEY_PROFILES))
        {
            Load();
        }
        else
        {
            mProfiles.Songs = SongMgr.GetSongs();
            mIsLoaded = true;
            Save();
        }
    }

    public void Save()
    {
        if (mIsLoaded)
        {
            string json = JsonWriter.Serialize(mProfiles);
            PlayerPrefs.SetString(KEY_PROFILES, json);
        }
    }

    private void Load()
    {
        string json = PlayerPrefs.GetString(KEY_PROFILES);
        mProfiles = JsonReader.Deserialize<Profiles>(json);

        List<SongInfo> songs = SongMgr.GetSongs();
        for (int i = 0; i < songs.Count; i++)
        {
            SongInfo songInfo = GetSongById(mProfiles.Songs, songs[i].Id);
            if (songInfo != null)
            {
                SongMgr.SetSong(i, songInfo);
            }
        }

        mProfiles.Songs = SongMgr.GetSongs();
		TrackingSongBuy();

        mIsLoaded = true;
    }

    public void AddSong(SongInfo songInfo)
    {
        mProfiles.Songs.Insert(0, songInfo);
        Save();
    }

    public void DeleteSong(SongInfo songInfo)
    {
        mProfiles.Songs.Remove(songInfo);
        Save();
    }

    public void UnlockSong(SongInfo songInfo)
    {
        int index = mProfiles.Songs.IndexOf(songInfo);
        if (index != -1)
        {
            mProfiles.Songs[index].Locked = false;
        }
        Save();
    }

    public bool IsSongLocked(int index)
    {
        return mProfiles.Songs[index].Locked;
    }

    public bool UpdateSongProcess(SongInfo songInfo, int star, int percent)
    {
        int index = mProfiles.Songs.IndexOf(songInfo);
        bool isUpdated = false;

        if (index != -1)
        {
            if (mProfiles.Songs[index].Star < star)
            {
                mProfiles.Songs[index].Star = star;
                isUpdated = true;
            }

            if (mProfiles.Songs[index].Percent < percent)
            {
                mProfiles.Songs[index].Percent = percent;
                isUpdated = true;
            }

            if (isUpdated)
            {
                Save();
            }
        }

        return isUpdated;
    }

    public void UpdateSongTag(SongInfo songInfo)
    {
        int index = mProfiles.Songs.IndexOf(songInfo);
        if (index != -1)
        {
            if (mProfiles.Songs[index].Tag.Trim() != "hidden")
            {
                mProfiles.Songs[index].Tag = "hidden";
                Save();
            }
        }
    }

    public void UpdateSongComplete(SongInfo songInfo)
    {
        int index = mProfiles.Songs.IndexOf(songInfo);
        if (index != -1)
        {
            mProfiles.Songs[index].Completed = true;
            Save();
        }
    }

    private SongInfo GetSongById(List<SongInfo> songs, string id)
    {
        for (int i = 0; i < songs.Count; i++)
        {
            if (songs[i].Id == id)
            {
                return songs[i];
            }
        }

        return null;
    }

    private void TrackingSongBuy()
    {
        int count = 0;
        foreach(SongInfo song in mProfiles.Songs)
        {
            if (!song.Locked)
            {
                count++;
            }
        }
        PlayerPrefs.SetInt("song_buy", count);
    }
}
