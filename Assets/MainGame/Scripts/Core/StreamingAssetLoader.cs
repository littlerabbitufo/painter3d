﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Amanotes.Data;
using Defines;

public class StreamingAssetLoader : Singleton<StreamingAssetLoader>
{
    private bool mIsUseCache = !Config.IsTestBuild;

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //--UNITY FUNCTION-----------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    void Start()
    {

    }

    void Update()
    {

    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //--PUBLIC FUNCTION----------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public void LoadAmanotesContentFile(string fileName, string path, Difficulty difficulty, Action<float> progress, Action<List<NoteData>> callback)
    {
        StartCoroutine(GetContent(GetPath(path, fileName), difficulty, progress, notes =>
        {
            callback(notes);
        }));
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public void LoadString(string fileName, string path, Action<string> callback)
    {
        StartCoroutine(GetString(GetPath(path, fileName), text =>
        {
            callback(text);
        }));
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public void LoadAudioClip(string fileName, string path, Action<float> progress, Action<AudioClip> callback)
    {
        StartCoroutine(GetAudioClip(GetPath(path, fileName), progress, audioClip =>
        {
            callback(audioClip);
        }));
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public void CheckInternet(Action<bool> callback)
    {
        StartCoroutine(Ping(status =>
        {
            callback.Invoke(status);
        }));
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //--PRIAVTE FUNCTION---------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    IEnumerator GetContent(string url, Difficulty difficulty, Action<float> progress, Action<List<NoteData>> callback)
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        www.SendWebRequest();

        while (!www.isDone)
        {
            progress.Invoke(www.downloadProgress);
            yield return null;
        }
        if (www.isNetworkError || www.isHttpError)
        {
            callback(null);
        }
        else
        {
            try
            {
                NoteGeneration.LoadFileContent(www.downloadHandler.data, difficulty, notes =>
                {
                    if (url.StartsWith("http"))
                    {
                        string savePath = Path.Combine(Application.persistentDataPath, Path.GetFileName(url));
                        File.WriteAllBytes(savePath, www.downloadHandler.data);
                    }
                    callback(notes);
                },
                error =>
                {
                    callback(null);
                });
            }
            catch
            {
                callback(null);
            }
        }
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    IEnumerator GetString(string url, Action<string> callback)
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        www.timeout = 10;
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            callback(null);
        }
        else
        {
            callback(www.downloadHandler.text);
        }
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    IEnumerator GetAudioClip(string url, Action<float> progress, Action<AudioClip> callback)
    {
        AudioType type = AudioType.MPEG;
        #if UNITY_IOS
        if(url.EndsWith(".mp4") || url.EndsWith(".m4a"))
        {
            type = AudioType.AUDIOQUEUE;
        }
        #endif
        UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(url, type);
        www.SendWebRequest();

        while (!www.isDone)
        {
            progress.Invoke(www.downloadProgress);
            yield return null;
        }

        if (www.isNetworkError || www.isHttpError)
        {
            callback(null);
        }
        else
        {
            if (url.StartsWith("http"))
            {
                string savePath = Path.Combine(Application.persistentDataPath, Path.GetFileName(url));
                File.WriteAllBytes(savePath, www.downloadHandler.data);
            }

            callback(DownloadHandlerAudioClip.GetContent(www));
        }
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    IEnumerator Ping(Action<bool> status)
    {
        UnityWebRequest www = UnityWebRequest.Head("http://google.com");
        www.timeout = 5;
        yield return www.SendWebRequest();

        status.Invoke(!(www.isNetworkError || www.isHttpError));
    }

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    string GetPath(string path, string url)
    {
        bool isOnline = url.StartsWith("http");
        bool isAbsolute = url.StartsWith("file:///");

        if (isOnline)
        {
            if (!mIsUseCache)
            {
                Debug.Log("StreamingAssetLoader load file with cache is disabled " + url);
                return url;
            }
            else
            {
                string filePath = Path.Combine(Application.persistentDataPath, Path.GetFileName(url));
                if (File.Exists(filePath))
                {
                    Debug.Log("StreamingAssetLoader load file from cache " + Path.GetFileName(url));
                    return "file:///" + filePath;
                }
                else
                {
                    Debug.Log("StreamingAssetLoader load file from cloud " + url);
                    return url;
                }
            }
        }
        else if (isAbsolute)
        {
            return url;
        }
        else
        {
            Debug.Log("StreamingAssetLoader load file from streaming assets path " + url);
    #if UNITY_ANDROID
            return Path.Combine(Application.streamingAssetsPath + "/" + path, url);
    #else
                return "file:///" + Path.Combine(Application.streamingAssetsPath + "/" + path, url);
    #endif
        }
    }
}
