﻿// #define FEATURE_NOTIFICATIONS

using UnityEngine;
using System.Collections.Generic;

#if FEATURE_NOTIFICATIONS
#if UNITY_ANDROID
    using Unity.Notifications.Android;
#elif UNITY_IOS
    using Unity.Notifications.iOS;
#endif
#endif

class NotificationInfo
{
    public string Title;
    public string Info;
}

public class LocalNotification : MonoBehaviour
{
#if FEATURE_NOTIFICATIONS
    #if UNITY_ANDROID
    private AndroidNotification mNotification;

    #elif UNITY_IOS
    private iOSNotification mNotification;
    #endif
#endif

    List<string> mNotificationInfo = new List<string>()
    {
        "[NEW] [SONG] is in da house. Play that LIT song now!",
        "Ting! You got FREE GIFT! Get it NOW or I'll take it"
    };

    bool mIsRegistered = false;
    int mNotifyID = -1;

    void Setup(string songName)
    {
        Debug.Log("NotificationsMgr Setup");
#if FEATURE_NOTIFICATIONS
        if (!mIsRegistered)
        {
        #if UNITY_ANDROID
            var c = new AndroidNotificationChannel()
            {
                Id = "dunknbeat",
                Name = "Default Channel",
                Importance = Importance.High,
                Description = "Generic notifications",
            };
            AndroidNotificationCenter.RegisterNotificationChannel(c);
        #endif
            mIsRegistered = true;
        }

        string info = mNotificationInfo[UnityEngine.Random.Range(0, mNotificationInfo.Count)];
        if (info.Contains("[SONG]"))
        {
            info = info.Replace("[SONG]", songName);
        }

        #if UNITY_ANDROID
            mNotification = new AndroidNotification();
            mNotification.Title = "Music time!!!";
            mNotification.Text = info;
            mNotification.LargeIcon = "icon_notification";

            if (PlayerPrefs.HasKey("NotifyID"))
            {
                mNotifyID = PlayerPrefs.GetInt("NotifyID");
            }
        #elif UNITY_IOS
            mNotification = new iOSNotification()
            {
                Identifier = "dunknbeat",
                Title = "Music time!!!",
                Body = info,
                Subtitle = "",
                ShowInForeground = true,
                ForegroundPresentationOption = (PresentationOption.Alert | PresentationOption.Sound),
                CategoryIdentifier = "category",
                ThreadIdentifier = "thread1",
                Trigger = new iOSNotificationTimeIntervalTrigger()
                {
                    TimeInterval = new System.TimeSpan(24, 0, 0),
                    Repeats = false
                }
            };
        #endif
#endif
    }

    //PUBLIC FUNCTION
    public void AddNotification(string songName)
    {
        Setup(songName);

        Debug.Log("NotificationsMgr AddNewNotification");
#if FEATURE_NOTIFICATIONS
    #if UNITY_ANDROID
        // mNotification.FireTime = System.DateTime.Now.AddSeconds(60);
        mNotification.FireTime = System.DateTime.Now.AddDays(1);

        AndroidNotificationCenter.CancelNotification(mNotifyID);
        mNotifyID = AndroidNotificationCenter.SendNotification(mNotification, "dunknbeat");
        PlayerPrefs.SetInt("NotifyID", mNotifyID);
    #elif UNITY_IOS
        iOSNotificationCenter.RemoveAllScheduledNotifications();
        iOSNotificationCenter.ScheduleNotification(mNotification);
    #endif
#endif
    }

}
