﻿Shader "Hidden/cloud"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _NoiseConfig("NoiseConfig", Vector) = (1, 1, 1, 1)
        _NoiseHeight("NoiseHeight", Float) = 0
        _BaseStrength("BaseStrength", Float) = 1
        _Remap("Remap", Vector) = (0, 1, -1, 1)
        _ColorMin("ColorMin", Color) = (0, 0, 0, 0)
        _ColorMax("ColorMax", Color) = (1, 1, 1, 1)
        _CloudCapacity("CloudCapacity", Float) = .01
    }
    SubShader
    {
        // No culling or depth
        Lighting Off
        Blend SrcAlpha OneMinusSrcAlpha
        Cull Back
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            uniform sampler2D _CameraDepthTexture;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 color : COLOR;
                float4 vertex : SV_POSITION;
                float4 projPos : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };

            float4 _MainTex_ST;
            sampler2D _MainTex;
            float4 _NoiseConfig;
            float _NoiseHeight;
            float _BaseStrength;
            float4 _Remap;
            float4 _ColorMin;
            float4 _ColorMax;
            float _CloudCapacity;

            v2f vert (appdata v)
            {
                v2f o;
                float2 uv1 = v.vertex.xz * _NoiseConfig.x;
                float2 uv2 = TRANSFORM_TEX(uv1, _MainTex);
                float speed = _Time.x * _NoiseConfig.y;
                uv1 = TRANSFORM_TEX(uv1 + float2(0, speed), _MainTex);

                float d1 = tex2Dlod(_MainTex, float4(uv1, 0, 0)).r;
                float d2 = tex2Dlod(_MainTex, float4(uv2, 0, 0)).r;

                float d = (d1 + d2) * .5;
                d = saturate(d * d);
                d = _Remap.z + (d - _Remap.x) * (_Remap.w - _Remap.z) / (_Remap.y - _Remap.x);
                d = smoothstep(0., 1., abs(d));

                uv1 = v.vertex.xz * _NoiseConfig.z;
                speed = _Time.x * _NoiseConfig.w;
                uv1 = TRANSFORM_TEX(uv1 + float2(0, speed), _MainTex);

                d1 = tex2Dlod(_MainTex, float4(uv1, 0, 0)).r;
                d = (d + d1 * _BaseStrength) / (1. + _BaseStrength);

                float x = v.vertex.x * .01;
                o.vertex = UnityObjectToClipPos(v.vertex + float4(0, d * _NoiseHeight + x * x, 0, 0));
                UNITY_TRANSFER_FOG(o,o.vertex);

                o.color = float4(d, d, d, d);
                o.projPos = ComputeScreenPos(o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float depth = LinearEyeDepth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)).r);

                fixed4 col = lerp(_ColorMin, _ColorMax, i.color) * 1.2;
                col.a = saturate((depth - i.projPos.w) * _CloudCapacity);
                UNITY_APPLY_FOG(i.fogCoord, col);
                // UNITY_OPAQUE_ALPHA(col.a);
                // just invert the colors
                // col.rgb = 1 - col.rgb;
                return col;
            }
            ENDCG
        }
    }
}
