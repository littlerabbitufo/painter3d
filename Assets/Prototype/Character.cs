﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Character : MonoBehaviour
{
    public Transform PickerObject;
    public Vector3 pos;
    public float X;
    public Vector3 movementConfig;
    public AudioClip[] audioClips;
    //public Animator[] HumanAnim;
    [SerializeField] float INVINCIBLE_TIME = 3f;
    private enum TouchState
    {
        TOUCH_DOWN = 0,
        TOUCH_HOLD,
        TOUCH_UP
    }
    [SerializeField] [Range(1, 500f)] float _sensity = 100;

    TouchState _touchState = TouchState.TOUCH_UP;
    Vector3 _previousMouseDownPos;
    Vector3 origin;
    Vector3 p;
    int milestoneId;
    Animator anim;
    private HPContainer hpContainer;
    float _invicibleTimer = 0f;
    // Renderer _humanRenderer;
    Renderer _pickerRenderer;
    DrawTexture drawTexture;
    AudioSource audioSource;
    internal static event Action<int> OnScore;
    // Start is called before the first frame update
    void Awake()
    {
        origin = transform.position;
        audioSource = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        hpContainer = FindObjectOfType<HPContainer>();
        NoteMgr.OnReset += Reset;

        // foreach (var i in HumanAnim)
        // {
        //     i.Play("Idle", 0, UnityEngine.Random.Range(0f, 1f));
        // }

        // this._humanRenderer = HumanAnim[0].GetComponentInChildren<Renderer>();
        this._pickerRenderer = this.PickerObject.GetComponentInChildren<Renderer>();
        drawTexture = FindObjectOfType<DrawTexture>();
    }

    void Start()
    {
        //LoopPlane.OnCrack += OnCrack;
        ThrowObject.OnCrack += OnThrow;
    }

    public void Reset()
    {
        transform.position = origin;
        gameObject.SetActive(false);
        milestoneId = 0;
        _invicibleTimer = 0f;

        this._pickerRenderer.enabled = true;
        // this._humanRenderer.enabled = true;
    }

    public IEnumerator Revive()
    {
        enabled = false;
        OnClear();
        yield return null;
        gameObject.SetActive(true);
    }

    void OnEnable()
    {
        p = transform.position;
        pos.z = pos.x;

        // foreach (var i in HumanAnim)
        // {
        //     i.Play("Push-2", 0, UnityEngine.Random.Range(0f, 1f));
        // }
        _touchState = TouchState.TOUCH_UP;
        //PickerObject.GetComponent<TrailRenderer>().enabled = true;
    }

    void OnClear()
    {
        drawTexture.ClearCharacter();
    }

    // Update is called once per frame
    void Update()
    {
        if (this._invicibleTimer > 0)
        {
            int splashCount = Mathf.FloorToInt(this._invicibleTimer / 0.05f);

            this._invicibleTimer -= Time.deltaTime;

            bool isEnableRenderer = splashCount % 2 == 0 || this._invicibleTimer <= 0f;

            // this._humanRenderer.enabled = isEnableRenderer;
            this._pickerRenderer.enabled = isEnableRenderer;
        }

        if (Input.GetMouseButton(0))
        {
            if (_touchState == TouchState.TOUCH_DOWN)
            {
                Vector3 delta = (Input.mousePosition - _previousMouseDownPos) / _sensity;
                // no up down move aim
                delta.y = 0f;
                //Debug.Log("x offset " + delta.x);
                //this._targetShot.position += (this._targetShot.right * delta.x);
                var x = p.x + delta.x;
                if (x < -X)
                {
                    x = -X;
                }
                else if (x > X)
                {
                    x = X;
                }

                p.x = x;

                _previousMouseDownPos = Input.mousePosition;
            }
            else if (_touchState == TouchState.TOUCH_UP)
            {
                _touchState = TouchState.TOUCH_DOWN;
                _previousMouseDownPos = Input.mousePosition;
            }
        }
        else
        {
            _touchState = TouchState.TOUCH_UP;
        }


        p.z += pos.z * Time.deltaTime;
        PickerObject.GetChild(0).Rotate(0, 0, movementConfig.z * Time.deltaTime * (pos.z / pos.x), Space.Self);
        transform.position = p;
    }
    public void OnObstacle(int live)
    {
        // if (this._invicibleTimer > 0f || live == 3)
        // {
        //     return;
        // }
        if (live < 3)
        {
            this._invicibleTimer = this.INVINCIBLE_TIME;
            anim.Play("obstacle");
        }
        //HumanAnim[0].gameObject.SetActive(live > 0);
        //HumanAnim[1].gameObject.SetActive(live > 1);
        //HumanAnim[2].gameObject.SetActive(live > 2);
        hpContainer.OnHeartUpdate(live);
    }

    public bool IsInvincible()
    {
        return this._invicibleTimer > 0f;
    }

    void OnTriggerEnter(Collider other)
    {
        var po = other.GetComponent<PoolObject>();
        if (po)
        {
            po.Crack();
            if (po is CrackedObject && po.transform.childCount > 1)
            {
                audioSource.clip = audioClips[4];
                audioSource.Play();
            }
        }
    }

    private void OnCrack()
    {
        pos.z = pos.x;
        //FindObjectOfType<DrawTexture>().OnHide();
        enabled = true;
        //GetComponent<Animation>().Play();

        // foreach (var i in HumanAnim)
        //     i.Play("Push-2", 0, UnityEngine.Random.Range(0f, 1f));
    }

    IEnumerator Fixx()
    {
        float t = 1f;
        while (t > 0)
        {
            t -= Time.deltaTime;
            yield return null;
        }
        audioSource.clip = audioClips[3];
        audioSource.Play();

        t = .3f;
        while (t > 0)
        {
            t -= Time.deltaTime;
            p.x *= 0.9f;
            transform.position = p;
            yield return null;
        }

        audioSource.clip = audioClips[3];
        audioSource.Play();

        t = 1.1f;
        while (t > 0)
        {
            t -= Time.deltaTime;
            p.x *= 0.9f;
            transform.position = p;
            yield return null;
        }
        audioSource.clip = audioClips[1];
        audioSource.Play();

        t = .7f;
        while (t > 0)
        {
            t -= Time.deltaTime;
            yield return null;
        }
        audioSource.clip = audioClips[2];
        audioSource.Play();
    }

    private void OnThrow(ThrowObject obj)
    {
        _invicibleTimer = 0;
        // this._humanRenderer.enabled = true;
        this._pickerRenderer.enabled = true;

        pos.z = pos.y;
        //transform.position = new Vector3(p.x, p.y, p.z);
        //GetComponent<Animation>().Play();
        anim.Play(drawTexture.isFinalDraw() ? "finish" : "Push");
        audioSource.clip = audioClips[0];
        audioSource.Play();
        if (drawTexture.isFinalDraw()) StartCoroutine(Fixx());
        // foreach (var i in HumanAnim)
        //     i.Play("PushDraw", 0, UnityEngine.Random.Range(0f, 1f));

        float t = FindObjectOfType<NoteMgr>().getMilestoneDelay(milestoneId);
        if (enabled && t > .1f && t < 100f)
        {
            milestoneId += 2;
            Invoke("OnCrack", t);
        }
        enabled = false;
        // PickerObject.GetComponent<TrailRenderer>().enabled = false;
        PickerObject.GetComponent<BloodSplatterVfx>().enabled = true;
    }
}
