﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopPlane : PoolObject
{
    Vector3 origin;
    public Mesh[] meshes;
    //public Transform picture;

    internal static event Action<LoopPlane> OnCrack;
    // Start is called before the first frame update
    void Start()
    {
        origin = transform.position;
        NoteMgr.OnReset += Reset;
        //NoteMgr.OnInit += StartGame;

        if (meshes.Length > 0) OnCrack += ReRoll;

        enabled = false;
    }

    void OnDestroy()
    {
        if (meshes.Length > 0) OnCrack -= ReRoll;
        NoteMgr.OnReset -= Reset;
        //NoteMgr.OnInit -= StartGame;
    }

    void ReRoll(LoopPlane loopPlane)
    {
        // var mesh = meshes[UnityEngine.Random.Range(0, meshes.Length)];
        // var meshFilter = GetComponentsInChildren<MeshFilter>();
        // foreach (var m in meshFilter)
        // {
        //     m.mesh = mesh;
        //     var t = m.transform;
        //     var h = UnityEngine.Random.Range(41, 67);
        //     t.localScale = new Vector3(50, h, 50);

        //     var pos = t.localPosition;
        //     t.localPosition = new Vector3(pos.x, h * .5f, pos.z);
        //     t.Rotate(0, UnityEngine.Random.Range(0, 360), 0);
        // }

        var trans = loopPlane.transform;
        transform.position = trans.position + new Vector3(0, transform.position.y, trans.lossyScale.z + 200f);
        GetComponent<Animation>().Play();
    }

    private void Reset()
    {
        transform.position = origin;
        enabled = false;
    }

    public override void Crack()
    {
        OnCrack(this);
    }
}
