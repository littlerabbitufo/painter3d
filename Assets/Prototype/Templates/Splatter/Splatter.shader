﻿Shader "Custom/Splatter"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        _Offset("Offset", Vector) = (0, 0, 0, 0)
        _MainTex ("Texture", 2D) = "white" {}
        _SplatterTex ("Splatter Texture", 2D) = "white" {}
        //_Power("Power", Vector) = (1, 1, 1, 1)
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent"}

        Cull Off
		Lighting On
		ZWrite On
        Blend One OneMinusSrcAlpha

        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows alpha:fade

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _SplatterTex;

        struct Input
        {
            float2 uv_SplatterTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;
        float4 _Offset;
        //float4 _Power;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            float4 c = tex2D(_MainTex, _Offset);
            float4 mainC = tex2D(_SplatterTex, IN.uv_SplatterTex);
            float4 final = mainC * c;
            //float d = dot(o.Normal, float3(0., _Power.z, _Power.w));
            //c.rbg = pow(c.rbg, _Power.y) * pow(d, _Power.x);

            o.Albedo = final.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = mainC.a;
            o.Emission = final.rgb;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
