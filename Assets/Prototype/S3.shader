﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Hidden/S3"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Mask ("Mask1", 2D) = "white" {}
        //_Mask2 ("Mask2", 2D) = "white" {}
        _Y ("Y", Vector) = (0, 0, 0, 0)
    }
    SubShader
    {
        // No culling or depth
        Lighting Off
        Cull Off ZWrite On ZTest Less
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _Mask;
            //sampler2D _Mask2;
            float4 _Y;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed t = _Y.x;
                fixed4 col = tex2D(_MainTex, i.uv);
                fixed4 d = tex2D(_Mask, i.uv);// * t + tex2D(_Mask2, i.uv) * (1. - t);
                //float intent = (d.r + d.b + d.g) / 3.;
                //float intent = d.r;
                //intent = smoothstep(0., 1., intent);
                //intent = pow(intent, 3.);

                fixed4 res = col * d + (1. - d);
                res = res * t +  col * (1. - t);
                res.a = _Y.y;

                return res;
            }
            ENDCG
        }
    }
}
