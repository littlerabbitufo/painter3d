﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct Command
{
    public float time;
    public float rate;
    public string type;
    public int posId;
    //public float angle;
    public bool isNull()
    {
        return time == 0;
    }
}

[System.Serializable]
public class Mission
{
    public int maxScore;
    public List<Command> commands;
    int id = 0;
    public Command GetCommand()
    {
        if (id == commands.Count)
        {
            //id = 0;
            return new Command();
        }
        return commands[id++];
    }

    public int GetId() { return id; }
}

[System.Serializable]
public class Pattern
{
    public PoolObject prefab;
    public Transform positions;
    public float delay;
    Queue<PoolObject> available = new Queue<PoolObject>();

    public void Init()
    {
        for (int i = 0; i < positions.childCount; i++)
        {
            var t = positions.GetChild(i);
            t.gameObject.SetActive(true);
        }
    }

    public GameObject SpawnById(int id)
    {
        var obj = available.Count > 0 ? available.Dequeue() : null;
        if (obj == null)
        {
            obj = GameObject.Instantiate<PoolObject>(prefab);
            obj.name = prefab.name;
        }

        obj.Set(positions.GetChild(id));

        return obj.gameObject;
    }

    public GameObject SpawnRandom(Vector3 delta, Vector3 rot)
    {
        var obj = available.Count > 0 ? available.Dequeue() : null;
        if (obj == null)
        {
            obj = GameObject.Instantiate<PoolObject>(prefab);
            obj.name = prefab.name;
        }

        positions.position = delta;
        positions.eulerAngles = rot;
        obj.Set(positions);
        //transList.RemoveAt(id);
        return obj.gameObject;
    }

    public GameObject SpawnNearest(Vector3 player)
    {
        var obj = available.Count > 0 ? available.Dequeue() : null;
        if (obj == null)
        {
            obj = GameObject.Instantiate<PoolObject>(prefab);
            obj.name = prefab.name;
        }

        Transform trans = null;
        float min = 1000;
        bool isFirework = obj.name == "firework";
        for (int i = 0; i < positions.childCount; i++)
        {
            var t = positions.GetChild(i);
            if (!t.gameObject.activeSelf) continue;
            if (isFirework && t.position.y < 0) continue;
            var d = (t.position - player).sqrMagnitude;
            if (d < min)
            {
                min = d;
                trans = t;
            }
        }

        if (trans != null)
        {
            obj.Set(trans);
            trans.gameObject.SetActive(false);
            return obj.gameObject;
        }
        else
        {
            Debug.LogError("full slot");
            return null;
        }
    }

    public void Return(PoolObject obj)
    {
        available.Enqueue(obj);
        Transform holder = obj.holder;
        holder.gameObject.SetActive(true);
        //Debug.Log("Obj return " + obj.name);
    }
}

public class GameManager : MonoBehaviour
{
    public SongConfig config;
    public Pattern[] patterns;
    public Image progress;
    public GameObject songSelect;
    public GameObject menu;
    public Text scoreTxt;
    public Text subScoreTxt;
    public float X;
    Mission mission;
    Command currentCmd;
    AudioSource audioSource;
    Character player;
    float freqSample;
    float samplesDiv;
    int score;

    // Use this for initialization
    void Awake()
    {
        this.enabled = false;
        score = 0;
        config.Init();
        foreach (Pattern p in patterns) p.Init();
        player = FindObjectOfType<Character>();
        GameEvent.ON_CRACKED += OnCracked;

        var content = songSelect.transform.parent;
        var count = config.songs.Length;
        for (int i = 1; i < count; i++)
        {
            var obj = Instantiate(songSelect, content);
            obj.GetComponentInChildren<Text>().text = i.ToString();
        }
    }

    public void Select(Transform trans)
    {
        config.id = trans.GetSiblingIndex();
        this.enabled = true;
        menu.SetActive(false);
    }

    void OnEnable()
    {
        var song = config.GetCurrent();

        var clip = Resources.Load<AudioClip>("Prototype/" + song.data);
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = clip;
        freqSample = 1f / clip.frequency;
        samplesDiv = 1f / clip.samples;

        var data = Resources.Load<TextAsset>("Prototype/" + song.data);
        mission = JsonUtility.FromJson<Mission>(data.text);
        var commands = mission.commands;
        Debug.Log(commands.Count);
        for (int i = 0; i < commands.Count; i++)
        {
            Command cmd = commands[i];
            if (cmd.type == null)
            {
                var id = Mathf.FloorToInt(cmd.rate * patterns.Length);
                var p = patterns[id];
                cmd.type = p.prefab.name;
                cmd.time -= p.delay;
            }
            commands[i] = cmd;
        }

        currentCmd = mission.GetCommand();
        player.enabled = true;
        audioSource.Play();
    }

    Pattern GetPattern(string name)
    {
        foreach (Pattern p in patterns)
            if (p.prefab.name == name) return p;

        return null;
    }

    void OnCracked(Transform trans)
    {
        //foreach (Pattern p in patterns)
        //    if (p.prefab.name == name) return p;

        GetPattern("cracked").SpawnRandom(trans.position, trans.eulerAngles);
    }

    public void ReturnObject(PoolObject obj)
    {
        GetPattern(obj.name).Return(obj);
        //AddScore(1, 0, 0);
    }

    public void AddScore(int beat, int item, int hit)
    {
        score += (beat - hit) * 10 + item * 5;

        if (item > 0)
        {
            scoreTxt.GetComponent<Animation>().Play();
            subScoreTxt.text = "+5";
            subScoreTxt.color = Color.cyan;
        }

        if (hit > 0)
        {
            scoreTxt.GetComponent<Animation>().Play();
            subScoreTxt.text = "-10";
            subScoreTxt.color = new Color(1f, 0.39f, 0.39f);
        }

        scoreTxt.text = score.ToString();
    }

    void OnDisable()
    {
        if (audioSource) Invoke("GameOver", 7f);
    }

    void GameOver()
    {
        audioSource.Stop();
        player.enabled = false;
        menu.SetActive(true);
        GameEvent.GAME_OVER();
    }

    // Update is called once per frame
    void Update()
    {
        float time = audioSource.timeSamples * freqSample + 3f;
        while (time > currentCmd.time)
        {
            //TODO
            var obj = GetPattern("Cube").SpawnRandom(
                new Vector3(Random.Range(-X, X), 6.5f, currentCmd.time * player.pos.z),
                new Vector3(0, Random.Range(0, 360), 0));
            //get next command
            currentCmd = mission.GetCommand();
            if (currentCmd.isNull())
            {
                //time = 0;
                //currentCmd = mission.GetCommand();
                progress.fillAmount = 0f;
                enabled = false;
                return;
            }
        }

        progress.fillAmount = 1f - audioSource.timeSamples * samplesDiv;
    }
}
*/