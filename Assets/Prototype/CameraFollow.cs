﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    //public GameObject env;
    //public Vector2 planeConfig;
    //public Material ground;
    //public Vector2 groundSpeed;
    Character target;
    Vector3 offset;
    Animation animation;
    [SerializeField] float _lerpSpd = 5f;
    [SerializeField] AnimationCurve _easingCurve;
    bool isPainting;
    // Start is called before the first frame update
    void Start()
    {
        target = FindObjectOfType<Character>();
        animation = GetComponentInChildren<Animation>();
        offset = transform.position - target.transform.position;
        /*env.GetComponent<LoopPlane>().delta = planeConfig.x * planeConfig.y;

        for (int i = 0; i < planeConfig.x; ++i)
        {
            var obj = Instantiate(env).transform;
            obj.position = new Vector3(0, 0, i * planeConfig.y);
            obj.Rotate(0, UnityEngine.Random.Range(0, 360) / 90 * 90, 0);
            StaticBatchingUtility.Combine(obj.gameObject);

            obj = Instantiate(env).transform;
            obj.position = new Vector3(50, 0, i * 50);
            obj.Rotate(0, UnityEngine.Random.Range(0, 360) / 90 * 90, 0);
            StaticBatchingUtility.Combine(obj.gameObject);

        }*/

        LoopPlane.OnCrack += OnCrack;
        ThrowObject.OnCrack += OnThrow;
        CrackedObject.OnCrack += OnScore;
    }

    private void OnScore(CrackedObject obj)
    {
        animation.Play("cam1");
    }

    private void OnCrack(LoopPlane obj)
    {
        isPainting = false;
    }

    private void OnThrow(ThrowObject obj)
    {
        isPainting = true;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        var a = transform.position;
        var b = target.transform.position + offset;

        if(isPainting) b.x = 0;

        //b.x = Mathf.LerpUnclamped(a.x, b.x, Time.deltaTime * _lerpSpd);
        b.x =  a.x + (this._easingCurve.Evaluate(Time.deltaTime * _lerpSpd)*(b.x - a.x));
        transform.position = b;

        //if (target.isActiveAndEnabled)
        //    ground.mainTextureOffset += groundSpeed * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        var po = other.GetComponent<PoolObject>();
        if (po) po.Return();
    }
}
