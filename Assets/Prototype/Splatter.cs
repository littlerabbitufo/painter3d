﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splatter : MonoBehaviour
{
    // Start is called before the first frame update
    public Material material;
    Material _mat;
    [SerializeField]Texture2D[] _splatterTextures;
    void Awake()
    {
        this._mat = GetComponentInChildren<Renderer>().material;
    }

    // Update is called once per frame
    public Texture2D ApplyColor(Texture2D mapTexture, Vector2 offset)
    {
        int randomIndex = Random.Range(0, this._splatterTextures.Length);
        var tex = this._splatterTextures[randomIndex];
        this._mat.SetTexture("_MainTex", mapTexture);
        this._mat.SetTexture("_SplatterTex", tex);
        this._mat.SetVector("_Offset", offset);

        material.SetTexture("_Splatter", tex);

        return tex;
    }
}
