﻿//using System;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawTexture : MonoBehaviour
{
    public RenderTexture texture;
    public RenderTexture texture1;
    //public RenderTexture texture2;
    public Texture2D[] texList;
    public Material material;
    public Material material1;
    public Material preview;
    public Material clear;
    public GameObject ConfettiVfx;
    Queue<int> badge;
    Texture2D texture2D;
    int baseNum = 1;
    public float[] baseRadius;
    int radius;
    Transform container;
    Animation anim;
    //Vector2 x;
    // Start is called before the first frame update
    public void Draw(Texture2D tex, Vector2 p)
    {
        //material.SetVector("_Offset", p);
        material1.SetVector("_Offset", p);

        if (tex != null)
        {
            //material.SetTexture("_Splatter", tex);
            material1.SetTexture("_Splatter", tex);
        }

        //Graphics.Blit(texture, texture, material);
        Graphics.Blit(texture1, texture1, material1);
    }

    public void Paint(Texture2D tex, Vector2 p)
    {
        p.x += .5f;
        material.SetVector("_Offset", p);

        if (tex != null)
        {
            material.SetTexture("_Splatter", tex);
        }

        Graphics.Blit(texture, texture, material);
    }

    void Reset()
    {
        clear.SetColor("_Color", Color.black);
        Graphics.Blit(texture, texture, clear);
        //Graphics.Blit(texture2, texture2, clear);
        radius = 0;
        //x.x = 0;
        material.SetFloat("_Radius", baseRadius[radius]);
    }

    public void ClearCharacter()
    {
        //x.x = 0;
        clear.SetColor("_Color", Color.white);
        Graphics.Blit(texture1, texture1, clear);
    }

    // Update is called once per frame
    void Awake()
    {
        ThrowObject.OnCrack += OnDraw;
        CrackedObject.OnCrack += OnCrack;
        LoopPlane.OnCrack += OnReplace;

        badge = new Queue<int>();
        container = transform.parent;
        anim = GetComponent<Animation>();
        //x.y = material1.GetFloat("_Radius");
    }

    private void OnReplace(LoopPlane obj)
    {
        var trans = obj.transform;
        enabled = false;
        anim.Play();
        container.position = trans.position + new Vector3(0, 0, trans.lossyScale.z + 5f);
    }

    private void OnDraw(ThrowObject obj)
    {
        if (!anim.enabled)
        {
            anim.enabled = true;
            material.SetFloat("_Radius", baseRadius[++radius]);
            anim.Play(isFinalDraw() ? "draw2" : "draw1");
            //Destroy(Instantiate(ConfettiVfx, transform.position, Quaternion.identity), 5f);
            // if (radius >= baseRadius.Length - 1)
            //     Graphics.Blit(texture, texture, this.clear);
        }
    }

    public bool isFinalDraw()
    {
        return radius >= baseRadius.Length - 1;
    }

    void OnEndAnim()
    {
        NoteMgr.OnFinishEndingAnim();
    }

    public Texture2D Init(int n)
    {
        Reset();
        badge.Clear();
        n = Mathf.CeilToInt(Mathf.Sqrt(n));
        baseNum = n;
        n *= n;
        var temp = new List<int>();
        for (int i = 0; i < n; i++)
            temp.Add(i);

        for (int i = 0; i < n; i++)
        {
            var k = UnityEngine.Random.Range(0, temp.Count);
            badge.Enqueue(temp[k]);
            temp.RemoveAt(k);
        }

        texture2D = texList[UnityEngine.Random.Range(0, texList.Length)];
        //material.SetTexture("_Mask", texture2D);
        material1.SetTexture("_Mask", texture2D);
        preview.SetTexture("_MainTex", texture2D);
        //anim.Play("draw1");
        return texture2D;
    }

    public void SetUp(Material mat)
    {
        mat.SetTexture("_MainTex", texture2D);
        int i = badge.Dequeue();
        mat.SetVector("_Offset", new Vector2(i / baseNum, i % baseNum) / baseNum);
    }

    public void DrawRandom()
    {
        int i = badge.Dequeue();
        Draw(null, new Vector2(i / baseNum, i % baseNum) / baseNum);
    }

    void Update()
    {
        var p = container.position;
        p.z += Time.deltaTime * 500f;
        container.position = p;
    }
    private void OnCrack(CrackedObject obj)
    {
        if (obj)
        {
            var param = obj.SpawnSplatter();
            Draw(param.Item1, param.Item2);
        }
    }
}
