﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Hidden/S4"
{
    Properties
    {
        _Splatter ("Texture0", 2D) = "white" {}
        _Mask ("Texture1", 2D) = "white" {}
        _Offset("Offset", Vector) = (0, 0, 0, 0)
        _Radius("Radius", Float) = 0.1
    }
    SubShader
    {
        // No culling or depth
        Lighting Off
        Cull Off ZWrite Off ZTest Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _Splatter;
            sampler2D _Mask;
            float2 _Offset;
            float _Radius;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_Mask, _Offset);

                float2 d = (i.uv - _Offset) * _Radius;
                float4 a = tex2D(_Splatter, d + .5).a;
                col.a = step(length(d), .5) * a;

                return  col;
            }
            ENDCG
        }
    }
}
