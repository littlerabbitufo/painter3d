﻿using System;
using UnityEngine;

public class ThrowObject : PoolObject
{
    internal static event Action<ThrowObject> OnCrack;
    // Start is called before the first frame update
    public override void Crack()
    {
        OnCrack(this);
        Return();
    }
}
