﻿using System;
using UnityEngine;

public class PoolObject : MonoBehaviour
{
    [HideInInspector]
    //public Transform holder = null;
    internal static event Action<PoolObject> OnReturn;

    public void Set(Vector3 position, Quaternion rotation)
    {
        transform.SetPositionAndRotation(position, rotation);
        //holder = trans;
        gameObject.SetActive(true);
    }

    public virtual void Return()
    {
        gameObject.SetActive(false);
        OnReturn(this);
    }

    public virtual void Crack()
    {

    }

    // Use this for initialization
    // void Start () {

    // }

    // Update is called once per frame
    // void Update () {

    // }
}
