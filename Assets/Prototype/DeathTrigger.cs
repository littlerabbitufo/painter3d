﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class DeathTrigger : PoolObject
{
    internal static event Action<DeathTrigger> OnCrack;
    // Start is called before the first frame update

    public override void Crack()
    {
        OnCrack(this);
    }
}
