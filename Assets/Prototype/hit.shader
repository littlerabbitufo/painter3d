﻿Shader "Hidden/hit"
{
    Properties
    {
        _Offset("Offset", Vector) = (0, 0, 0, 0)
        _MainTex ("Texture", 2D) = "white" {}
        _Power("Power", Vector) = (1, 1, 1, 1)
    }
    SubShader
    {
        // No culling or depth
        Cull Back ZWrite On ZTest Less
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal: NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float3 normal: NORMAL;
            };

            sampler2D _MainTex;
            float4 _Offset;
            float4 _Power;
            

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.normal = v.normal;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float4 c = tex2D(_MainTex, _Offset) * _Power.y;
                float d = dot(i.normal, float3(0., _Power.z, _Power.w));
                c.rbg = pow(c.rbg, _Power.y) * pow(d, _Power.x);
                return c;
            }
            ENDCG
        }
    }
}
