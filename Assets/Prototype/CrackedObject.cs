﻿using System;
using UnityEngine;
using DG.Tweening;

public class CrackedObject : PoolObject
{
    [SerializeField] GameObject _display;
    [SerializeField] GameObject _splatter;
    [SerializeField] float X;
    internal static event Action<CrackedObject> OnCrack;
    // Start is called before the first frame update

    void Awake()
    {
        //var mesh = GetComponent<MeshRenderer>();
        //mesh.material = new Material(mesh.material);
    }

    public void Init()
    {
    }
    
    public override void Crack()
    {
        OnCrack(this);
        base.Return();
    }

    public override void Return()
    {
        OnCrack(null);
        base.Return();
    }

    void OnEnable()
    {
        var anim = this._display.GetComponent<Animator>();
        anim.Play("PaintBall_Idle", 0, UnityEngine.Random.value);
    }

    public (Texture2D, Vector2) SpawnSplatter()
    {
        Material mat = this._display.GetComponent<Renderer>().material;
        Vector2 offset = mat.GetVector("_Offset");
        Texture2D texture = mat.GetTexture("_MainTex") as Texture2D;

        Vector3 splatterPos = this.transform.position;
        splatterPos.y = 1.3f;
        splatterPos.z += 5f;

        Quaternion randomRotate = Quaternion.Euler(0, UnityEngine.Random.Range(-90f, 90f), 0f);
        Quaternion randomRotateBloodVfx = Quaternion.Euler(0, 1f, 0f);
        Splatter splatter = GameObject.Instantiate(_splatter, splatterPos, randomRotate).GetComponent<Splatter>();
        GetComponent<BloodSplatterVfx>().SpawnRandom(splatterPos, randomRotateBloodVfx);
        Destroy(splatter.gameObject, 2f);

        return (splatter.ApplyColor(texture, offset), offset);
    }
}
