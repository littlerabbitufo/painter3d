﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Hidden/S2"
{
    Properties
    {
        _Splatter ("Texture0", 2D) = "white" {}
        //_Mask ("Texture1", 2D) = "white" {}
        _Offset("Offset", Vector) = (0, 0, 0, 0)
        _Radius("Radius", Float) = 0.1
    }
    SubShader
    {
        // No culling or depth
        Lighting Off
        Cull Off ZWrite Off ZTest Off
        Blend One One

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _Splatter;
            sampler2D _Mask;
            float2 _Offset;
            float _Radius;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                //fixed4 col = tex2D(_Mask, i.uv);
                //fixed4 mask = tex2D(_Mask, _Offset);
                
                //fixed d1 = length(i.uv - _Offset);
                //fixed d = abs(dot(col.rbg, mask.rbg));
                //d *= step(_Radius, 1. - pow(d, 3.));
                float2 d = (i.uv - _Offset) * _Radius;
                float4 a = tex2D(_Splatter, float2(i.uv.y, d.x + .5)).a * .5;

                return a * step(abs(d.x), .5) * step(i.uv.y, _Offset.y);
            }
            ENDCG
        }
    }
}
