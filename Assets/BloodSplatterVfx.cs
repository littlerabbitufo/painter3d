﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodSplatterVfx : MonoBehaviour
{
    public GameObject[] BloodVfx;

    float durationSpawn = 0.1f;
    int triggerSpawnNumber = 0;
    float m_spawnThrowVfxTimer = 0f;
    DrawTexture drawTexture;
    // Start is called before the first frame update
    void Start()
    {
        drawTexture = FindObjectOfType<DrawTexture>();
    }

    public void SpawnRandom(Vector3 pos, Quaternion rotation)
    {
        int randVfx = UnityEngine.Random.Range(0, BloodVfx.Length);
        GameObject bloodVfx = GameObject.Instantiate(BloodVfx[randVfx], pos, rotation);
        Destroy(bloodVfx, 3f);
    }
    public void SpawnRandomBillBoard(Vector3 pos, Quaternion rotation)
    {
        int randVfx = UnityEngine.Random.Range(0, BloodVfx.Length);
        GameObject bloodVfx = GameObject.Instantiate(BloodVfx[randVfx], pos, rotation);
        //bloodVfx.transform.LookAt(Camera.main.transform.position, Vector3.right);
        //bloodVfx.transform.rotation = Quaternion.identity;//Camera.main.transform.rotation;
        Destroy(bloodVfx, 3f);
    }

    void OnEnable()
    {
        m_spawnThrowVfxTimer = 0f;
        triggerSpawnNumber = 1;
    }

    // Update is called once per frame
    void Update()
    {
        m_spawnThrowVfxTimer += Time.deltaTime;
        // if (m_spawnThrowVfxTimer > triggerSpawnNumber * durationSpawn)
        // {
        //     triggerSpawnNumber++;
        //     for (int i = 0; i < 7; i++)
        //     {
        //         SpawnRandomBillBoard(transform.position + new Vector3(UnityEngine.Random.Range(-15f, 15f), 0f, 0f), Quaternion.Euler(-90f, 0f, 90f));
        //     }
        // }
        drawTexture.Paint(null, transform.position / new Vector2(25f, 25f));
        if (m_spawnThrowVfxTimer > .8f) enabled = false;
    }
}
