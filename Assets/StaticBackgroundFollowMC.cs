﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticBackgroundFollowMC : MonoBehaviour
{
    [SerializeField] Transform MC;

    private Vector3 Offset;
    // Start is called before the first frame update
    void Start()
    {
        Offset = MC.position - transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, MC.position.z + 550f);
    }
}
