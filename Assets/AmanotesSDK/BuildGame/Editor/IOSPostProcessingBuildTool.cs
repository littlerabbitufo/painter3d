﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
using UnityEditor.iOS.Xcode;
using System.IO;

public class IOSPostProcessingBuildTool
{

    [PostProcessBuild]
    public static void ChangeXcodePlist(BuildTarget buildTarget, string pathToBuiltProject)
    {
#if UNITY_IOS
        if (buildTarget == BuildTarget.iOS)
        {

            // Get plist
            string plistPath = pathToBuiltProject + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));

            // Get root
            PlistElementDict rootDict = plist.root;

            // Change value of CFBundleVersion in Xcode plist
            
        #region description permission for IOS 10
            rootDict.SetString("NSCalendarsUsageDescription", "Advertisement would like to create a calendar event.");
            rootDict.SetString("NSCameraUsageDescription", "Advertisement would like to use Camera.");
            rootDict.SetString("NSPhotoLibraryUsageDescription", "Advertisement would like to store a photo.");
            rootDict.SetString("NSMotionUsageDescription", "Interactive Advertisement controls");
        #endregion
         string exitsOnSuspendKey = "UIApplicationExitsOnSuspend";
        if (rootDict.values.ContainsKey(exitsOnSuspendKey))
        {
            rootDict.values.Remove(exitsOnSuspendKey);
        }
        #region for Adcolony
            PlistElementArray array = rootDict.CreateArray("LSApplicationQueriesSchemes");
            array.AddString("fb");
            array.AddString("instagram");
            array.AddString("tumblr");
            array.AddString("twitter");
            /* 
            PlistElementDict atsDict = rootDict.CreateDict("NSAppTransportSecurity");
		    atsDict.SetBoolean("NSAllowsArbitraryLoads", true);
            atsDict.SetBoolean("NSAllowsLocalNetworking", true);
            atsDict.SetBoolean("NSAllowsArbitraryLoadsInWebContent", true);*/
        #endregion
           

            // Write to file
            File.WriteAllText(plistPath, plist.WriteToString());

            //Setting project
            SettingProjectIOS(buildTarget,pathToBuiltProject);
        }
#endif
    }
    public static void SettingProjectIOS(BuildTarget buildTarget, string pathToBuiltProject)
    {
#if UNITY_IOS
        if (buildTarget == BuildTarget.iOS)
        {
            string projPath = pathToBuiltProject + "/Unity-iPhone.xcodeproj/project.pbxproj";

            PBXProject proj = new PBXProject();
            proj.ReadFromString(File.ReadAllText(projPath));

            string target = proj.TargetGuidByName("Unity-iPhone");

            proj.SetBuildProperty(target, "ENABLE_BITCODE", "false");// disable bitcode by default, reduce appside
            proj.AddCapability(target, PBXCapabilityType.PushNotifications); // turn on push notification
            proj.AddCapability(target, PBXCapabilityType.InAppPurchase); // turn on IAP IOS
            proj.AddFrameworkToProject(target,"iAd.framework",false);// ThuanTQ Add framework for Appsflyer tracking search ads
            File.WriteAllText(projPath, proj.WriteToString());

        }
#endif
    }
}