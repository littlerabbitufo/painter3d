﻿using UnityEditor;
using UnityEditor.Build;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Amanotes.Utils;
class IOSPreProcessingBuild : IPreprocessBuild
{
    private const string PROJECT_SETTING_FILE_PATH = "AmanotesSDK/BuildGame/build_setting.json";
	[MenuItem("Build/Build IOS")]
    public static void BuildIOS()
    {
#if !UNITY_IOS
        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.iOS,BuildTarget.iOS);
        OnPreprocessBuildIOS();
#endif
        string pathFolder=EditorUtility.OpenFolderPanel("Choose export folder","","");
        if(!string.IsNullOrEmpty(pathFolder)){        
            BuildPipeline.BuildPlayer(GetScenes(),pathFolder,BuildTarget.iOS, BuildOptions.AcceptExternalModificationsToPlayer);
        }
    }
    
	static string[] GetScenes()
    {
        return EditorBuildSettings.scenes.Where(s => s.enabled).Select(s => s.path).ToArray();
    }


	public int callbackOrder { get { return 0; } }
	public void OnPreprocessBuild(BuildTarget target, string path) {
		Debug.LogWarning("OnPreprocessBuild");
		// Do the preprocessing here
		#if UNITY_IPHONE
		OnPreprocessBuildIOS();
		#elif UNITY_ANDROID
		AndroidPreProcessingBuildTool.SettingAndroid();
		#endif
	}
	public static void OnPreprocessBuildIOS(){
		Debug.LogWarning("OnPreprocessBuildIOS");
		PlayerSettings.SplashScreen.show=false;
		PlayerSettings.SplashScreen.showUnityLogo=false;
		string path = Application.dataPath + "/" + PROJECT_SETTING_FILE_PATH;
		var sourse = new StreamReader(path);
		string fileContents = sourse.ReadToEnd();
		sourse.Close();
		InitialSettingData buildsetting = JsonUtility.FromJson<InitialSettingData>(fileContents);
        PlayerSettings.iOS.appleDeveloperTeamID = buildsetting.ios_team_developer_id;
		
		Debug.LogWarning("OnPreprocessBuildIOS Finish");
        AssetDatabase.SaveAssets();
	}
	public static string GetBuildNumber()
    {
        DateTime dt = DateTime.Now;
        int min = dt.Minute;
        return string.Format("{0}{1}{2}{3}{4}",
            dt.Year.ToString().Substring(2),
            dt.Month.ToString("00"),
            dt.Day.ToString("00"),
            dt.Hour.ToString("00"),
            (min).ToString("00"));

    }
}
