/// OneP
/// Strong.D v1.1.2
///
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor; //Note, this script must reside in a folder called 'Editor' or the compilation will fail at this point!
using UnityEditor.Callbacks;
using UnityEngine;
using System.Diagnostics;
using Amanotes.Utils;
using System.IO;

public class AndroidPreProcessingBuildTool
{
    private const string PROJECT_SETTING_FILE_PATH = "AmanotesSDK/BuildGame/build_setting.json";
    private static InitialSettingData buildsetting=null;
    private string savePath=string.Empty;
    [PostProcessBuild]
    public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
    {
        #if UNITY_ANDROID
        if(pathToBuiltProject.Contains("apk")){
            string nameExport= Path.GetFileNameWithoutExtension(pathToBuiltProject);
            string pathExport=Path.GetDirectoryName(pathToBuiltProject)+"/";
            string pathProject=Application.dataPath.Substring(0,Application.dataPath.Length-6);
            
            string srcX86= pathProject+"Temp/gradleOut/build/outputs/apk/release/x86-release.apk";
            string srcARM64= pathProject+"Temp/gradleOut/build/outputs/apk/release/arm64-v8a-release.apk";
            string srcARMV7= pathExport+nameExport+".apk";
            string desX86   = pathExport+nameExport+"-"+ PlayerSettings.bundleVersion+"-"+(PlayerSettings.Android.bundleVersionCode+2)+"-x86-release.apk";
            string desARM64 = pathExport+nameExport+"-"+ PlayerSettings.bundleVersion+"-"+(PlayerSettings.Android.bundleVersionCode+1)+"-arm64-v8a-release.apk";
            string desARMV7 = pathExport+nameExport+"-"+ PlayerSettings.bundleVersion+"-"+PlayerSettings.Android.bundleVersionCode+"-armeabi-v7a-release.apk";
            CopyFile(srcX86,desX86);
            CopyFile(srcARM64,desARM64);
            CopyFile(srcARMV7,desARMV7);
            string info="Build Successful\n";
            info+="\nARMV7: ";
            info+="\n"+desARMV7;
            info+="\nARM64bit: ";
            info+="\n"+desARM64;
            info+="\nX86: ";
            info+="\n"+srcX86;

            EditorUtility.DisplayDialog("Result",info
                ,"OK");
                return;
        }
        #endif

    }
    public static void CopyFile(string src,string des){
        //UnityEngine.Debug.LogError("CopyFile:"+src+","+des);
        if (System.IO.File.Exists(src)) {
             try
                {
                    if (File.Exists(des)) {// delete old first
                        System.IO.File.Delete(des);
                    }
                    System.IO.File.Copy(src, des,true);
                }
                catch(Exception ex) {
                    UnityEngine.Debug.LogError("Error Copy File:"+src+","+des);
                }
           
        }
        else{
            UnityEngine.Debug.LogWarning("Error Empty File:"+ src);
        }
    }

    [MenuItem("Build/BuildAndroid")]
    public static void BuildAndroid()
    {
        SettingAndroid();
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;
        OnStartExportAndroidProject();

    }

    public static void SettingAndroid(){
        UnityEngine.Debug.LogWarning("SettingAndroid");
        PlayerSettings.SplashScreen.show = false;
        PlayerSettings.SplashScreen.showUnityLogo = false;
    }
    private static void OnStartExportAndroidProject(){
        var path = EditorUtility.SaveFilePanel(
            "Save Apk",
            "",
            "",
            "apk");
        UnityEditor.Build.Reporting.BuildReport report= BuildPipeline.BuildPlayer(GetScenes(),path,
        BuildTarget.Android, BuildOptions.None);
        if(report.summary.result==UnityEditor.Build.Reporting.BuildResult.Succeeded)
        {
            UnityEngine.Debug.LogError("Build done:"+report.summary.result);
            
        }
        UnityEngine.Debug.LogError("Build done:"+report.summary.result);
    }
    
    static string[] GetScenes()
    {
        return EditorBuildSettings.scenes.Where(s => s.enabled).Select(s => s.path).ToArray();
    }

    static string GetBuildNumber()
    {
        DateTime dt = DateTime.Now;
        int min = dt.Minute;
        return string.Format("{0}{1}{2}{3}{4}",
            dt.Year.ToString().Substring(2),
            dt.Month.ToString("00"),
            dt.Day.ToString("00"),
            dt.Hour.ToString("00"),
            (min).ToString("00"));

    }
}