﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIDemoSceme : MonoBehaviour
{

    [SerializeField]
    GameObject adsDemo;

    [SerializeField]
    public GameObject IapDemo;

    [SerializeField]
    public GameObject analyticsDemo;

    private bool isShowGM=false;

    public void Start(){
        adsDemo.SetActive(false);
        IapDemo.SetActive(false);
        analyticsDemo.SetActive(false);
    }
    #region Action button click

    public void OnClickAdsScene()
    {
        adsDemo.SetActive(true);
    }

    public void OnClickIapScene()
    {
        
        IapDemo.SetActive(true);
    }

    public void OnClickAnalyticScene()
    {
        analyticsDemo.SetActive(true);
    }
    public void OnShowHideGM(){
        isShowGM=!isShowGM;
        GameInitialManager.Instance.ShowGM(isShowGM);
    }
    #endregion
}
