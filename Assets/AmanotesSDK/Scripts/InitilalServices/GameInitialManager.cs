﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Amanotes.Utils;

[RequireComponent(typeof(GlobalData))]
public class GameInitialManager : SingletonMono<GameInitialManager>
{
    [SerializeField]
    private TextAsset settingJson;

     [SerializeField]
    private GameObject gmCommand;

    private InitialSettingData initSettingData=null;
    private bool errorConfig = false;
    public InitialSettingData InitSettingData{
        get { return initSettingData; }
    }


    public void Start()
    {
        if (settingJson == null) {
            errorConfig = true;
        }
        else
        {
            try{
                initSettingData = JsonUtility.FromJson<InitialSettingData>(settingJson.text);
                if (initSettingData == null) {
                    errorConfig = true;
                }
            }
            catch(System.Exception ex) {
                Debug.LogError("Ama GameInitialManager Exception:" + ex.Message);
                errorConfig = true;

            }
        }

        DoOnMainThread.Instance.Initial();
        //Initial Firebase
        FirebaseManager.instance.Init();



        //Initial remain game services
        StartCoroutine(RoutineInitService());
         if(gmCommand!=null){
             gmCommand.SetActive(false);
         }
    }

    private IEnumerator RoutineInitService(){
        //waiting for other service awake
        yield return new WaitForSeconds(0.1f);

        if (errorConfig) {
            ShowLog("Ama Cannot Initial any Service dueto Error setting Json, please drag BuildGame/BuildSetting.json to GameInitialManager->settingJson");
        }
        else
        {
            InitialService();
        }

    }

    public void ShowGM(bool isShow){
        if(gmCommand!=null){
            gmCommand.SetActive(isShow);
        }
    }
    private void InitialService()
    {
        ShowLog("GameInitialManager:InitialService");
        Development.SetActiveLog(initSettingData.enable_log_analytics);
        //ShowGM(initSettingData.enable_gm_tool);
        //init ads
        string keyIronsource = "";

#if UNITY_ANDROID
         keyIronsource = InitSettingData.ironsrc_android_key;
        //keyIronsource =  FirebaseManager.Instance.GetStringValueRemoteConfig("ironsrc_android_key");
#elif UNITY_IOS
         keyIronsource = InitSettingData.ironsrc_ios_key;
        //keyIronsource = FirebaseManager.Instance.GetStringValueRemoteConfig("ironsrc_ios_key");
#endif

        Development.Log("==> firebase remote key ironsoucre "+keyIronsource);
        AdHelper.Instance.Initialize(keyIronsource, UserPropertiesTracker.Instance);

        //init appsflyer
        AppsFlyerService.Instance.Init();

        // init iap
        InitIAPService();

        // initial analtics parameter -> user properties...
        ShowLog("Finish Initilal all third party services");
    }

    #region Initial IAP Item
    private void InitIAPService()
    {
        IAPService.Instance.Init(  InitSettingData.consumable_product_ids
                                 , InitSettingData.nonconsumable_product_ids
                                 , InitSettingData.subcription_product_ids
                                       , (onComplete) =>
       {
            int count1 = 0;
            int count2 = 0;
            int count3 = 0;
            if (InitSettingData.consumable_product_ids != null)
            {
                count1 = InitSettingData.consumable_product_ids.Count;
            }
            if (InitSettingData.nonconsumable_product_ids != null)
            {
                count2 = InitSettingData.nonconsumable_product_ids.Count;
            }
            if (InitSettingData.subcription_product_ids != null)
            {
                count3 = InitSettingData.subcription_product_ids.Count;
            }

            ShowLog("InitIAPService:" + onComplete + ", comsumable=" + count1 + ", non-comsumable=" + count2 + ", subcription=" + count3);

       });
    }
    #endregion

    #region Log Util
    public void ShowLog(string log){
        if(InitSettingData!=null&&InitSettingData.enable_log_analytics){
            Debug.LogWarning("GameInitialManager->"+log);
        }
    }

    public bool IsCanShowLog{
        get{
            if(InitSettingData==null){
                return true;
            }
            else if(InitSettingData!=null&&InitSettingData.enable_log_analytics){
                return true;
            }
            return false;
        }
    }
    #endregion
}

