﻿
public enum MessageBusType
{
    NONE,
    XPChanged,      // in case User XP level Change
	DiamondChanged,  // in case User Diamod Change

    UserLevelChanged, // in case User Level Change
    SongBought,       // in case User Bought success a Song
    UserInfoUpdated,  // in case User Update Profile Information 
    LanguageChanged,  //in case User Change language
    ComebackFromPushWithExtraData, // in case User comeback game from push
    

    ShowHideTransition,  // in transition screen
    ShowHideLoadingIcon, // in transition screen
    ProgressLoading,     // in transition screen

    LoadingContentFileFinish,   // in case load content success
    IOSSelectMusicSongSuccess,  // in case load local IOS song success
    IOSSelectMusicSongFailed,   // in case load local IOS song failed
    RefreshIAPData,             // incase Refresh IAP
}