﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class IAPManagerDemo : MonoBehaviour
{

    [SerializeField] private int gem = 0;
    [SerializeField] private Text txtGem;
    [SerializeField] private Text txtGem500;
    [SerializeField] private Text txtGem1500;
    [SerializeField] private Text txtGem2000;
    [SerializeField] private Text txtRemoveAds;

    public List<string> ConsumableProductIDs = new List<string>();
    public List<string> NonConsumableProductIDs = new List<string>();
    public List<string> SubscriptionProductIDs = new List<string>();

    /// <summary>
    /// Init package iap 3 type 
    /// Consumable in list ConsumableProductIDs
    /// NonConsumable in list NonConsumableProductIDs
    /// Subscription in list SubscriptionProductIDs
    /// </summary>

    public void OnEnable()
    {

        this.ConsumableProductIDs = GameInitialManager.Instance.InitSettingData.consumable_product_ids;
        this.NonConsumableProductIDs = GameInitialManager.Instance.InitSettingData.nonconsumable_product_ids;
        this.SubscriptionProductIDs = GameInitialManager.Instance.InitSettingData.subcription_product_ids;


        Development.Log("[IAPManagerDemo] Item Count " + this.ConsumableProductIDs.Count + "  " + this.NonConsumableProductIDs.Count + "   " + this.SubscriptionProductIDs.Count);

        if(IAPService.Instance.IsInitSuccess){
            string price500 = "";
            string price1500 = "";
            string price2000 = "";
            string priceRemoveAds = "";
            if (ConsumableProductIDs.Count > 0)
            {
                price500 = IAPService.Instance.GetLocalPriceString(ConsumableProductIDs[0], "$0.99");
            }
            if (ConsumableProductIDs.Count > 1)
            {
                price1500 = IAPService.Instance.GetLocalPriceString(ConsumableProductIDs[1], "$0.99");
            }
            if (ConsumableProductIDs.Count > 2)
            {
                price2000 = IAPService.Instance.GetLocalPriceString(ConsumableProductIDs[2], "$0.99");
            }

            if (NonConsumableProductIDs.Count > 0)
            {
                priceRemoveAds = IAPService.Instance.GetLocalPriceString(NonConsumableProductIDs[0], "$0.99");
            }
            this.txtGem500.text = price500.ToString();
            this.txtGem1500.text = price1500.ToString();
            this.txtGem2000.text = price2000.ToString();
            this.txtRemoveAds.text = priceRemoveAds.ToString();
        }
        else{
            Development.Log("IAPManagerDemo: not Initial IAP yet",LogType.Error);
        }

        this.gem = 0;
        this.txtGem.text = this.gem.ToString();
    }

    #region action button click 

    public void OnClickBuy500()
    {
        Debug.LogError("[IapDemo: ] " + "on touch 50");
        if (ConsumableProductIDs.Count > 0)
        {
            IAPService.Instance.BuyItemWithProductID(ConsumableProductIDs[0], OnCompleteBuy500);
        }
    }

    public void OnCompleteBuy500(bool isSuccess)
    {
        Debug.LogError("[IapDemo: ] " + "buy iap consumable " + isSuccess);
        if (isSuccess)
        {
            this.gem += 500;
            this.txtGem.text = this.gem.ToString();
        }
        else
        {

        }
    }

    public void OnClickBuy1500()
    {
        Debug.LogError("[IapDemo: ] " + "on touch `1500");
        if (ConsumableProductIDs.Count > 1)
        {
            IAPService.Instance.BuyItemWithProductID(ConsumableProductIDs[1], OnCompleteBuy1500);
        }
    }


    public void OnCompleteBuy1500(bool isSuccess)
    {
        Debug.LogError("[IapDemo: ] " + "buy iap consumable " + isSuccess);
        if (isSuccess)
        {
            this.gem += 1500;
            this.txtGem.text = this.gem.ToString();
        }
        else
        {

        }
    }


    public void OnClickBuy2000()
    {
        Debug.LogError("[IapDemo: ] " + "on touch 2000");
        if (ConsumableProductIDs.Count > 2)
        {
            IAPService.Instance.BuyItemWithProductID(ConsumableProductIDs[2], OnCompleteBuy2000);
        }
    }

    public void OnCompleteBuy2000(bool isSuccess)
    {
        Debug.LogError("[IapDemo: ] " + "buy iap consumable " + isSuccess);
        if (isSuccess)
        {
            this.gem += 2000;
            this.txtGem.text = this.gem.ToString();
        }
        else
        {

        }
    }


    public void OnClickBuyRemoveAds()
    {
        Debug.LogError("[IapDemo: ] " + "on touch remove ads");
        if (NonConsumableProductIDs.Count > 0)
        {
            IAPService.Instance.BuyItemWithProductID(NonConsumableProductIDs[0], OnCompleteBuyRemoveAds);
        }

    }

    public void OnCompleteBuyRemoveAds(bool isSuccess)
    {
        Debug.LogError("[IapDemo: ] " + "buy iap non consumable " + isSuccess);
        if (isSuccess)
        {
            this.gem -= 100;
            this.txtGem.text = this.gem.ToString();

            //save remove Ads 
        }
        else
        {

        }
    }

    public void OnClickBack()
    {
        this.gameObject.transform.parent.gameObject.SetActive(false);
    }
    #endregion
}
