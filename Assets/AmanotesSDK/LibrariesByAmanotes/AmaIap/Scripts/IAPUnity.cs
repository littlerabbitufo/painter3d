﻿


//=================================INSTRUCTION======================================================
// Create by R&D team amanotes
// 1.Enable Unity IAP service of current project
// 2.Make sure you already add *RECEIPT_VALIDATION* to Scripting define sysmbol if you want to safe transaction 
// 3.Fill receipt validation of Unity IAP. Window->IAPUnity->Receipt Validation Obfuscator
// 4.Drag IAPService prefab to your scene
//==================================================================================================

using UnityEngine;

using System;
using System.Collections.Generic;
#if UNITY_IAP
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
#endif

namespace com.amanotes.iap
{
    #if UNITY_IAP
    public class IAPUnity : MonoBehaviour, IStoreListener
    {

        // The Unity Purchasing system.
        private static IStoreController m_StoreController;

        // The store specific Purchasing subsystems.
        private static IExtensionProvider m_StoreExtensionProvider;

        readonly string ERROR_PRODUCT_NOT_FOUND = "Product not found";
        readonly string ERROR_PRODUCT_NOT_AVAILABLE = "Product not available";
        readonly string ERROR_UNITY_IAP_NOT_INIT = "Unity IAP not initialized";
        readonly string RESTORE_OK = "Restore purchased finished";
        readonly string RESTORE_NOT_FOUND = "Not exist product to restore";
        readonly string RESTORE_NOT_CORRECT_OS = "Not supported on this platform";

        public Action<bool, string, string> onFinishPurchase;
        public Action<bool, string> onFinishAppleRestore;
        public Action<bool> onFinishInit;


        public List<string> listProductIDConsumable = new List<string>();
        public List<string> listProductIDNonConsumable = new List<string>();
        public List<string> listSubscriptionId = new List<string>();

        private Dictionary<string, DateTime> dicGooglePurchasedDate = new Dictionary<string, DateTime>();

    #if RECEIPT_VALIDATION && UNITY_IAP
        private CrossPlatformValidator validator;
    #endif

        private static IAPUnity instance;
        public static IAPUnity Instance
        {
            get
            {
                return instance;
            }
        }

        void Awake()
        {
            instance = this;
        }

        /// <param name="listProductIDConsumable">List product identifier consumable.</param>
        /// <param name="listProductIDNonConsumable">List product identifier non consumable.</param>
        /// <param name="listSubscriptionId">List product identifier subscription.</param>

        public void Init(List<string> listProductIDConsumable = null, List<string> listProductIDNonConsumable = null, List<string> listSubscriptionId = null)
        {
            // If we haven't set up the Unity Purchasing reference
            if (m_StoreController == null)
            {
                // Begin to configure our connection to Purchasing
                InitializePurchasing(listProductIDConsumable, listProductIDNonConsumable, listSubscriptionId);
            }
        }

        private void InitializePurchasing(List<string> listProductIDConsumable, List<string> listProductIDNonConsumable, List<string> listSubscriptionId)
        {
            // If we have already connected to Purchasing ...
            if (IsInitialized())
            {
                return;
            }
            #if UNITY_IAP
            // Create a builder, first passing in a suite of Unity provided stores.
            ConfigurationBuilder builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

            // Adding the consumable product.
            if (listProductIDConsumable != null)
            {
                this.listProductIDConsumable = listProductIDConsumable;
                for (int i = 0; i < listProductIDConsumable.Count; i++)
                {
                    builder.AddProduct(listProductIDConsumable[i], UnityEngine.Purchasing.ProductType.Consumable);
                }
            }

            // Adding the consumable product
            if (listProductIDNonConsumable != null)
            {
                this.listProductIDNonConsumable = listProductIDNonConsumable;
                for (int i = 0; i < listProductIDNonConsumable.Count; i++)
                {
                    builder.AddProduct(listProductIDNonConsumable[i], UnityEngine.Purchasing.ProductType.NonConsumable);
                }
            }

            // Adding the Subscription product
            if (listSubscriptionId != null)
            {
                this.listSubscriptionId = listSubscriptionId;
                for (int i = 0; i < listSubscriptionId.Count; i++)
                {
                    builder.AddProduct(listSubscriptionId[i], UnityEngine.Purchasing.ProductType.Subscription);
                }
            }

    #if RECEIPT_VALIDATION
            string appIdentifier = string.Empty;
    #if UNITY_5_6_OR_NEWER
            appIdentifier = Application.identifier;
    #else
            appIdentifier = Application.bundleIdentifier;
    #endif
            validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), UnityChannelTangle.Data(), appIdentifier);
    #endif
            UnityPurchasing.Initialize(this, builder);
            #endif
        }

        private bool IsInitialized()
        {
            // Only say we are initialized if both the Purchasing references are set.
            return m_StoreController != null && m_StoreExtensionProvider != null;
        }

        public void BuyProductID(string productId)
        {
            // If Purchasing has been initialized ...
            if (IsInitialized())
            {
                // Look up the Product reference with the general product identifier 
                UnityEngine.Purchasing.Product product = m_StoreController.products.WithID(productId);

                // If the look up found a product for this device's store and that product is ready to be sold ... 
                if (product != null && product.availableToPurchase)
                {
                    // Buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                    m_StoreController.InitiatePurchase(product);
                }
                else
                {
                    if (this.onFinishPurchase != null)
                    {
                        if (product == null)
                        {
                            this.onFinishPurchase(false, string.Empty, ERROR_PRODUCT_NOT_FOUND);
                        }
                        else if (product.availableToPurchase == false)
                        {
                            this.onFinishPurchase(false, string.Empty, ERROR_PRODUCT_NOT_AVAILABLE);
                        }
                    }
                }
            }
            else
            {
                // Purchasing has not succeeded initializing yet
                Debug.Log("[IAPUnity: ] BuyProductID FAIL.Because IAP was not initialized.");
                if (this.onFinishPurchase != null)
                {
                    this.onFinishPurchase(false, string.Empty, ERROR_UNITY_IAP_NOT_INIT);
                }
            }
        }

        // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google.
        // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
        /// <summary>

        public void RestorePurchases()
        {
            #if UNITY_IAP
            // If Purchasing has not yet been set up ...
            if (!IsInitialized())
            {
                Debug.Log("[IAPUnity: ] RestorePurchases FAIL. Unity IAP Not initialized.");
                if (this.onFinishAppleRestore != null)
                {
                    this.onFinishAppleRestore(false, ERROR_UNITY_IAP_NOT_INIT);
                }
                return;
            }

            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                // Fetch the Apple store-specific subsystem.
                IAppleExtensions apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();

                // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
                // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
                apple.RestoreTransactions((result) =>
                {
                    // iOS Specific.
                    // This is called as part of Apple's 'Ask to buy' functionality,
                    // When a purchase is requested by a minor and referred to a parent for approval.
                    // When the purchase is approved or rejected, the normal purchase events will fire.
                    if (this.onFinishAppleRestore != null)
                    {
                        if (result)
                        {
                            this.onFinishAppleRestore(true, RESTORE_OK);
                        }
                        else
                        {
                            this.onFinishAppleRestore(false, RESTORE_NOT_FOUND);
                        }
                    }
                });
            }
            else
            {
                Debug.LogWarning("Restore on this platform not work");
            }
            #endif
        }

        //=======================================================================================
        // NATIVE CALL
        // Called when Unity IAP has retrieved all product metadata and is ready to make purchases.
        // Parameters:
        //   controller: Access cross-platform Unity IAP functionality.
        //   extensions: Access store-specific Unity IAP functionality.
        //=======================================================================================
        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {

            // Overall Purchasing system, configured with products for this application.
            m_StoreController = controller;
            // Store specific subsystem, for accessing device-specific store features.
            m_StoreExtensionProvider = extensions;
    #if IOS        
            // On Apple platforms we need to handle deferred purchases caused by Apple's Ask to Buy feature.
            // On non-Apple platforms this will have no effect; OnDeferred will never be called.
            extensions.GetExtension<IAppleExtensions>().RegisterPurchaseDeferredListener(OnDeferred);
    #endif

            if (this.onFinishInit != null)
            {
                this.onFinishInit(true);
            }


    #if SUBSCRIPTION_MANAGER
            Dictionary<string, string> introductory_info_dict = extensions.GetExtension<IAppleExtensions>().GetIntroductoryPriceDictionary();
    #endif

            bool isReceiptAll = false;

            foreach (var item in m_StoreController.products.all)
            {
                if (item.availableToPurchase)
                {
    #if INTERCEPT_PROMOTIONAL_PURCHASES
                    // Set all these products to be visible in the user's App Store according to Apple's Promotional IAP feature
                    // https://developer.apple.com/library/content/documentation/NetworkingInternet/Conceptual/StoreKitGuide/PromotingIn-AppPurchases/PromotingIn-AppPurchases.html
                    m_AppleExtensions.SetStorePromotionVisibility(item, AppleStorePromotionVisibility.Show);
    #endif

    #if SUBSCRIPTION_MANAGER
                    // this is the usage of SubscriptionManager class
                    if (item.receipt != null)
                    {
                        if (item.definition.type == ProductType.Subscription)
                        {
                            if (checkIfProductIsAvailableForSubscriptionManager(item.receipt))
                            {
                                string intro_json = (introductory_info_dict == null || !introductory_info_dict.ContainsKey(item.definition.storeSpecificId)) ? null : introductory_info_dict[item.definition.storeSpecificId];
                                SubscriptionManager p = new SubscriptionManager(item, intro_json);
                                SubscriptionInfo info = p.getSubscriptionInfo();
                                Result result = info.isCancelled();

                                if (result == Result.True || info.isExpired() == Result.True || info.isSubscribed() == Result.False)
                                {
                                    // iap was canceled
                                }
                                else
                                {
                                    isReceiptAll = true;
                                }

                            }
                            else
                            {
                                Debug.Log("[IAPUnity: ] This product is not available for SubscriptionManager class, only products that are purchase by 1.19+ SDK can use this class.");
                            }
                        }
                        else
                        {
                            Debug.Log("[IAPUnity: ] the product is not a subscription product");
                        }
                    }
                    else
                    {
                        Debug.Log("[IAPUnity: ] the product should have a valid receipt");
                    }

                    if (!isReceiptAll)
                    {
                        Debug.Log("[IAPUnity: ] the product should have cancel");
                    }
    #endif
                }
            }


        }

    #if SUBSCRIPTION_MANAGER
        private bool checkIfProductIsAvailableForSubscriptionManager(string receipt)
        {
            var receipt_wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(receipt);
            if (!receipt_wrapper.ContainsKey("Store") || !receipt_wrapper.ContainsKey("Payload"))
            {
                Debug.Log("[IAPUnity: ] The product receipt does not contain enough information");
                return false;
            }
            var store = (string)receipt_wrapper["Store"];
            var payload = (string)receipt_wrapper["Payload"];

            if (payload != null)
            {
                switch (store)
                {
                    case "GooglePlay":
                        {
                            var payload_wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(payload);
                            if (!payload_wrapper.ContainsKey("json"))
                            {
                                Debug.Log("[IAPUnity: ] The product receipt does not contain enough information, the 'json' field is missing");
                                return false;
                            }
                            var original_json_payload_wrapper = (Dictionary<string, object>)MiniJson.JsonDecode((string)payload_wrapper["json"]);
                            if (original_json_payload_wrapper != null && !original_json_payload_wrapper.ContainsKey("developerPayload"))
                            {
                                Debug.Log("[IAPUnity: ] The product receipt does not contain enough information, the 'developerPayload' field is missing");
                                return false;
                            }
                            var developerPayloadJSON = (string)original_json_payload_wrapper["developerPayload"];
                            var developerPayload_wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(developerPayloadJSON);
                            if (developerPayload_wrapper == null || !developerPayload_wrapper.ContainsKey("is_free_trial") || !developerPayload_wrapper.ContainsKey("has_introductory_price_trial"))
                            {
                                Debug.Log("[IAPUnity: ] The product receipt does not contain enough information, the product is not purchased using 1.19 or later");
                                return false;
                            }
                            return true;
                        }
                    case "AppleAppStore":
                    case "MacAppStore":
                        {
                            return true;
                        }
                    default:
                        {
                            return false;
                        }
                }
            }
            return false;
        }
    #endif


        //=======================================================================================
        // NATIVE CALL
        // Note that Unity IAP will not call this method if the device is offline, but continually
        // attempt initialization until online.
        //=======================================================================================
        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Debug.Log("[IAPUnity: ] Unity IAP Init Failure Reason: " + error);
            if (this.onFinishInit != null)
            {
                this.onFinishInit(false);
            }
        }

        //=======================================================================================
        // NATIVE CALL
        // Called when a purchase fails.
        //=======================================================================================
        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            // A product purchase attempt did not succeed. Check failureReason for more detail
            // this reason with the user to guide their troubleshooting actions.
            //Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
            if (this.onFinishPurchase != null)
            {
    #if UNITY_ANDROID
                if (failureReason.Equals(PurchaseFailureReason.DuplicateTransaction))
                {
                    // because we need restore for user when user had this item(nom-consumable) already so we return product id to restore
                    onFinishPurchase(false, product.definition.storeSpecificId, failureReason.ToString());
                }
                else
                {
                    onFinishPurchase(false, string.Empty, failureReason.ToString());
                }
    #else
                onFinishPurchase(false, string.Empty, failureReason.ToString());
    #endif
            }
        }

        //=======================================================================================
        // NATIVE CALL
        // Called when a purchase succeeds.
        // Parameters:
        //   args: The purchase details.
        //=======================================================================================
        #pragma warning disable
        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {

            string productID = args.purchasedProduct.definition.id;
    #if UNITY_EDITOR
            if (this.listProductIDConsumable.Contains(productID)
                || this.listProductIDNonConsumable.Contains(productID)
                || this.listSubscriptionId.Contains(productID))
            {
                if (this.onFinishPurchase != null)
                {
                    this.onFinishPurchase(true, productID, "purchase succeeded");
                }
            }
            else
            {

                if (this.onFinishPurchase != null)
                {
                    this.onFinishPurchase(false, string.Empty, ERROR_PRODUCT_NOT_FOUND);
                }
            }
            return PurchaseProcessingResult.Complete;
    #endif

    #if RECEIPT_VALIDATION
            if (Application.platform == RuntimePlatform.Android
                || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                try
                {
                    IPurchaseReceipt[] result = this.validator.Validate(args.purchasedProduct.receipt);
                    Debug.Log("[IAPUnity: ] Receipt is valid: " + productID + ". Contents:");
                    foreach (IPurchaseReceipt productReceipt in result)
                    {

    #if UNITY_ANDROID
                        GooglePlayReceipt google = productReceipt as GooglePlayReceipt;
                        if (null != google)
                        {

                            if (string.Equals(google.productID, productID))
                            {
                                if (google.purchaseState == GooglePurchaseState.Purchased)
                                {
                                    dicGooglePurchasedDate[productID] = google.purchaseDate;
                                    if (onFinishPurchase != null)
                                    {
                                        onFinishPurchase(true, productID, "purchase succeeded");
                                    }
                                }
                                break;
                            }
                        }
    #elif UNITY_IOS
                        if (string.Equals(productReceipt.productID, productID))
                        {
                            AppleInAppPurchaseReceipt apple = productReceipt as AppleInAppPurchaseReceipt;
                            if (null != apple)
                            {
                                //Debug.Log(apple.originalTransactionIdentifier);
                                //Debug.Log(apple.subscriptionExpirationDate);
                                //Debug.Log(apple.cancellationDate);
                                //Debug.Log(apple.quantity);
                                if (onFinishPurchase != null)
                                {
                                    onFinishPurchase(true, productID, "purchase succeeded");
                                }
                            }
                            else
                            {
                                Debug.Log("Invalid receipt + " + productID + ", not unlocking content");
                            }
                            break;
                        }
    #endif
                    }
                }
                catch (IAPSecurityException)
                {
                    Debug.Log("[IAPUnity: ] Invalid receipt, not unlocking content");
                    return PurchaseProcessingResult.Complete;
                }
            }
    #endif

            // Return a flag indicating whether this product has completely been received, or if the application needs 
            // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
            // saving purchased products to the cloud, and when that save is delayed. 
            return PurchaseProcessingResult.Complete;
        }
        #pragma warning restore

        /// <summary>
        /// iOS Specific.
        /// This is called as part of Apple's 'Ask to buy' functionality,
        /// when a purchase is requested by a minor and referred to a parent
        /// for approval.
        ///
        /// When the purchase is approved or rejected, the normal purchase events
        /// will fire.
        /// </summary>
        /// <param name="item">Item.</param>
        private void OnDeferred(UnityEngine.Purchasing.Product item)
        {
            Debug.Log("[IAPUnity: ] Purchase deferred: " + item.definition.id);
        }

        public bool IsPurchased(string productId, Action<bool> isPurchased = null)
        {
            if (!IsInitialized())
                return false;
            string transactionId = null;
            return IsPurchased(productId, out transactionId, isPurchased);
        }

        public bool IsPurchased(string productId, out string transactionId, Action<bool> isPurchased = null)
        {
            UnityEngine.Purchasing.Product product = m_StoreController.products.WithID(productId);
            if (product == null)
            {
                Debug.Log("[IAPUnity: ] Null product: " + productId);
                transactionId = null;

                if (isPurchased != null)
                {
                    isPurchased(false);
                }

                return false;
            }

    #if UNITY_ANDROID && RECEIPT_VALIDATION
            if (product.hasReceipt)
            {
                IPurchaseReceipt[] result = validator.Validate(product.receipt);
                foreach (IPurchaseReceipt productReceipt in result)
                {
                    if (string.Equals(productReceipt.productID, productId))
                    {
                        GooglePlayReceipt google = productReceipt as GooglePlayReceipt;
                        if (google != null)
                        {
                            if (google.purchaseState == GooglePurchaseState.Purchased)
                            {
                                Debug.Log("[IAPUnity: ] Product: " + productId + " PurchasedDate: " + google.purchaseDate);
                                transactionId = google.transactionID;
                                if (isPurchased != null)
                                {
                                    isPurchased(true);
                                }
                                return true;
                            }
                        }
                    }
                }
            }
            transactionId = null;
            return false;
    #else
            transactionId = null;
            return product.hasReceipt;
    #endif
        }

        public bool HasReceipt(string productId)
        {
            UnityEngine.Purchasing.Product product = m_StoreController.products.WithID(productId);
            if (product != null)
            {
                return product.hasReceipt;
            }
            else
            {
                Debug.Log("[IAPUnity: ] Null product: " + productId);
                return false;
            }
        }

        public void CheckGoogleSubscription(string productId, out DateTime purchasedDate)
        {
            //ERROR: null reference if user renew product
            if (dicGooglePurchasedDate.ContainsKey(productId))
            {
                purchasedDate = dicGooglePurchasedDate[productId];
            }
            else
            {
                purchasedDate = DateTime.MinValue;
            }
        }

        public string GetReceipt()
        {
            string receipt = "";
            #if UNITY_IAP
            ConfigurationBuilder builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            
    #if UNITY_ANDROID
            receipt = builder.Configure<IGooglePlayConfiguration>().ToString();

    #elif UNITY_IOS
            receipt= builder.Configure<IAppleConfiguration>().appReceipt;
    #endif
            #endif
            return receipt;
        }

        public bool CanMakePayments()
        {
            #if UNITY_IAP
            ConfigurationBuilder builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            bool canMakePayments = builder.Configure<IAppleConfiguration>().canMakePayments;
            return canMakePayments;
            #else
            return false;
            #endif

        }

        public delegate void Action<T1, T2, T3, T4, T5>(T1 arg, T2 arg2, T3 arg3, T4 arg4, T5 arg5);
        //	public delegate void Action<productId, purchasedDate, expireDate, cancelDate, transactionId> SubscriptionAction(string, DateTime, DateTime, DateTime, string);
        public Action<string, DateTime, DateTime, DateTime, string> subscriptionDate;

        public void CheckSubscriptionReceipt()
        {
    #if UNITY_EDITOR
            return;
    #endif

    #if UNITY_IOS
            string receiptCode = GetReceipt();
            //Debug.Log("Code: " + receiptCode);
            byte[] receiptData = System.Convert.FromBase64String(receiptCode);
            AppleReceipt receipt = new AppleValidator(AppleTangle.Data()).Validate(receiptData);
            for (int i = receipt.inAppPurchaseReceipts.Length - 1; i > receipt.inAppPurchaseReceipts.Length - 3 && i >= 0; i--) {
                Debug.Log("CheckReceipt: " + receipt.inAppPurchaseReceipts[i].subscriptionExpirationDate.ToString() + " - " 
                    + receipt.inAppPurchaseReceipts[i].productID + " - transactionId: "
                    + receipt.inAppPurchaseReceipts[i].transactionID);
                if (subscriptionDate != null) {
                    subscriptionDate(
                        receipt.inAppPurchaseReceipts[i].productID,
                        receipt.inAppPurchaseReceipts[i].purchaseDate,
                        receipt.inAppPurchaseReceipts[i].subscriptionExpirationDate,
                        receipt.inAppPurchaseReceipts[i].cancellationDate,
                        receipt.inAppPurchaseReceipts[i].transactionID);
                }
            }
    #endif
        }

        public void RefreshAppleReceipt()
        {
            // Refresh Apple receipt
            #if UNITY_IAP
            m_StoreExtensionProvider.GetExtension<IAppleExtensions>().RefreshAppReceipt((success) =>
            {
                Debug.Log("[IAPUnity: ] Refresh receipt success");
                CheckSubscriptionReceipt();
            }, () =>
            {
                Debug.Log("[IAPUnity: ] Refresh receipt error");
            });
            #endif
        }

        public void RefreshGoogleReceipt()
        {
            //TODO implement
        }

        UnityEngine.Purchasing.Product GetProduct(string productId)
        {
            return m_StoreController.products.WithID(productId); ;
        }

        public string GetTransactionID(string productId)
        {
            UnityEngine.Purchasing.Product product = GetProduct(productId);
            if (product != null)
            {
                return product.transactionID;
            }

            return string.Empty;
        }

        public float GetProductPrice(string productId)
        {
            return (float)GetProductPriceDecimal(productId);
        }

        public decimal GetProductPriceDecimal(string productId)
        {
            UnityEngine.Purchasing.Product product = GetProduct(productId);
            if (product != null)
            {
                return product.metadata.localizedPrice;
            }
            return -1.0m;
        }

        public string GetProductLocalPriceString(string productId)
        {
            UnityEngine.Purchasing.Product product = GetProduct(productId);
            if (product != null)
            {
                return product.metadata.localizedPriceString;
            }
            return "#ISO_code";
        }

        public string GetCurrencySymbol(string productId)
        {
            UnityEngine.Purchasing.Product product = GetProduct(productId);
            if (product != null)
            {
                return product.metadata.isoCurrencyCode;
            }

            return "#ISO_Symbol";
        }
    }
    #else
    public class IAPUnity : MonoBehaviour //fake for compile
    {

    }
    #endif
}