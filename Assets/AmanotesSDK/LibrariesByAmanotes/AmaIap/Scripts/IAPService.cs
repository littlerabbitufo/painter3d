﻿
using System.Collections.Generic;
using UnityEngine;

using System;
using Amanotes.Utils;

#if UNITY_IAP
using UnityEngine.Purchasing;
[RequireComponent(typeof(IAPUnity))]
#endif
public class IAPService : SingletonMono<IAPService>
{
    private bool isInitSuccess;
    public delegate void OnPurchaseSuccess(string id);
    public static OnPurchaseSuccess EventPurchaseSuccess;


    private Action<bool> onFinishBuyProccess;
    private Action<bool> onInitProccess;


    //==========Product ID===========
    //[SerializeField]
    private List<string> listConsumable;
    //[SerializeField]
    private List<string> listNonConsumable;
    //[SerializeField]
    private List<string> listsubcription;

    public bool IsInitSuccess
    {
        get
        {
            return isInitSuccess;
        }
    }
    public void SetListConsumableItem(List<String> _listConsumable)
    {
        listConsumable = _listConsumable;
    }
    public List<String> GetListConsumableItem()
    {
        return listConsumable;
    }

    public void SetListNonConsumable(List<String> _listNonConsumable)
    {
        listNonConsumable = _listNonConsumable;
    }

    public List<String> GetListNonConsumableItem()
    {
        return listNonConsumable;
    }

    public void SetListSubcriptionItem(List<String> _listsubcription)
    {
        listsubcription = _listsubcription;
    }

    public List<String> GetListSubcriptionItem()
    {
        return listsubcription;
    }
    //===============================



    #region public function

    public void InitIAPService()
    {
        IAPService.Instance.Init(listConsumable, listNonConsumable, listsubcription, (onComplete) =>
       {
           if (onComplete)
           {
               AnalyticService.Instance.LogEvent("iap_initial", new Dictionary<string, object> { { "Value", "Successed" } });
           }
           else
           {
               AnalyticService.Instance.LogEvent("iap_initial", new Dictionary<string, object> { { "Value", "Failed" } });

           }
       });
    }
    public void Init(List<string> listProductIDConsumable = null, List<string> listProductIDNonConsumable = null, List<string> listSubscriptionId = null
                     , Action<bool> onComplete = null)
    {
        if (isInitSuccess) return;

        #if UNITY_IAP
        // init store item
        IAPUnity.Instance.Init(listProductIDConsumable, listProductIDNonConsumable, listSubscriptionId);
        #endif

#if UNITY_EDITOR
        if (onComplete != null)
        {
            onComplete(true);
        }
        OnFinishInit(true);
#else
        // register action
        this.onInitProccess = onComplete;
#endif

        // register IAP event 
        #if UNITY_IAP
        IAPUnity.Instance.onFinishInit += OnFinishInit;
        IAPUnity.Instance.onFinishPurchase += OnFinishPurchase;
        IAPUnity.Instance.onFinishAppleRestore += OnFinishAppleRestore;
        #endif
        this.onFinishBuyProccess += OnFinishBuyProccess;

    }

    private void OnFinishBuyProccess(bool isSuccess)
    {
        if (isSuccess)
        {
            Debug.Log("IAP on finish buy process userpropertie");
            UserPropertiesTracker.Instance.AddIAPLtvCount();
        }
    }
    public void BuyItemWithProductID(string productId, Action<bool> onComplete = null)
    {
        #if UNITY_IAP
        IAPUnity.Instance.BuyProductID(productId);
        if (onComplete != null)
        {
            this.onFinishBuyProccess = onComplete;
        }
        #endif
    }

    /// <summary>
    /// Gets the local price string. Ex: $1.99
    /// </summary>

    public string GetLocalPriceString(string productId, string defaultPrice)
    {
        if (this.isInitSuccess == false)
        {
            return defaultPrice;
        }
        #if UNITY_IAP
        return IAPUnity.Instance.GetProductLocalPriceString(productId);
        #else
        return "";
        #endif
    }

    /// <summary>
    /// Restore for IOS platform
    /// </summary>

    public void RestorePurchased()
    {
        #if UNITY_IAP
        IAPUnity.Instance.RestorePurchases();
        #endif
    }

    public bool IsPurachaseWithProductID(string productID, Action<bool> isPurchased = null)
    {
        #if UNITY_IAP
            return IAPUnity.Instance.IsPurchased(productID, isPurchased);
        #else
            return false;
        #endif
    }
    #endregion

    #region Listener event IAP systems

    // Call when finish init 
    private void OnFinishInit(bool isComplete)
    {
        this.isInitSuccess = isComplete;
        if (this.onInitProccess != null)
        {
            this.onInitProccess(isComplete);
        }

    }

    // Call when finish apple restore
    private void OnFinishAppleRestore(bool result, string data)
    {

    }

    // Call when finish purchase
    private void OnFinishPurchase(bool result, string productID, string errorMessage)
    {
        PurchaseSucceededProcess(result, productID);
        if (result && EventPurchaseSuccess != null)
        {
            EventPurchaseSuccess(productID);
        }
        else
        {
            PurchaseFailureProccess(productID, errorMessage);
        }
    }

    private void PurchaseSucceededProcess(bool result, string productID)
    {
        if (this.onFinishBuyProccess != null)
        {
            this.onFinishBuyProccess(result);
        }
    }

    private void PurchaseFailureProccess(string productID, string errorMessage)
    {
        Debug.LogError("[IAPService : ] " + errorMessage);
#if UNITY_EDITOR
#elif UNITY_ANDROID && UNITY_IAP
        if (errorMessage.Equals(PurchaseFailureReason.DuplicateTransaction))
        {
            // This special case on Android platform
            // We need check whether this item is non-consumable
            if (IAPUnity.Instance.listProductIDNonConsumable.Contains(productID))
            {
                if (this.onFinishBuyProccess != null)
                {
                    this.onFinishBuyProccess(true);
                }
            }
            else
            {
                if (this.onFinishBuyProccess != null)
                {
                    this.onFinishBuyProccess(false);
                }
            }
        }
        else
        {
            if (this.onFinishBuyProccess != null)
            {
                this.onFinishBuyProccess(false);
            }
        }
#else
        if (this.onFinishBuyProccess != null)
        {
            this.onFinishBuyProccess(false);
        }
#endif


    }
    #endregion
}

