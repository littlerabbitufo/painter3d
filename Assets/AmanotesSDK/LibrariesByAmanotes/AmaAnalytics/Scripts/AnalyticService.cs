﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Amanotes.Utils;
using System;

public enum ANALYTIC_MODULE
{
    ALL,
    UNITY,
    FIREBASE,
    APPSFLYER,
    FACEBOOK,
    UNITY_FIREBASE,
    UNITY_APPFLYER,
    UNITY_FACEBOOK,
    UNITY_FIREBASE_APPSFLYER,
    UNITY_APPFLYER_FACEBOOK,
    UNITY_FACEBOOK_FIREBASE,
    ALL_EXCLUDE_APPSFLYER,
}

public class AnalyticService : SingletonMono<AnalyticService>
{
    [SerializeField] private bool isShowDebugLog = true;
    [SerializeField] private bool isEnableAnalytics = true;
   
        
    public void SetShowDebugLog() { 
    
    }
    private void OnEnable()
    {
      
    }

    private void OnAnalyticLogEvent(CustomEvent obj)
    {
        LogEvent(obj.eventName, obj.eventParams);
    }

    public void LogEvent(string customEventName, Dictionary<string, object> eventData = null, ANALYTIC_MODULE type = ANALYTIC_MODULE.ALL_EXCLUDE_APPSFLYER)
    {
        if (isEnableAnalytics == false) return;

        if (isShowDebugLog)// for QC Debuging
        {
            Debug.Log("==========EVENT NAME: " + customEventName + "=========");
            if (eventData != null)
            {
                Debug.Log("************************************************************************");
                foreach (KeyValuePair<string, object> item in eventData)
                {
                    Debug.Log("PARA NAME: " + item.Key + " - PARA VALUE: " + item.Value);
                }
                Debug.Log("************************************************************************");
            }
        }
        switch (type)
        {
            case ANALYTIC_MODULE.UNITY:
                UnityLogEvent(customEventName, eventData);
                break;

            case ANALYTIC_MODULE.FIREBASE:
                FirebaseLogEvent(customEventName, eventData);
                break;

            case ANALYTIC_MODULE.APPSFLYER:
                AppsFlyerLogEvent(customEventName, eventData);
                break;
            case ANALYTIC_MODULE.UNITY_APPFLYER:
                UnityLogEvent(customEventName, eventData);
                AppsFlyerLogEvent(customEventName, eventData);
                break;

            case ANALYTIC_MODULE.UNITY_APPFLYER_FACEBOOK:
                UnityLogEvent(customEventName, eventData);
                FacebookLogEvent(customEventName, eventData);
                AppsFlyerLogEvent(customEventName, eventData);
                break;

            case ANALYTIC_MODULE.UNITY_FACEBOOK:
                FacebookLogEvent(customEventName, eventData);
                break;

            case ANALYTIC_MODULE.UNITY_FACEBOOK_FIREBASE:
                UnityLogEvent(customEventName, eventData);
                FacebookLogEvent(customEventName, eventData);
                FirebaseLogEvent(customEventName, eventData);
                break;

            case ANALYTIC_MODULE.UNITY_FIREBASE:
                UnityLogEvent(customEventName, eventData);
                FirebaseLogEvent(customEventName, eventData);
                break;

            case ANALYTIC_MODULE.UNITY_FIREBASE_APPSFLYER:
                UnityLogEvent(customEventName, eventData);
                FirebaseLogEvent(customEventName, eventData);
                AppsFlyerLogEvent(customEventName, eventData);
                break;

            case ANALYTIC_MODULE.ALL_EXCLUDE_APPSFLYER:
                UnityLogEvent(customEventName, eventData);
                FacebookLogEvent(customEventName, eventData);
                FirebaseLogEvent(customEventName, eventData);
                break;

            case ANALYTIC_MODULE.ALL:
            default:
                {
                    UnityLogEvent(customEventName, eventData);
                    FirebaseLogEvent(customEventName, eventData);
                    FacebookLogEvent(customEventName, eventData);
                    AppsFlyerLogEvent(customEventName, eventData);
                }
                break;
        }
    }

    #region Facebook analytics related

    private void FacebookLogEvent(string eventName, Dictionary<string, object> eventData)
    {
        // Facebook Analytics
    }

    #endregion


    #region unity analytics related

    private void UnityLogEvent(string eventName, Dictionary<string, object> eventData)
    {
        UnityEngine.Analytics.Analytics.CustomEvent(eventName, eventData);
    }

    #endregion

    #region firebase analytic related

    private void FirebaseLogEvent(string eventName, Dictionary<string, object> eventData)
    {
        if (FirebaseManager.Instance.IsFirebaseAnalyticInitSuccess == false)
        {
            // Because we dont want miss any event 
            // We push it to queue to fire event later when Firebase Init success
            //StoreFailedEvent(new AnalycticEventData(eventName, eventData, ANALYTIC_MODULE.FIREBASE));
        }
        else
        {
            eventName = ValidateFirebaseName(eventName);
            if (eventData == null)
            {
                FirebaseManager.Instance.LogEvent(eventName);
            }
            else
            { 
                FirebaseManager.Instance.LogEvent(eventName, eventData);
            }
        }
    }

    #endregion


    #region Appflyer Analytic

    private bool isInitAppFlyerSuccess = false;
    public bool IsInitAppFlyerSuccess
    {
        get
        {
            return this.isInitAppFlyerSuccess;
        }

        set
        {
            this.isInitAppFlyerSuccess = value;
        }
    }

    Dictionary<string, string> paraConvert = new Dictionary<string, string>();
    public void AppsFlyerLogEvent(string eventName, Dictionary<string, object> para = null)
    {
        if (!isInitAppFlyerSuccess)
        {
            // Because we dont want miss any event 
            // We push it to queue to fire event later when Appflyer Init success
            //StoreFailedEvent(new AnalycticEventData(eventName, para, ANALYTIC_MODULE.APPSFLYER));
        }
        else
        {
            if (para != null)
            {
                //Debug.Log("LOG  #NULL");
                //Dictionary<string, object> paraDic = new Dictionary<string, object>(para);
                paraConvert.Clear();
                if (para != null)
                {
                    foreach (KeyValuePair<string, object> item in para)
                    {
                        paraConvert.Add(item.Key, item.Value.ToString());
                    }
                }
                AppsFlyer.trackRichEvent(eventName, paraConvert);
            }
            else
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                data.Add("test", "test");
                try
                {
                    AppsFlyer.trackRichEvent(eventName, data);
                }
                catch (Exception ex)
                {
                    Debug.LogError("AF TrackRichEvent messageError: " + ex.Message);
                }
            }
        }

    }
    #endregion

    public class AnalycticEventData
    {
        public string eventName;
        public Dictionary<string, object> eventData;
        public ANALYTIC_MODULE type;
        public AnalycticEventData(string _eventName, Dictionary<string, object> _eventData, ANALYTIC_MODULE _type)
        {
            this.eventName = _eventName;
            this.eventData = _eventData;
            this.type = _type;
        }
    }

    string ValidateFirebaseName(string inputStr)
    {
        string outputStr = inputStr.Replace(' ', '_');
        outputStr = outputStr.Replace('-', '_');
        return outputStr;
    }

    private Queue<AnalycticEventData> firebaseEventQueue = new Queue<AnalycticEventData>();
    private Queue<AnalycticEventData> facebookEventQueue = new Queue<AnalycticEventData>();
    private Queue<AnalycticEventData> appFlyerEventQueue = new Queue<AnalycticEventData>();


    /// <summary>
    /// This func will store event to queue to send event as soon as analytic module init success
    /// </summary>
    private void StoreFailedEvent(AnalycticEventData data)
    {
        switch (data.type)
        {
            case ANALYTIC_MODULE.FIREBASE:
                this.firebaseEventQueue.Enqueue(data);
                break;
            case ANALYTIC_MODULE.APPSFLYER:
                this.appFlyerEventQueue.Enqueue(data);
                break;
            case ANALYTIC_MODULE.FACEBOOK:
                this.facebookEventQueue.Enqueue(data);
                break;
        }
    }


    public void ReSendEventFailedAppflyerEventQueue()
    {
        while (this.appFlyerEventQueue.Count > 0)
        {
            this.appFlyerEventQueue.Dequeue();
        }
    }

    public void ReSendEventFailedFirebaseEventQueue()
    {
        while (this.firebaseEventQueue.Count > 0)
        {
            FirebaseLogEvent(this.firebaseEventQueue.Peek().eventName, this.firebaseEventQueue.Peek().eventData);
            this.firebaseEventQueue.Dequeue();
        }
    }

}
