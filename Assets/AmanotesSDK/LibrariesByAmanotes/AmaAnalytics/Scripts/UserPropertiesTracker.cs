﻿using System;
using System.Collections;
using System.Collections.Generic;
using Amanotes.Utils;
using Firebase.Analytics;
using UnityEngine;

[RequireComponent(typeof(GlobalData))]
public class UserPropertiesTracker : MonoBehaviour
{
    #region singletons

    private static UserPropertiesTracker instance = null;
    public static UserPropertiesTracker Instance
    {
        get
        {
            if (instance == null)
            {
                Debug.LogError("InterstitialManager instance is null, is this an error?");
            }

            return instance;
        }
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            instance.Initialize();
        }
        else
        {
            if (this != instance)
            {
                Destroy(this);
            }
        }
    }

    #endregion

    #region KEYS

    private const string AB = "ab";
    private const string UID = "uid";
    private const string BANNERADS_CLICK = "bannerads_click";
    private const string BANNERADS_SHOW = "bannerads_show";
    private const string FULLADS_CLICK = "fullads_click";
    private const string FULLADS_FINISH = "fullads_finish";
    private const string FULLADS_SHOW = "fullads_show";
    private const string VIDEOADS_CLICK = "videoads_click";
    private const string VIDEOADS_FINISH = "videoads_finish";
    private const string VIDEOADS_SHOW = "videoads_show";
    private const string IAPLTV = "iapltv";
    private const string LVL = "lvl";
    private const string SCREEN = "screen";
    private const string SESSION = "session";
    private const string SESSION_DURATION = "session_duration";
    private const string SONG_BUY = "song_buy";
    private const string SONG_END = "song_end";
    private const string SONG_START = "song_start";
    private const string TOTAL_TIME = "total_time";
    private const string PLAY_TIME = "play_time";
    private const string CCOL = "ccol";
    private const string COWN = "cown";
    private const string SCOL = "scol";
    private const string SOWN = "sown";

    #endregion

    private Dictionary<string, string> keyMap = new Dictionary<string, string>();

    void Initialize()
    {
        keyMap[AB] = "ab";
        keyMap[UID] = "uid";
        keyMap[BANNERADS_CLICK] = "bannerads_click";
        keyMap[BANNERADS_SHOW] = "bannerads_show";
        keyMap[FULLADS_CLICK] = "fullads_click";
        keyMap[FULLADS_FINISH] = "fullads_finish";
        keyMap[FULLADS_SHOW] = "fullads_show";
        keyMap[VIDEOADS_CLICK] = "videoads_click";
        keyMap[VIDEOADS_FINISH] = "videoads_finish";
        keyMap[VIDEOADS_SHOW] = "videoads_show";
        keyMap[IAPLTV] = "iapltv";
        keyMap[LVL] = "lvl";
        keyMap[SCREEN] = "screen";
        keyMap[SESSION] = "session";
        keyMap[SESSION_DURATION] = "session_duration";
        keyMap[SONG_BUY] = "song_buy";
        keyMap[SONG_END] = "song_end";
        keyMap[SONG_START] = "song_start";
        keyMap[TOTAL_TIME] = "total_time";
        keyMap[PLAY_TIME] = "play_time";
        keyMap[CCOL] = "song_end";
        keyMap[COWN] = "song_start";
        keyMap[SCOL] = "total_time";
        keyMap[SOWN] = "play_time";
    }

    bool SetProperty(string key, object value)
    {
        if (!keyMap.ContainsKey(key))
        {
            Debug.LogError(this.name + " you are not init for this key, and it not be tracked!");
            return false;
        }
        Development.Log("UserPropertiesTracker [" + key + "] " + ": " + value.ToString());
        FirebaseAnalytics.SetUserProperty(keyMap[key], value.ToString());
        return true;
    }

    int GetCachedInt(string key)
    {
        return PlayerPrefs.GetInt(key, 0);
    }

    float GetCachedFloat(string key)
    {
        return PlayerPrefs.GetFloat(key, 0.0f);
    }

    void CacheInt(string key, int value)
    {
        PlayerPrefs.SetInt(key, value);
    }
    void CacheFloat(string key, float value)
    {
        PlayerPrefs.SetFloat(key, value);
    }

    void CacheString(string key, string value)
    {
        PlayerPrefs.SetString(key, value);
    }

    #region tracking funcs
    private DateTime curTotalTime = new DateTime(0);
    private DateTime curPlayTime = new DateTime(0);
    double GetDiffTotalDateTime()
    {
        if (curTotalTime.Ticks == 0L)
        {
            curTotalTime = DateTime.Now;
            return 0;
        }
        DateTime now = DateTime.Now;
        TimeSpan diffTime = now - curTotalTime;
        curTotalTime = now;
        return diffTime.TotalSeconds;
    }

    double GetDiffPlayDateTime()
    {
        if (curPlayTime.Ticks == 0L)
        {
            curPlayTime = DateTime.Now;
            return 0;
        }
        DateTime now = DateTime.Now;
        TimeSpan diffTime = now - curPlayTime;
        curPlayTime = now;
        return diffTime.TotalSeconds;
    }

    void OnApplicationPause(bool isPause)
    {
        if (isPause)
        {
            UpdateTotalPlayTime();
        }
        else
        {
            curTotalTime = DateTime.Now;
        }
    }

    void OnApplicationQuit()
    {
        UpdateTotalPlayTime();
    }

    private float playTimePassed = 0.0f;

    public void AddPlayTimePass(float time)
    {
        playTimePassed += time;
    }

    #endregion

    #region public funcs

    public void SetABTestting(string ABValue)
    {

    }

    public void SetLevel(int level)
    {
        SetProperty(LVL, level);
    }

    public void AddInterstitialClickCount()
    {
        int nVal = GetCachedInt(FULLADS_CLICK) + 1;
        SetProperty(FULLADS_CLICK, nVal);
        CacheInt(FULLADS_CLICK, nVal);
    }

    public void AddInterstitialShowCount()
    {
        int nVal = GetCachedInt(FULLADS_SHOW) + 1;
        SetProperty(FULLADS_SHOW, nVal);
        CacheInt(FULLADS_SHOW, nVal);
    }

    public void AddInterstitialFinishCount()
    {
        int nVal = GetCachedInt(FULLADS_FINISH) + 1;
        SetProperty(FULLADS_FINISH, nVal);
        CacheInt(FULLADS_FINISH, nVal);
    }

    public void AddRewardVideoClickCount()
    {
        int nVal = GetCachedInt(VIDEOADS_CLICK) + 1;
        SetProperty(VIDEOADS_CLICK, nVal);
        CacheInt(VIDEOADS_CLICK, nVal);
    }

    public void AddRewardVideoShowCount()
    {
        int nVal = GetCachedInt(VIDEOADS_SHOW) + 1;
        SetProperty(VIDEOADS_SHOW, nVal);
        CacheInt(VIDEOADS_SHOW, nVal);
    }

    public void AddRewardVideoFinishCount()
    {
        int nVal = GetCachedInt(VIDEOADS_FINISH) + 1;
        SetProperty(VIDEOADS_FINISH, nVal);
        CacheInt(VIDEOADS_FINISH, nVal);
    }

    public void AddBannerAdsClickCount()
    {
        int nVal = GetCachedInt(BANNERADS_CLICK) + 1;
        SetProperty(BANNERADS_CLICK, nVal);
        CacheInt(BANNERADS_CLICK, nVal);
    }

    public void AddBannerAdsShowCount()
    {
        int nVal = GetCachedInt(BANNERADS_SHOW) + 1;
        SetProperty(BANNERADS_SHOW, nVal);
        CacheInt(BANNERADS_SHOW, nVal);
    }

    public void AddIAPLtvCount()
    {
        int nVal = GetCachedInt(IAPLTV) + 1;
        SetProperty(IAPLTV, nVal);
        CacheInt(IAPLTV, nVal);
    }

    public void AddScreen(string screen)
    {
        SetProperty(SCREEN, screen);
    }

    public void AddSessionCount()
    {
        int nVal = GetCachedInt(SESSION) + 1;
        SetProperty(SESSION, nVal);
        CacheInt(SESSION, nVal);
    }

    public void AddSessionDuration(int timeDuration)
    {
        SetProperty(SESSION_DURATION, timeDuration);
    }

    public void AddSongBuy()
    {
        int nVal = GetCachedInt(SONG_BUY) + 1;
        SetProperty(SONG_BUY, nVal);
        CacheInt(SONG_BUY, nVal);
    }

    public void AddSongEnd()
    {
        int nVal = GetCachedInt(SONG_END) + 1;
        SetProperty(SONG_END, nVal);
        CacheInt(SONG_END, nVal);

        UpdateTotalTimeSP();
    }

    public void AddSongStart()
    {
        int nVal = GetCachedInt(SONG_START) + 1;
        SetProperty(SONG_START, nVal);
        CacheInt(SONG_START, nVal);

        curPlayTime = DateTime.Now;
        playTimePassed = 0.0f;
    }

    public void AddSownCount()
    {
        int nVal = GetCachedInt(SOWN) + 1;
        SetProperty(SOWN, nVal);
        CacheInt(SOWN, nVal);
    }



    public void UpdateTotalPlayTime()
    {
        AddTotalPlayTime((float)GetDiffTotalDateTime());
    }

    public void AddTotalPlayTime(float value)
    {
        float nVal = GetCachedFloat(TOTAL_TIME) + value;
        SetProperty(TOTAL_TIME, nVal);
        CacheFloat(TOTAL_TIME, nVal);
    }

    public void SetUID(string value)
    {
        SetProperty(UID, value);
    }

    public void UpdateTotalTimeSP()
    {
        AddTimeSP(playTimePassed);
        //        AddTimeSP((float)GetDiffPlayDateTime());
    }

    public void AddTimeSP(float value)
    {
        float nVal = GetCachedFloat(PLAY_TIME) + value;
        SetProperty(PLAY_TIME, nVal);
        CacheFloat(PLAY_TIME, nVal);
    }



    #endregion
}
