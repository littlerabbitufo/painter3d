﻿using System.Collections;
using System.Collections.Generic;
using Amanotes.Utils;
using UnityEngine;

[RequireComponent(typeof(GlobalData))]
public class AppsFlyerService : MonoBehaviour
{
    #region singletons

    private static AppsFlyerService instance = null;
    public static AppsFlyerService Instance
    {
        get
        {
            if (instance == null)
            {
                Debug.LogError("InterstitialManager instance is null, is this an error?");
            }

            return instance;
        }
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            if (this != instance)
            {
                Destroy(this);
            }
        }
    }

    #endregion

    public void Init()
    {
        Debug.Log("Appsflyer Init:"+GameInitialManager.Instance.InitSettingData.appsflyer_dev_key);
#if UNITY_IOS
		AppsFlyer.setAppsFlyerKey (GameInitialManager.Instance.InitSettingData.appsflyer_dev_key);
		AppsFlyer.setAppID(GameInitialManager.Instance.InitSettingData.appsflyer_ios_itune_app_id);
		AppsFlyer.getConversionData ();
		AppsFlyer.trackAppLaunch ();
        AnalyticService.Instance.IsInitAppFlyerSuccess = true;
#elif UNITY_ANDROID


        AppsFlyer.setAppsFlyerKey(GameInitialManager.Instance.InitSettingData.appsflyer_dev_key);
        
        AppsFlyer.setAppID(Application.identifier);
        AppsFlyer.init(GameInitialManager.Instance.InitSettingData.appsflyer_dev_key);

        // for getting the conversion data
        AppsFlyer.loadConversionData("StartUp");
        AppsFlyer.trackAppLaunch();

        //For Android Uninstall
        //AppsFlyer.setGCMProjectNumber ("YOUR_GCM_PROJECT_NUMBER");
        AnalyticService.Instance.IsInitAppFlyerSuccess = true;
#endif
        Dictionary<string,string> param=new Dictionary<string,string> ();
        param["af_count"] = "1";
        AppsFlyer.trackRichEvent("af_open_session",param);

    }
}
