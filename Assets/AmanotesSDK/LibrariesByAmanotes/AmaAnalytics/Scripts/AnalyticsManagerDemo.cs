﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Amanotes.Utils;
using UnityEngine.SceneManagement;

public class AnalyticsManagerDemo : MonoBehaviour
{

#region action button click

	public void OnClickSendAll()
    {
        AnalyticService.Instance.LogEvent("LogEventSendAll", new Dictionary<string, object> { { "Value", "success" } });
  
    }

    public void OnClickSendUnity()
    {
        AnalyticService.Instance.LogEvent("LogEventSendUnity", new Dictionary<string, object> { { "Value", "success" } } , ANALYTIC_MODULE.UNITY);
    }

    public void OnClickSendFirebase()
    {
        AnalyticService.Instance.LogEvent("LogEventSendFirebase", new Dictionary<string, object> { { "Value", "success" } }, ANALYTIC_MODULE.FIREBASE);
    }

    public void OnClickSendFacebook()
    {
        AnalyticService.Instance.LogEvent("LogEventSendFacebook", new Dictionary<string, object> { { "Value", "success" } }, ANALYTIC_MODULE.FACEBOOK);
    }

    public void OnClickSendAppFlyer()
    {
        AnalyticService.Instance.LogEvent("LogEventSendAppFlyer", new Dictionary<string, object> { { "Value", "success" } }, ANALYTIC_MODULE.APPSFLYER);
    }

    /// <summary>
    /// Ons the click no parameters.
    /// send key : "LogEventNoParams" :: params : none
    /// </summary>
    public void OnClickNoParams()
    {
        AnalyticService.Instance.LogEvent("LogEventNoParams");
    }

    /// <summary>
    /// Ons the click one parameters.
    /// Send key : "LogEventOneParams" :: params : success
    /// </summary>
    public void OnClickOneParams()
    {
        AnalyticService.Instance.LogEvent("LogEventOneParams", new Dictionary<string, object> { { "Value", "success" } });
    }


    /// <summary>
    /// Ons the click all parameters.
    /// Send key : "LogEventAllParams" :: params : {"IsTest" , "Success"} {"Params" ,1} 
    /// </summary>
    private Dictionary<string, object> allParamsDics = new Dictionary<string, object>(3);
    public void OnClickAllParams()
    {
        
        allParamsDics.Clear();
        allParamsDics.Add("IsTest", "Success");
        allParamsDics.Add("Params", 1);

        AnalyticService.instance.LogEvent("LogEventAllParams", allParamsDics);
    }


    public void OnClickBack()
    {
        this.gameObject.SetActive(false);
    }
#endregion
}
