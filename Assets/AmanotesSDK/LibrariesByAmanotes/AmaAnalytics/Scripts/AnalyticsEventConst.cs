﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class AnalyticsEventConst
{

    #region Main Events

    public const string song_start = "song_start";
    public const string song_end = "song_end";
    public const string song_result = "song_result";
    public const string song_fail = "song_fail";
    public const string tutorial = "tutorial";
    public const string tutorial_start = "tutorial_start";
    public const string tutorial_finish = "tutorial_finish";
    public const string ubutton_clickid = "button_click";
    public const string share = "share";
    public const string item_buy = "item_buy";
    public const string currency_income = "currency_income";
    public const string item_usage = "item_usage";
    public const string iap_click = "iap_click";
    public const string status_change = "status_change";
    public const string level_up = "level_up";
    #endregion

    #region Ad Event

    public const string FullAds_Show = "FullAds_Show";
    public const string FullAds_Request = "FullAds_Request";
    public const string FullAds_Show_Ready = "FullAds_Show_Ready";
    public const string FullAds_Show_Failed = "FullAds_Show_Failed";
    public const string FullAds_Show_NotReady = "FullAds_Show_NotReady";
    public const string FullAds_Request_Success = "FullAds_Request_Success";
    public const string FullAds_Request_Failed = "FullAds_Request_Failed";
    public const string FullAds_Finish = "FullAds_Finish";
    public const string FullAds_Click = "FullAds_Click";


    public const string VideoAds_Request = "VideoAds_Request";
    public const string VideoAds_Show_Ready = "VideoAds_Show_Ready";
    public const string VideoAds_Request_Failed = "VideoAds_Request_Failed";
    public const string VideoAds_Show = "VideoAds_Show";
    public const string VideoAds_Show_Failed = "VideoAds_Show_Failed";
    public const string VideoAds_Show_NotReady = "VideoAds_Show_NotReady";
    public const string VideoAds_Finish = "VideoAds_Finish";
    public const string VideoAds_Click = "VideoAds_Click";
    public const string VideoAds_End = "VideoAds_End";



    public const string bannerads_show = "bannerads_show";
    public const string bannerads_click = "bannerads_click";

    public const string fullads_user_all_show = "fullads_user_all_show";
    #endregion


}
