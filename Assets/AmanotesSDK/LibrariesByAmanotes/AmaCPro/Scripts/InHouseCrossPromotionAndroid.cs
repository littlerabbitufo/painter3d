﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Amanotes.CrossPromotion
{
    #pragma warning disable CS0162  
    public partial class InHouseCrossPromotionAndroid : InHouseCrossPromotionProvider
    {
        public const string NAME = "CrossPromotion";

        //private string APP_KEY = "";
        bool initialized = false;

#if UNITY_ANDROID
        private AndroidJavaClass javaClass;

        //private AndroidJavaObject toastExample = null;

        //private AndroidJavaObject activityContext = null;
#endif
        const string PackageClassName = "com.amanotes.inhouseads2.InHouseAdsPlugin";

        public void ActiveLog(bool isShow)
        {
#if UNITY_EDITOR

            return;
#endif
#if UNITY_ANDROID
#if UNITY_5_4
            string bundleId = Application.bundleIdentifier + ";";
#else
            string bundleId = Application.identifier + ";";
#endif
            AndroidJavaClass permissionManager = new AndroidJavaClass(PackageClassName);
            permissionManager.CallStatic("SetCurrentBundleId", bundleId);
# endif
        }


        public void SetCurrentBundleId()
        {
#if UNITY_EDITOR

            return;
#endif
#if UNITY_ANDROID
#if UNITY_5_4
            string bundleId = Application.bundleIdentifier + ";";
#else
            string bundleId = Application.identifier + ";";
#endif
            AndroidJavaClass permissionManager = new AndroidJavaClass(PackageClassName);
            permissionManager.CallStatic("SetCurrentBundleId", bundleId);
# endif
        }


        public void SetCurrentAppName()
        {
#if UNITY_EDITOR

            return;
#endif
#if UNITY_ANDROID
            AndroidJavaClass permissionManager = new AndroidJavaClass(PackageClassName);
            permissionManager.CallStatic("SetCurrentAppName", Application.productName);
# endif
        }


        public void SetCurrentVersion()
        {
#if UNITY_EDITOR

            return;
#endif
#if UNITY_ANDROID
            AndroidJavaClass permissionManager = new AndroidJavaClass(PackageClassName);
            permissionManager.CallStatic("SetCurrentVersion", Application.version);
# endif
        }

        public override void ActiveUnity(string param)
        {
#if UNITY_EDITOR
            return;
#endif
#if UNITY_ANDROID
            Debug.Log("[CrossPromotion: ] active unity  " + param);
            AndroidJavaClass permissionManager = new AndroidJavaClass(PackageClassName);
            permissionManager.CallStatic<int>("ActiveUnity", param);
# endif
        }

        public override IEnumerator Initialize()
        {
            Debug.Log("[CrossPromotion: ] Init 0 ");
          
            initialized = false;
            try
            {
                Debug.Log("[CrossPromotion: ] Init ");
#if UNITY_ANDROID
                javaClass = new AndroidJavaClass(PackageClassName);
                javaClass.CallStatic("SetAdsId", InHouseCrossPromotionHelper.Instance.GetAdsId());
                javaClass.CallStatic("InitialAds", InHouseCrossPromotionHelper.Instance.GetSecretKey());
#endif
            }
            catch (System.Exception)
            {

            }
            finally
            {

            }
            OnEnabled();
            initialized = true;
            yield return new WaitForSeconds(1);
        }


        public override void OnApplicationPaused(bool isPaused)
        {
            try
            {

            }
            catch (System.Exception) { }
        }


        public override void OnEnabled()
        {
            try
            {
            }
            catch (System.Exception) { }
        }

        public override bool IsInterstitialReady()
        {
            return initialized;
        }

        public override bool IsRewardVideoReady()
        {
            return initialized;
        }


        public override string Name()
        {
            return NAME;
        }

        public override void ShowInterstitial()
        {

            Debug.Log("[CrossPromotion: ] show intertitial ads ");
#if UNITY_ANDROID
            javaClass = new AndroidJavaClass(PackageClassName);
            javaClass.CallStatic("ShowIntertitialAds");
#endif
        }

        public override void ShowRewardVideo()
        {
            Debug.Log("[CrossPromotion: ] show reward video ads ");
#if UNITY_ANDROID
            javaClass = new AndroidJavaClass(PackageClassName);
            javaClass.CallStatic("ShowRewardAds");
#endif
        }

        public override void ShowBanner()
        {
            Debug.Log("[CrossPromotion: ] show banner ads ");
#if UNITY_ANDROID
            javaClass = new AndroidJavaClass(PackageClassName);
            javaClass.CallStatic("ShowBannerAds");
#endif
        }

        #region event cross promotion

        // receive callback from Android
        public void LogEvent(string eventData)
        {
            Debug.Log("[CrossPromotion: ] callback from Android " + eventData);
            if (string.Compare(eventData, InHouseEventCallback.videoads_end) == 0)
            {
                OnRewardVideoEndedJni();
            }

            if (string.Compare(eventData, InHouseEventCallback.videoads_close) == 0)
            {
                OnRewardVideoClosedJni();
            }

            if (string.Compare(eventData, InHouseEventCallback.fullads_end) == 0)
            {
                OnIntertitialEndJni();
            }

            if (string.Compare(eventData, InHouseEventCallback.fullads_close) == 0)
            {
                OnIntertitialClosedJni();
            }

        }


        public void OnRewardVideoClosedJni()
        {
            Debug.Log("[CrossPromotion: ] unity OnRewardVideoClosed ");
            if (OnRewardVideoClosed != null)
            {
                OnRewardVideoClosed();
            }
        }

        public void OnRewardVideoEndedJni()
        {
            Debug.Log("[CrossPromotion: ] unity OnRewardVideoEnded ");
            if (OnRewardedVideoAdEnded != null)
            {
                OnRewardedVideoAdEnded();
            }
        }

        public void OnIntertitialClosedJni()
        {
            Debug.Log("[CrossPromotion: ] unity OnIntertitialClosed ");
            if (OnInterstitialClosed != null)
            {
                OnInterstitialClosed();
            }
        }

        public void OnIntertitialEndJni()
        {
            Debug.Log("[CrossPromotion: ] unity OnIntertitialEnd ");
            if (OnInterstitialEnd != null)
            {
                OnInterstitialEnd();
            }
        }

        #endregion
    }
    #pragma warning restore CS0162  
}

