﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Amanotes.CrossPromotion
{
    public abstract class InHouseCrossPromotionProvider
    {

        public abstract string Name();

        public abstract void ActiveUnity(string parmas);
        public abstract IEnumerator Initialize();
        public abstract void OnApplicationPaused(bool isPaused);
        public abstract void OnEnabled();
        public abstract void ShowInterstitial();
        public abstract void ShowRewardVideo();
        public abstract void ShowBanner();
        public abstract bool IsInterstitialReady();
        public abstract bool IsRewardVideoReady();


        public Action OnInitIntertitialAds;

        public Action OnInterstitialRequest;

        public Action OnInterstitialRequestFailed;

        public Action OnInterstitialRequestSuccess;

        public Action OnInterstitialShowSuccess;

        public Action OnInterstitialClosed;

        public Action OnInterstitialEnd;

        public Action OnInterstitialClicked;

        // Reward ads

        public Action OnInitRewardAds;

        public Action OnRewardVideoRequest;

        public Action OnRewardVideoShowSuccess;

        public Action OnRewardVideoClosed;

        public Action OnRewardedVideoAdsRequestSuccess;

        public Action OnRewardedVideoAdEnded;

        public Action OnRewardedVideoAdRequestFailed;

        public Action OnRewardedVideoAdClicked;


    }
}
