﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using UnityEngine.Networking;
using UnityEngine.Advertisements;
using Amanotes.Utils;

namespace Amanotes.CrossPromotion
{
    [RequireComponent(typeof(GlobalData))]
    public class InHouseCrossPromotionHelper : SingletonMono <InHouseCrossPromotionHelper>
    {
        [SerializeField]
        private bool isShowLog = true;

        [SerializeField]
        private string iosSecretKey="";

        [SerializeField]
        private string androidSecretKey="";


        private bool initialized = false;
        private string advertisingId = "";// advertisingId

        public string GetAdsId()
        {
            return advertisingId;
        }
        public string GetSecretKey()
        {
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                return iosSecretKey;
            }
            return androidSecretKey;
        }

        [SerializeField] private List<string> adsProviders = new List<string> { "Crosspromotion" };

        private Dictionary<string, InHouseCrossPromotionProvider> adsProvidersMap = new Dictionary<string, InHouseCrossPromotionProvider>();

        private List<InHouseCrossPromotionProvider> interstitialProviders = new List<InHouseCrossPromotionProvider>();
        //private int iInterstitalProvider = 0; // Index of the ads provider used to show interstitial
        //private float interstitialShownTime = 0;


        //private List<InHouseCrossPromotionProvider> rewardVideoProviders = new List<InHouseCrossPromotionProvider>();
        //private int iRewardVideoProvider = 0; // Index of the ads provider used to show rewarded video




        public Action OnIntertitialInitEvent;
        public Action OnIntertitialEndEvent;
        public Action<bool> OnIntertitialClosedEvent;
        public Action OnRewardVideoInitEvent;
        public Action OnRewardVideoEndedEvent;
        public Action<bool> OnRewardVideoClosedEvent;
        public Action OnBannerEndedEvent;
        public Action<bool> OnBannerClosedEvent;

 
        public InHouseCrossPromotionProvider GetAdsProvider(string name)
        {
            if (adsProvidersMap.ContainsKey(name))
            {
                return adsProvidersMap[name];
            }

            return null;
        }

        public bool IsAdsProviderInitialized(string adsProviderName)
        {
            if (!adsProvidersMap.ContainsKey(adsProviderName)) return false;
            return adsProvidersMap[adsProviderName].IsInterstitialReady();
        }

        void OnApplicationPause(bool isPaused)
        {
            foreach (InHouseCrossPromotionProvider adsProvider in adsProvidersMap.Values)
            {
                adsProvider.OnApplicationPaused(isPaused);
            }
        }
        // Use this for initialization
        void Start()
        {
            DontDestroyOnLoad(this.gameObject);

        }
        public void Initialize()
        {
            ActiveCPro();
            Application.RequestAdvertisingIdentifierAsync((string advertising, bool trackingEnabled, string error) =>
            {
                this.advertisingId = advertising;

                if (this.advertisingId == null)
                {
                    this.advertisingId = "";
                }

                if (!string.IsNullOrEmpty(this.advertisingId))
                {
                    string final = this.advertisingId + ";";
#if UNITY_5_4
            final += Application.bundleIdentifier + ";";
#else
                    final += Application.identifier + ";";
#endif
                    final += Application.version + ";";
                    final += Application.productName + ";";
                    if (Application.platform == RuntimePlatform.IPhonePlayer)
                    {
                        final += iosSecretKey + ";";
                    }
                    else
                    {
                        final += androidSecretKey + ";";
                    }
                    final += isShowLog + ";";
                    final += SystemInfo.deviceModel + ";";
                    final += SystemInfo.deviceName + ";";
                    final += SystemInfo.deviceType + ";";
                    final += SystemInfo.operatingSystem + ";";
                    final += Application.systemLanguage + ";";
                    final += IPManager.GetLocalIPAddress() + ";";
                    final += "" + ";";

                    final += "" + ";";

                    Debug.Log("[CrossPromotion: ] params 2 " + final);
                    interstitialProviders[0].ActiveUnity(final);

                    ActiveEventAction();
                }
            });


        }


        private void ActiveEventAction()
        {


            // Initialize ads providers
            foreach (InHouseCrossPromotionProvider adsProvider in adsProvidersMap.Values)
            {
                // Intertittial
                adsProvider.OnInitIntertitialAds += OnInitIntertitialAds;
                adsProvider.OnInterstitialRequest += OnInterstitialRequest;
                adsProvider.OnInterstitialRequestFailed += OnInterstitialRequestFailed;
                adsProvider.OnInterstitialRequestSuccess += OnInterstitialRequestSuccess;
                adsProvider.OnInterstitialShowSuccess += OnInterstitialShowSuccess;
                adsProvider.OnInterstitialClosed += OnInterstitialClosed;
                adsProvider.OnInterstitialEnd += OnInterstitialEnd;
                adsProvider.OnInterstitialClicked += OnInterstitialClicked;


                // Reward video
                adsProvider.OnInitRewardAds += OnInitRewardAds;
                adsProvider.OnRewardVideoRequest += OnRewardVideoRequest;
                adsProvider.OnRewardVideoShowSuccess += OnRewardVideoShowSuccess;
                adsProvider.OnRewardVideoClosed += OnRewardVideoClosed;
                adsProvider.OnRewardedVideoAdsRequestSuccess += OnRewardedVideoAdsRequestSuccess;
                adsProvider.OnRewardedVideoAdEnded += OnRewardedVideoAdEnded;
                adsProvider.OnRewardedVideoAdRequestFailed += OnRewardedVideoAdRequestFailed;
                adsProvider.OnRewardedVideoAdClicked += OnRewardedVideoAdClicked;

                StartCoroutine(adsProvider.Initialize());
            }


        }

        private void ActiveCPro()
        {

            if (initialized)
            {
                return;
            }
            initialized = true;

            if (adsProvidersMap != null && adsProvidersMap.Count > 0)
            {
                return;
            }
#if UNITY_ANDROID || UNITY_IOS

            if (adsProviders.Contains("Crosspromotion"))
            {
                adsProvidersMap.Add("Crosspromotion", new InHouseCrossPromotionAndroid());
            }
#endif
            foreach (string name in adsProviders)
            {
                if (!adsProvidersMap.ContainsKey(name))
                {

                    continue;
                }

                InHouseCrossPromotionProvider provider = adsProvidersMap[name];

                interstitialProviders.Add(provider);
            }
        }
        public void ShowIntertitialAds(Action<bool> onComplete = null)
        {

            OnIntertitialClosedEvent = onComplete;

#if UNITY_EDITOR
            if (OnIntertitialClosedEvent != null)
            {
                OnIntertitialClosedEvent(true);
            }
#elif UNITY_ANDROID || UNITY_IOS
            interstitialProviders[0].ShowInterstitial();
#endif
        }

        public void ShowRewardVideoAds(Action<bool> onComplete = null)
        {
            OnRewardVideoClosedEvent = onComplete;
#if UNITY_EDITOR
            if (OnRewardVideoClosedEvent != null)
            {
                OnRewardVideoClosedEvent(true);
            }
#elif UNITY_ANDROID || UNITY_IOS
            interstitialProviders[0].ShowRewardVideo();
#endif
        }

        public void ShowRewardBannerAds(Action<bool> onComplete = null)
        {
            OnBannerClosedEvent = onComplete;
#if UNITY_EDITOR
            if (OnBannerClosedEvent != null)
            {
                OnBannerClosedEvent(true);
            }
#elif UNITY_ANDROID || UNITY_IOS
            interstitialProviders[0].ShowBanner();
#endif
        }

        #region event cross promotion

        // receive callback from Android
        public void LogEvent(string eventData)
        {

            if (string.Compare(eventData, InHouseEventCallback.Cpro_init_fail) == 0)
            {
                OnInitFail();
            }

            if (string.Compare(eventData, InHouseEventCallback.videoads_init) == 0)
            {
                OnInitRewardAds();
            }

            if (string.Compare(eventData, InHouseEventCallback.videoads_end) == 0)
            {
                OnRewardedVideoAdEnded();
            }

            if (string.Compare(eventData, InHouseEventCallback.videoads_close) == 0)
            {
                OnRewardVideoClosed();
            }

            if (eventData.Contains(InHouseEventCallback.videoads_error))
            {
                OnRewardVideoError(eventData);
            }

            if (string.Compare(eventData, InHouseEventCallback.fullads_init) == 0)
            {
                OnInitIntertitialAds();
            }

            if (string.Compare(eventData, InHouseEventCallback.fullads_end) == 0)
            {
                OnInterstitialEnd();
            }

            if (string.Compare(eventData, InHouseEventCallback.fullads_close) == 0)
            {
                OnInterstitialClosed();
            }
            if (eventData.Contains(InHouseEventCallback.fullads_error))
            {
                OnInterstitialError(eventData);
            }

        }

        #endregion
        private void OnInitIntertitialAds()
        {
            if (OnIntertitialInitEvent != null)
                OnIntertitialInitEvent();

        }

        private void OnInterstitialRequest()
        {

        }

        private void OnInterstitialRequestFailed()
        {

        }

        private void OnInterstitialRequestSuccess()
        {

        }
        private void OnInterstitialShowSuccess()
        {

        }

        public void OnInterstitialClosed()
        {
            if (OnIntertitialClosedEvent != null)
            {
                OnIntertitialClosedEvent(true);
                OnIntertitialClosedEvent = null;
            }
        }
        public void OnInterstitialEnd()
        {
            if (OnIntertitialClosedEvent != null)
            {
                OnIntertitialClosedEvent(true);
                OnIntertitialClosedEvent = null;
            }

        }

         public void OnInterstitialError(string msg = "")
        {
            if (OnIntertitialClosedEvent != null)
            {
                Debug.LogError("ad request failed to load: " + msg);
                OnIntertitialClosedEvent(false);
                OnIntertitialClosedEvent = null;
            }
        }

        public void OnInterstitialClicked()
        {


        }

        public void OnInitFail()
        {

        }

        public void OnInitRewardAds()
        {
            if (OnRewardVideoInitEvent != null)
                OnRewardVideoInitEvent();
        }

        public void OnRewardVideoRequest()
        {

        }

        public void OnRewardVideoShowSuccess()
        {

        }

        public void OnRewardVideoClosed()
        {

            if (OnRewardVideoClosedEvent != null)
            {
                OnRewardVideoClosedEvent(true);
                OnRewardVideoClosedEvent = null;
            }
        }
        public void OnRewardedVideoAdsRequestSuccess()
        {

        }

        public void OnRewardedVideoAdRequestFailed()
        {

        }

        public void OnRewardedVideoAdEnded()
        {
            if (OnRewardVideoClosedEvent != null)
            {
                OnRewardVideoClosedEvent(true);
                OnRewardVideoClosedEvent = null;
            }
        }
       
        public void OnRewardVideoError(string msg = "")
        {
            if (OnRewardVideoClosedEvent != null)
            {
                Debug.LogError("ad request failed to load: " + msg);
                OnRewardVideoClosedEvent(false);
                OnRewardVideoClosedEvent = null;
            }
        }
        public void OnRewardedVideoAdClicked()
        {


        }
    }

    public static class IPManager
    {
        public static string GetLocalIPAddress()
        {
#if UNITY_EDITOR
            return "";
#elif UNITY_ANDROID || UNITY_IOS
            var host = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }

            throw new System.Exception("No network adapters with an IPv4 address in the system!");
        
#endif
        }
    }
}