﻿using System;
using UnityEngine;

namespace Amanotes.CrossPromotion
{
    public abstract class Singleton<T> where T : new()
    {
        private static T singleton;
        public static T Instance
        {
            get
            {
                if (singleton == null)
                {
                    singleton = new T();
                }
                return singleton;
            }
        }
        public static T instance
        {
            get
            {
                if (singleton == null)
                {
                    singleton = new T();
                }
                return singleton;
            }
        }
    }

 
    public class SingletonCpro<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T singleton;

        public static bool IsInstanceValid() { return singleton != null; }

        void Reset()
        {
            gameObject.name = typeof(T).Name;
        }

        public static T Instance
        {
            get
            {
                if (SingletonCpro<T>.singleton == null)
                {
                    SingletonCpro<T>.singleton = (T)FindObjectOfType(typeof(T));
                    if (SingletonCpro<T>.singleton == null)
                    {
                        GameObject obj = new GameObject();
                        obj.name = "[@" + typeof(T).Name + "]";
                        SingletonCpro<T>.singleton = obj.AddComponent<T>();
                    }
                }

                return SingletonCpro<T>.singleton;
            }
        }
        public static T instance
        {
            get
            {
                if (SingletonCpro<T>.singleton == null)
                {
                    SingletonCpro<T>.singleton = (T)FindObjectOfType(typeof(T));
                    if (SingletonCpro<T>.singleton == null)
                    {
                        GameObject obj = new GameObject();
                        obj.name = "[@" + typeof(T).Name + "]";
                        SingletonCpro<T>.singleton = obj.AddComponent<T>();
                    }
                }

                return SingletonCpro<T>.singleton;
            }
        }

    }
}