﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Amanotes.CrossPromotion
{
    public class InHouseEventCallback : MonoBehaviour
    {
        public static InHouseEventCallback instance;

        public static InHouseEventCallback Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new InHouseEventCallback();
                }
                return instance;
            }

        }

        public static string Cpro_init_fail = "Cpro_init_fail";

        public static string Cpro_loaded = "Cpro_loaded";
        public static string Cpro_error = "Cpro_error";
        public static string Cpro_Request = "Cpro_Request";

        public static string fullads_init = "fullads_init";
        public static string fullads_show = "fullads_show";
        public static string fullads_click = "fullads_click";
        public static string fullads_close = "fullads_close";
        public static string fullads_end = "fullads_end";
        public static string fullads_error = "fullads_error";

        public static string videoads_init = "videoads_init";
        public static string videoads_show = "videoads_show";
        public static string videoads_click = "videoads_click";
        public static string videoads_close = "videoads_close";
        public static string videoads_end = "videoads_end";
        public static string videoads_error = "videoads_error";

        public static string bannerads_show = "bannerads_show";
        public static string bannerads_click = "bannerads_click";
        public static string bannerads_close = "bannerads_close";
    }
}