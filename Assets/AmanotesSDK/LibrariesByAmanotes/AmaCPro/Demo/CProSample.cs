using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Amanotes.CrossPromotion;
using UnityEngine.UI;
public class CProSample : MonoBehaviour
{
    [SerializeField]
    public GameObject loadingScreen;
    public Text loadingText;
    public Text rubyText;
    private int totalRuby;
    private const string RUBY= "RUBY";

    private void Start(){
        InHouseCrossPromotionHelper.Instance.Initialize();
        StartCoroutine(InitScreen());// wating ads initialize
    }
    IEnumerator InitScreen()
    {
        totalRuby = PlayerPrefs.GetInt(RUBY, 0);
        rubyText.text = "Ruby:" + totalRuby;

        loadingScreen.SetActive(true);
        loadingText.text = "Loading";
        float loadingTime = 5;
        while (loadingTime > 0)
        {
            yield return new WaitForSeconds(1);
            loadingText.text += ".";
            loadingTime--;
        }
        loadingScreen.SetActive(false);
    }

    #region Demo UI Action 

   
    public void OnClickRewardAds()
    {
        InHouseCrossPromotionHelper.Instance.ShowRewardVideoAds(OnCompleteRewardAds);
    }

    public void OnClickIntertitalAds()
    {
        InHouseCrossPromotionHelper.Instance.ShowIntertitialAds(OnCompleteIntertialAds);
    }


    public void OnClickBannerAds()
    {
        InHouseCrossPromotionHelper.Instance.ShowRewardBannerAds(OnCompleteBannerAds);
    }

    #endregion

    #region Cpro Callback  


    private void OnEnable()
    {
        InHouseCrossPromotionHelper.Instance.OnIntertitialInitEvent += HandleIntertitialInit;
        InHouseCrossPromotionHelper.Instance.OnRewardVideoInitEvent += HandleIRewardVideoInit;
    }

    private void OnDisable()
    {
        InHouseCrossPromotionHelper.Instance.OnIntertitialInitEvent -= HandleIntertitialInit;
        InHouseCrossPromotionHelper.Instance.OnRewardVideoInitEvent -= HandleIRewardVideoInit;
    }

    private void HandleIntertitialInit()
    {
        Debug.Log("On Intertitial Init");
    }

    private void HandleIRewardVideoInit()
    {
        Debug.Log("On RewardVideo Init");
    }


    private void OnCompleteRewardAds(bool isSuccess)
    {
        if (isSuccess) // Called when the user should be rewarded for watching a video.
        {
            totalRuby += 10;
            PlayerPrefs.SetInt(RUBY, totalRuby);
            rubyText.text = "Ruby:" + totalRuby;
        }
        else // Called when an ad request failed to load.
        {
           
        }
    }

    private void OnCompleteIntertialAds(bool isSuccess)
    {
        if (isSuccess) // Called when an ad request has successfully loaded
        {

        }
        else  // Called when an ad request failed to load.
        {
           
        }

    }


    private void OnCompleteBannerAds(bool isSuccess)
    {

    }
    #endregion
}
