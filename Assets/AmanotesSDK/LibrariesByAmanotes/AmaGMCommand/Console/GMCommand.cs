using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using Amanotes.Utils;
using UnityEngine;

namespace OneP.GMConsole
{
    public partial class GMCommand
    {
        // Main logger
        protected static readonly OneP.GMConsole.GMLogger log = GMLogger.GetLogger(typeof(GMCommand));

        public GMCommand()
        {

        }


        [Description("Display all available command")]
        public static string Help()
        {
            string str = ConsoleCommands.GetAllCommandHelps();
            //UnityEngine.Debug.LogError(str);
            return str;
        }

        [Description("Display a specific command")]
        public static string Help(string command)
        {
            string str = ConsoleCommands.GetCommandHelp(command); ;
            //UnityEngine.Debug.LogError(str);
            return str;
        }

        [Description("Change current quality settings.")]
        [CmdDetail("Params> 0: Fastest, " +
            "1: Fast, " +
            "2: Simple, " +
            "3: Good, " +
            "4: Beautiful, " +
            "5: Fantastic.")]
        public static string Quality(int value)
        {
            QualitySettings.SetQualityLevel(value);
            return "Quality now set to: " + CheckQuality();
        }

        [Description("Get current quality settings.")]
        public static string CheckQuality()
        {
            return QualitySettings.names[QualitySettings.GetQualityLevel()] + " (" + QualitySettings.GetQualityLevel() + ")";
        }

        [Description("AdsInitialized")]
        public string InitAds()
        {
            string keyIronsource = "";

            //AdHelper.Instance.GMOpenFullAds ();
#if UNITY_ANDROID
            keyIronsource = GameInitialManager.Instance.InitSettingData.ironsrc_android_key;
#elif UNITY_IOS
            keyIronsource = GameInitialManager.Instance.InitSettingData.ironsrc_ios_key;
#endif
            AdHelper.Instance.Initialize(keyIronsource, UserPropertiesTracker.Instance);
            return "Call Open Ads initialized";
        }

        [Description("AdsFull")]
        public string AdsFull()
        {
            //AdHelper.Instance.GMOpenFullAds ();
            AdHelper.Instance.ShowInterstitial();
            return "Call Open Ads Full";
        }

        [Description("AdsReward")]
        public string AdsReward()
        {
            AdHelper.Instance.ShowRewardVideo(onWatched: () =>
            {
                Debug.Log("GMTool: on watched ad reward");
            }, onFailed: () =>
            {
                Debug.Log("GMTool: on failed watch video reward");
            });
            return "Call Open Ads reward";
        }

        [Description("AdsBanner")]
        public string AdsBanner()
        {
            AdHelper.Instance.ShowBannerAds(RewardType.Ruby, AdsBannerPosition.BOTTOM, onWatched: () =>
            {
                Debug.Log("GMTool: on show banner ads success");
            }, onFailed: () =>
            {
                Debug.Log("GMTool: on show banner ads fail");
            });
            return "Call Open Ads Banner";
        }
    }
}


