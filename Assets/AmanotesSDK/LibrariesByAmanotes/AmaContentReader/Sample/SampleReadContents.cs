﻿using UnityEngine;
using System.Collections;
using Amanotes.Data;
using Amanotes.Content;
using Amanotes.Synth;
using System;
using System.Collections.Generic;

public class SampleReadContents : MonoBehaviour {
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Load(){
		string fileName = "Content_Sample.bin";
		string path=Application.streamingAssetsPath + "/"+fileName;
		Development.Log("Load Content In:"+path);
		#if !UNITY_EDITOR && UNITY_ANDROID
			StartCoroutine(LoadFileForAndroid(path,res=>{
				Debug.LogWarning ("Size Of File:"+res.Length);
				LoadContent(res);
			}));
		#else
			byte[] data=LoadFile(path);
			Debug.LogWarning ("Size Of File:"+data.Length);
			LoadContent (data);
		#endif
	}
	public IEnumerator LoadFileForAndroid(string url,Action<byte[]> callback){
		WWW www = new WWW(url);
		yield return www;
		callback (www.bytes);
	}

	public void LoadContent(byte[] data){
		//HIEN CHI TAO CHO 3 TRACK, CAC GAME FREELANCE CHI TAO TREN TRACK ADVANCE
		//EASY
		//NORMAL
		//ADVANCE
		NoteGeneration.LoadFileContent (data, Difficulty.Advanced, resSucess => {
			Debug.LogWarning("Here is the result of Content Tool");
			Debug.LogWarning("Each Note of content 2 important key are: <timeAppear>: Time It Appeare and  <stingIndex>: line index (max=5 line)");
			//Debug.LogWarning(Pathfinding.Serialization.JsonFx.JsonWriter.Serialize(resSucess));

			// stringIndex have value from 0->4
			if(resSucess!=null){
				string info="ID\tIndex\t TimeAppear:";
				for(int i=0;i<resSucess.Count;i++){
					info+="\n"+resSucess[i].noteOrder+"\t"+resSucess[i].stringIndex+"\t"+resSucess[i].timeAppear;
				}
				Debug.LogWarning(info);
			}
			else{
				Debug.LogWarning("Content load failed");
			}
		},resError=>{
			Debug.LogError("Error:"+resError);
		});
	}

	public static byte[] LoadFile(string filePath) {
		if (filePath == null || filePath.Length == 0) {
			return null;
		}
		if (System.IO.File.Exists(filePath)) {
			return System.IO.File.ReadAllBytes(filePath);
		} else {
			return null;
		}
	}
}
