﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Amanotes.Utils;
using UnityEngine.SceneManagement;
using System;
public class AdsManagerDemo : MonoBehaviour
{
    [SerializeField] private int gem;
    [SerializeField] private Text txtGem;

    [SerializeField] private GameObject loadingObj;

    

    private void OnEnable()
    {
        AdHelper.Instance.onIntertitialFailed += OnCompleteFail;
        AdHelper.Instance.onRewardVideoFailed += onCompleteFailed;
        AdHelper.Instance.onIntertitialWatched += OnCompleteIntertitialAds;
        AdHelper.Instance.onRewardVideoWatched += OnCompleteVideoAds;
    }

    private void OnDisable()
    {
        AdHelper.Instance.onIntertitialFailed -= OnCompleteFail;
        AdHelper.Instance.onRewardVideoFailed -= onCompleteFailed;
        AdHelper.Instance.onIntertitialWatched -= OnCompleteIntertitialAds;
        AdHelper.Instance.onRewardVideoWatched -= OnCompleteVideoAds;
    }

    private void OnWatchVideoFinish(RewardType obj = RewardType.Ruby)
    {
        this.gem += 100;
        this.txtGem.text = this.gem.ToString();
    }

    #region action button click 

    public void OnClickVideoAds()
    {
        this.loadingObj.SetActive(true);


#if !UNITY_EDITOR
        AdHelper.Instance.ShowRewardVideo(RewardType.Ruby, OnCompleteVideoAds ,onCompleteFailed);
#else
        AdHelper.Instance.ShowRewardVideo(RewardType.Ruby, OnCompleteVideoAds, onCompleteFailed);
#endif

    }

    private void OnCompleteVideoAds()
    {
        Debug.Log("Ads reward success");
        this.loadingObj.SetActive(false);
        this.gem += 100;
        this.txtGem.text = this.gem.ToString();
    }

    public void onCompleteFailed()
    {
        Debug.Log("Ads reward fail");
        this.loadingObj.SetActive(false);
    }

    public void OnClickIntertitialAds()
    {
        this.loadingObj.SetActive(true);
#if !UNITY_EDITOR
        AdHelper.Instance.ShowInterstitial(OnCompleteIntertitialAds , OnCompleteFail);
#else
        AdHelper.Instance.ShowInterstitial(OnCompleteIntertitialAds, OnCompleteFail);
#endif

    }

    private void OnCompleteIntertitialAds()
    {
        this.gem += 100;
        this.txtGem.text = this.gem.ToString();
        this.loadingObj.SetActive(false);
    }

    public void OnCompleteFail()
    {
        this.loadingObj.SetActive(false);
    }

    public void OnClickBannerAdsBottom()
    {
#if !UNITY_EDITOR
        AdHelper.Instance.ShowBannerAds( RewardType.Ruby , AdsBannerPosition.BOTTOM,  OnCompleteBannerAds, OnBannerFail);
#else
        AdHelper.Instance.ShowBannerAds(RewardType.Ruby, AdsBannerPosition.BOTTOM, OnCompleteBannerAds, OnBannerFail);
#endif
    }

    public void OnClickBannerAdsTop()
    {
#if !UNITY_EDITOR
        AdHelper.Instance.ShowBannerAds( RewardType.Ruby , AdsBannerPosition.TOP,  OnCompleteBannerAds, OnBannerFail);
#else
        AdHelper.Instance.ShowBannerAds(RewardType.Ruby, AdsBannerPosition.TOP, OnCompleteBannerAds, OnBannerFail);
#endif
    }

    public void OnClickBannerAdsPlacement()
    {
#if !UNITY_EDITOR
        AdHelper.Instance.ShowBannerAdsPlacement(RewardType.Ruby, AdsBannerPosition.BOTTOM, "Show banner test", OnCompleteBannerAds, OnBannerFail);
#else
        AdHelper.Instance.ShowBannerAdsPlacement(RewardType.Ruby, AdsBannerPosition.BOTTOM, "Show banner test", OnCompleteBannerAds, OnBannerFail);
#endif
    }


    public void OnClickBannerAdsHide()
    {
        AdHelper.Instance.HideBannerAds();
    }

    public void OnClickBannerAdsDestroy()
    {
        Development.Log("[AdsManager ] Destroy");
        AdHelper.Instance.DestroyBannerAds();
    }


    public void OnCompleteBannerAds()
    {
        Development.Log("[AdsManager ] show banner success");
    }

    public void OnBannerFail()
    {
        Development.Log("[AdsManager ] show banner fail");
    }

    public void OnClickSoundOn()
    {

    }

    public void OnClickSoundOff()
    {

    }

    public void OnClickBack()
    {
        this.gameObject.transform.parent.gameObject.SetActive(false);
    }

    #endregion
}
