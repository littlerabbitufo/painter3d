
using System;
using System.Collections;

public abstract class AdsProvider
{
    public abstract string Name();
    public abstract IEnumerator Initialize(string keyIronsource = "", UserPropertiesTracker userPropertiesTracker = null);
    public abstract void OnApplicationPaused(bool isPaused);
    public abstract void OnEnabled();
    public abstract void ShowInterstitial();
    public abstract void ShowRewardVideo();
    public abstract void ShowbannerAds(AdsBannerSize size, AdsBannerPosition position);
    public abstract void ShowBannerAdsPlacement(AdsBannerSize size, AdsBannerPosition position, string placementName);

    public abstract void HideBannerAds();

    public abstract void DestroyBannerAds();

    public abstract bool IsInterstitialReady();
    public abstract bool IsRewardVideoReady();
    public abstract bool IsBannerReady();
    public abstract bool IsRewardVideoAvailable();


    public Action OnInitialized;
    public Action OnInterstitialRequest;
    public Action OnInterstitialRequestFailed;
    public Action OnInterstitialRequestSuccess;
    public Action OnInterstitialShowSuccess;
    public Action OnInterstitialShowFailed;
    public Action OnInterstitialClosed;
    public Action OnInterstitialRewareded;
    public Action OnInterstitialClicked;

    public Action OnRewardVideoOpened;
    public Action OnRewardVideoClosed;
    public Action OnRewardedVideoAvailabilityChanged;
    public Action OnRewardedVideoAdStarted;
    public Action OnRewardedVideoAdEnded;
    public Action OnRewardedVideoAdRewarded;
    public Action OnRewardedVideoAdShowFailed;
    public Action OnRewardedVideoAdShowNotReady;
    public Action OnRewardedVideoAdClicked;


    public Action OnBannerOpen;
    public Action OnBannerWatched;
    public Action OnBannerAdLoaded;
    public Action OnBannerAdClicked;
    public Action OnBannerAdLoadFailed;
    public Action OnBannerAdLeftApplication;
    public Action OnBannerAdScreenDismissed;
    public Action OnBannerAdScreenPresented;

}

public class AdsBannerSize
{
    private int width;
    private int height;
    private string description;

    public static AdsBannerSize BANNER = new AdsBannerSize("BANNER");
    public static AdsBannerSize LARGE = new AdsBannerSize("LARGE");
    public static AdsBannerSize RECTANGLE = new AdsBannerSize("RECTANGLE");
    public static AdsBannerSize SMART = new AdsBannerSize("SMART");

    private AdsBannerSize()
    {

    }

    public AdsBannerSize(int width, int height)
    {
        this.width = width;
        this.height = height;
        this.description = "CUSTOM";
    }

    public AdsBannerSize(string description)
    {
        this.description = description;
        this.width = 0;
        this.height = 0;
    }

    public string Description { get { return description; } }
    public int Width { get { return width; } }
    public int Height { get { return height; } }
};

public enum AdsBannerPosition
{
    TOP = 1,
    BOTTOM = 2
};
