﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Amanotes.Utils
{
    public enum RewardType
    {
        Ruby,
        UnlockSong,
        Continue,
    }

    public enum ClickShowAdsType
    {
        IntertitialAds,
        RewardVideoAds,
        NoRewardCallIntertitial,
    }

    public enum PositionType
    {
        Top,
        Bottom,
        Left,
        Right
    }

    [RequireComponent(typeof(GlobalData))]
    public class AdHelper : MonoBehaviour
    {
        [SerializeField] private List<string> adsProviders = new List<string> { "IronSource" };
        [SerializeField] bool adsEnabled;

        public bool isAdsEnabled
        {
            get { return adsEnabled; }
            set { adsEnabled = value; }
        }

        public void SetNoAds(bool isNoAds)
        {
            isAdsEnabled = !isNoAds;
        }

        public int RewardVideoProviderCount { get { return rewardVideoProviders.Count; } }

        private Dictionary<string, AdsProvider> adsProvidersMap = new Dictionary<string, AdsProvider>();
        private int nInitializedAdsProviders = 0;

        private List<AdsProvider> interstitialProviders = new List<AdsProvider>();
        private int iInterstitalProvider = 0; // Index of the ads provider used to show interstitial
        private float interstitialShownTime = 0;

        private List<AdsProvider> rewardVideoProviders = new List<AdsProvider>();
        private int iRewardVideoProvider = 0; // Index of the ads provider used to show rewarded video

        private RewardType typeReward = RewardType.Ruby;
        private bool openingRewardVideo = false;
        private int indexTimeout = 0;

        private ClickShowAdsType clickShowAdsType = ClickShowAdsType.RewardVideoAds;

        public Action onRewardVideoFailed = null;
        public Action onRewardVideoWatched = null;

        public Action onIntertitialFailed = null;
        public Action onIntertitialWatched = null;

        private bool IsOpeningIntertitial = false;
        private const int NumberRetryIntertitial = 3;

        private Action onBannerFailed = null;
        private Action onBannerWatched = null;

        private bool skipNextInterstitial = false;

        private IEnumerator TimeOutRewardAdsCoroutine;
        private IEnumerator TimeOutIntertitialCoroutine;

        private static AdHelper instance;
        public static AdHelper Instance
        {
            get
            {
                if (instance == null)
                {
                    Debug.LogError("Adhelper instance is null, is this an error?");
                }

                return instance;
            }
        }

        public bool LoadingRewardVideo { get { return openingRewardVideo; } }

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                if (this != instance)
                {
                    Destroy(this);
                }
            }
        }



        public AdsProvider GetAdsProvider(string name)
        {
            if (adsProvidersMap.ContainsKey(name))
            {
                return adsProvidersMap[name];
            }

            return null;
        }

        public bool IsAdsProviderInitialized(string adsProviderName)
        {
            if (!adsProvidersMap.ContainsKey(adsProviderName)) return false;
            return adsProvidersMap[adsProviderName].IsInterstitialReady();
        }

        void OnApplicationPause(bool isPaused)
        {
            foreach (AdsProvider adsProvider in adsProvidersMap.Values)
            {
                adsProvider.OnApplicationPaused(isPaused);
            }
        }

        public void Initialize(string keyIronsource = "", UserPropertiesTracker userPropertiesTracker = null)
        {
            if (adsProvidersMap != null && adsProvidersMap.Count > 0)
            {
                return;
            }
#if UNITY_ANDROID || UNITY_IOS

            if (adsProviders.Contains(AdsIronSource.NAME))
            {
                adsProvidersMap.Add(AdsIronSource.NAME, new AdsIronSource());
            }
#endif
            // Initialize ads providers
            nInitializedAdsProviders = 0;
            TimeOutRewardAdsCoroutine = TimeOutVideoReward(1);
            TimeOutIntertitialCoroutine = TimeOutIntertitial();
            foreach (AdsProvider adsProvider in adsProvidersMap.Values)
            {
                // Intertittial
                adsProvider.OnInitialized += OnAdsProviderInitialized;
                adsProvider.OnInterstitialRequest += OnInterstitialRequest;
                adsProvider.OnInterstitialRequestFailed += OnInterstitialFailed;
                adsProvider.OnInterstitialRequestSuccess += OnInterstitialRequestSuccess;
                adsProvider.OnInterstitialShowSuccess += OnInterstitialShowSuccess;
                adsProvider.OnInterstitialShowFailed += OnInterstitialShowFailed;
                adsProvider.OnInterstitialClosed += OnInterstitialClosed;
                adsProvider.OnInterstitialRewareded += OnInterstitialRewareded;
                adsProvider.OnInterstitialClicked += OnInterstitialClicked;


                // Reward video
                adsProvider.OnRewardVideoOpened += OnRewardVideoOpened;
                adsProvider.OnRewardVideoClosed += OnRewardVideoClosed;
                adsProvider.OnRewardedVideoAvailabilityChanged += OnRewardedVideoAvailabilityChanged;
                adsProvider.OnRewardedVideoAdStarted += OnRewardedVideoAdStarted;
                adsProvider.OnRewardedVideoAdEnded += OnRewardedVideoAdEnded;
                adsProvider.OnRewardedVideoAdRewarded += OnRewardVideoWatched;
                adsProvider.OnRewardedVideoAdShowFailed += OnRewardedVideoAdShowFailed;
                adsProvider.OnRewardedVideoAdShowNotReady += EventVideoAds_Show_NotReady;
                adsProvider.OnRewardedVideoAdClicked += OnRewardVideoClicked;



                // Banner
                adsProvider.OnBannerOpen += OnBannerOpen;
                adsProvider.OnBannerWatched += OnBannerWatched;
                adsProvider.OnBannerAdLoaded += OnBannerAdLoaded;
                adsProvider.OnBannerAdClicked += OnBannerAdClicked;
                adsProvider.OnBannerAdLoadFailed += OnBannerAdLoadFailed;
                adsProvider.OnBannerAdLeftApplication += OnBannerAdLeftAppApplication;
                adsProvider.OnBannerAdScreenDismissed += OnBannerAdScreenDismissed;
                adsProvider.OnBannerAdScreenPresented += OnBannerAdScreenPresented;

                StartCoroutine(adsProvider.Initialize(keyIronsource));
            }

        }

        public void TestInterstitial()
        {
            if (interstitialProviders.Count <= 0)
            {
                Development.Log("Ads - There is no interstitial provider");
                return;
            }
            iInterstitalProvider = 0;
            interstitialProviders[0].ShowInterstitial();
        }

        public void SkipNextInterstitialTrigger()
        {
            skipNextInterstitial = true;
        }

        bool IsInGamePlayScene()
        {
            return false;
        }

        bool IsAbleToShowInterstitial()
        {
            return true;
            //return PlayerDataService.instance.GetPlayerData().VipUser == false;
        }

        public void ShowInterstitial(Action successCallback = null, Action failCallback = null, ClickShowAdsType clickShowAdsType = ClickShowAdsType.IntertitialAds)
        {
            Development.Log("Ads - Trigger interstitial");

            EventFullAds_Show_Ready();
            this.clickShowAdsType = clickShowAdsType;

            onIntertitialFailed = failCallback;
            onIntertitialWatched = successCallback;

#if !UNITY_EDITOR


            indexIntertitialAdsRetry = 0;
            StopTimeOutIntertitialCoroutine();

            StartTimeOutIntertitialCoroutine();
            if (interstitialProviders.Count <= 0)
            {
                Development.Log("Ads - There is no interstitial provider");
                return;
            }
            EventFullAds_Click();
            EventFullAds_Request();
            iInterstitalProvider = 0;
            interstitialProviders[0].ShowInterstitial();

            EventFullAds_User_All_Show();
#else
            OnInterstitialClosed();
            if (successCallback != null)
            {
                successCallback();
            }

#endif



        }

        private int indexIntertitialAdsRetry = 0;
        private IEnumerator TimeOutIntertitial()
        {
            yield return new WaitForSeconds(3);

            try
            {
                indexIntertitialAdsRetry++;

                iInterstitalProvider = 0;
                interstitialProviders[0].ShowInterstitial();


                if (indexIntertitialAdsRetry <= NumberRetryIntertitial)
                {
                    StartTimeOutIntertitialCoroutine();
                    yield break;
                }

                EventFullAds_Show_NotReady();

                IsOpeningIntertitial = false;
                indexIntertitialAdsRetry = 0;
                StopTimeOutIntertitialCoroutine();

                if (onIntertitialFailed != null)
                {
                    onIntertitialFailed();
                }
            }
            catch (Exception e)
            {
                onIntertitialFailed.Invoke();
            }

        }

        IEnumerator TimeOutIntertitialAds(float timeDelay)
        {
            yield return new WaitForSeconds(timeDelay);
            if (onIntertitialFailed != null)
            {
                onIntertitialFailed();
                EventFullAds_Show_NotReady();
            }
        }

        public static bool CanShowInterstitial()
        {
            return !AdHelper.Instance.IsInGamePlayScene();
        }

        public bool RewardVideoAvailable
        {
            get
            {
#if !UNITY_EDITOR
                foreach (var provider in rewardVideoProviders)
                {
                    if (provider.IsRewardVideoAvailable())
                    {
                        return true;
                    }
                }
                return false;
#else
                return true;
#endif
            }
        }


        public void ShowRewardVideo(
            RewardType type = RewardType.Ruby,
            Action onWatched = null,
            Action onFailed = null)
        {
            this.clickShowAdsType = ClickShowAdsType.RewardVideoAds;
            EventVideoAds_Show_Ready();


            if (openingRewardVideo)
            {
                return;
            }

            onRewardVideoFailed = onFailed;
            onRewardVideoWatched = onWatched;
            typeReward = type;
#if UNITY_EDITOR
            openingRewardVideo = true;
            OnRewardVideoWatched();

#else
            if (rewardVideoProviders.Count <= 0)
            {
                Development.Log("Ads - There is no rewarded video provider");
                onFailed.Invoke();
                return;
            }

            if (openingRewardVideo)
            {
                Development.Log("Ads - Rewarded video is already being loaded");
                return;
            }

            EventVideoAds_Request();
            openingRewardVideo = true;
            ShowVideoRewardAdsProgress();



            StopTimeOutRewardAdsCoroutine();
            StartTimeOutRewardAdsCoroutine(indexTimeout);


#endif

        }

        public bool IsBannerReady()
        {
            return true;
        }

        private bool isShowBanner = false;

        public void ShowBannerAds(
            RewardType type = RewardType.Ruby,
            AdsBannerPosition position = AdsBannerPosition.BOTTOM,
            Action onWatched = null,
            Action onFailed = null)
        {

            onBannerFailed = onFailed;
            onBannerWatched = onWatched;

            iRewardVideoProvider = 0;

            AdsBannerSize size = new AdsBannerSize(350, 100);
            rewardVideoProviders[0].ShowbannerAds(size, position);

        }

        public void ShowBannerAdsPlacement(
          RewardType type = RewardType.Ruby,
          AdsBannerPosition position = AdsBannerPosition.BOTTOM, string placement = "",
          Action onWatched = null,
          Action onFailed = null)
        {
            onBannerFailed = onFailed;
            onBannerWatched = onWatched;

            iRewardVideoProvider = 0;

            AdsBannerSize size = new AdsBannerSize(350, 100);
            rewardVideoProviders[0].ShowBannerAdsPlacement(size, position, placement);

        }

        public void HideBannerAds()
        {
            rewardVideoProviders[0].HideBannerAds();
        }

        public void DestroyBannerAds()
        {

        }

        /************* Intertitial Function *************/

        private void OnAdsProviderInitialized()
        {
            nInitializedAdsProviders++;

            if (nInitializedAdsProviders < adsProvidersMap.Keys.Count)
            {
                return;
            }

            foreach (string name in adsProviders)
            {
                if (!adsProvidersMap.ContainsKey(name))
                {
                    Development.Log("Ads - Ads provider " + name + " won't be used");
                    continue;
                }

                AdsProvider provider = adsProvidersMap[name];
                if (provider.IsInterstitialReady())
                {
                    interstitialProviders.Add(provider);
                }
                if (provider.IsRewardVideoReady())
                {
                    rewardVideoProviders.Add(provider);
                }
            }
        }

        void OnInterstitialRequest()
        {
            Development.Log("Ads - OnInterstitialRequest");
            EventFullAds_Request_Fail("Request fail");
        }


        private void OnInterstitialFailed()
        {
            Development.Log("Ads - Cannot open interstitial from ");

            indexTimeout = 0;
            IsOpeningIntertitial = false;
            iInterstitalProvider++;

            StopTimeOutRewardAdsCoroutine();
            int sizeProvider = interstitialProviders.Count;
            if (iInterstitalProvider > sizeProvider)
            {
                if (onIntertitialFailed != null)
                    onIntertitialFailed();
                return;
            }
            if (iInterstitalProvider < sizeProvider && interstitialProviders[iInterstitalProvider] != null)
            { // Try with another provider
                Development.Log("Ads - Try to open interstitial from " + interstitialProviders[iInterstitalProvider].Name());
                interstitialProviders[iInterstitalProvider].ShowInterstitial();
            }
            else
            { // Completely failed with all providers
                Development.Log("Ads - Opening interstitial completely failed");
            }

            if (onIntertitialFailed != null)
            {
                onIntertitialFailed();
                EventFullAds_Show_NotReady();
            }
        }

        void OnInterstitialRequestSuccess()
        {
            Development.Log("Ads - OnInterstitialRequestSuccess");
            EventFullAds_Request_Success();
        }

        private void OnInterstitialClosed()
        {
            Development.Log("Ads - OnInterstitialClosed");
            StopTimeOutIntertitialCoroutine();
            interstitialShownTime = Time.realtimeSinceStartup;

            if (iInterstitalProvider >= 0 && iInterstitalProvider < interstitialProviders.Count)
            {
                string providerName = interstitialProviders[iInterstitalProvider].Name();
                EventFullAds_Finish(this.clickShowAdsType);

            }

            if (onIntertitialWatched != null)
            {
                onIntertitialWatched();
            }

        }

        void OnInterstitialRewareded()
        {
            Development.Log("Ads - OnInterstitialRewareded");
            IsOpeningIntertitial = false;
        }

        void OnInterstitialClicked()
        {
            Development.Log("Ads - OnInterstitialClicked");
            EventFullAds_Click();
        }

        public void OnInterstitialShowSuccess()
        {
            Development.Log("Ads - OnInterstitialShowSuccess");
            StopTimeOutIntertitialCoroutine();
            IsOpeningIntertitial = false;

            EventFullAds_Show(this.clickShowAdsType);
        }

        public void OnInterstitialShowFailed()
        {

            Development.Log("Ads - OnInterstitialShowFailed");
            StopTimeOutIntertitialCoroutine();
            IsOpeningIntertitial = false;
            EventFullAds_Show_Failed();
        }
        /************* RewardedVideo Function *************/

        void OnRewardVideoOpened()
        {
            Development.Log("Ads - OnRewardVideoOpened");
            openingRewardVideo = true;
            StopTimeOutRewardAdsCoroutine();
            EventVideoAds_Show(this.typeReward, this.clickShowAdsType);
        }

        private void OnRewardVideoClosed()
        {
            Development.Log("Ads - OnRewardVideoClosed");
            StopTimeOutRewardAdsCoroutine();
            if (onRewardVideoWatched != null)
            {
                onRewardVideoWatched();
                onRewardVideoWatched = null;
            }

            openingRewardVideo = false;

        }

        void OnRewardedVideoAvailabilityChanged()
        {
            Development.Log("Ads - OnRewardedVideoAvailabilityChanged");
        }

        void OnRewardedVideoAdStarted()
        {
            Development.Log("Ads - OnRewardedVideoAdStarted");
            EventVideoAds_Show(this.typeReward, this.clickShowAdsType);
        }

        void OnRewardedVideoAdEnded()
        {
            Development.Log("Ads - OnRewardedVideoAdEnded");
            StopTimeOutRewardAdsCoroutine();
            EventVideoAds_End();
        }

        /// <summary>
        /// Add Ruby reward for user when finish watching video ad
        /// </summary>
        private void OnRewardVideoWatched()
        {
            Development.Log("Ads - on reward video watched " + rewardVideoProviders[iRewardVideoProvider].Name());
            StopTimeOutRewardAdsCoroutine();
            if (!openingRewardVideo)
            {
                return;
            }

            openingRewardVideo = false;


#if !UNITY_EDITOR
            string providerName = rewardVideoProviders[iRewardVideoProvider].Name();

#endif

            skipNextInterstitial = true;

            if (onRewardVideoWatched != null)
            {
                onRewardVideoWatched();
                onRewardVideoWatched = null;
            }

            // analytics
            EventVideoAds_Finish(this.typeReward, this.clickShowAdsType);
        }

        private void OnRewardedVideoAdShowFailed()
        {
            Development.Log("Ads - Cannot open rewarded video from " + rewardVideoProviders[iRewardVideoProvider].Name());
            iRewardVideoProvider++;


            if (iRewardVideoProvider < rewardVideoProviders.Count)
            { // Try with another provider
                Development.Log("Ads - Try to open rewarded video from " + rewardVideoProviders[iRewardVideoProvider].Name());
                rewardVideoProviders[iRewardVideoProvider].ShowRewardVideo();
                return;
            }

            if (!openingRewardVideo)
            {
                return;
            }
            EventVideoAds_Show_Failed();
            // show intertitial ads
            //CheckShowIntertitial();

            if (onRewardVideoFailed != null)
            {
                onRewardVideoFailed();
                onRewardVideoFailed = null;
            }

            openingRewardVideo = false;



        }

        void CheckShowIntertitial()
        {
            foreach (var provider in interstitialProviders)
            {
                // if have intertitial ads
                if (provider.IsInterstitialReady())
                {
                    ShowInterstitial(onRewardVideoWatched, onRewardVideoFailed, ClickShowAdsType.NoRewardCallIntertitial);
                }
                else // no intertitial ads
                {
                    StopCoroutine("TimeLoadingRewardAds");
                    StartCoroutine(TimeLoadingRewardAds());
                }
            }
        }

        private IEnumerator TimeLoadingRewardAds()
        {
            yield return new WaitForSeconds(10);
            Development.Log("Ads - Loading rewarded video time out 20 ");

            if (openingRewardVideo)
            {
                yield break;
            }
            ShowVideoRewardAdsProgress();
        }

        private IEnumerator TimeOutVideoReward(int countIndex)
        {
            //indexTimeout = countIndex;
            yield return new WaitForSeconds(3);
            //if (indexTimeout != countIndex)
            //{
            //    yield break;
            //}

            Development.Log("Ads - Loading rewarded video time out " + openingRewardVideo + " isOpen " + openingRewardVideo + " callback " + onRewardVideoFailed);

            if (!openingRewardVideo)
            {
                yield break;
            }

            if (onRewardVideoFailed != null)
            {
                onRewardVideoFailed();
                EventVideoAds_Request_Failed();

                // show intertitial ads
                //ShowInterstitial(onRewardVideoWatched, onRewardVideoFailed, ClickShowAdsType.NoRewardCallIntertitial);
            }

            openingRewardVideo = false;

        }

        private void ShowVideoRewardAdsProgress()
        {
            EventVideoAds_Click(this.typeReward);
#if UNITY_EDITOR
            Development.Log("Ads - video-reward-ad-progress can not run in editor");
            openingRewardVideo = false;
#else
            iRewardVideoProvider = 0;
            rewardVideoProviders[0].ShowRewardVideo();
#endif
        }

        public void OnRewardVideoClicked()
        {
            Development.Log("Ads - OnRewardVideoClicked");
            EventVideoAds_Click(this.typeReward);
        }

        /************* Banner Function *************/

        void OnBannerOpen()
        {

        }

        void OnBannerWatched()
        {
            if (onBannerWatched != null)
            {
                onBannerWatched();
            }
        }

        void OnBannerAdLoaded()
        {
            EventBannerAds_Show();
        }

        void OnBannerAdClicked()
        {
            Development.Log("Ads - OnBannerAdClicked");
            EventBannerAds_Click();
        }

        void OnBannerAdLoadFailed()
        {
            if (onBannerFailed != null)
            {
                onBannerFailed();
            }
        }

        void OnBannerAdLeftAppApplication()
        {

        }

        void OnBannerAdScreenDismissed()
        {

        }

        void OnBannerAdScreenPresented()
        {

        }


        void StartTimeOutRewardAdsCoroutine(int countIndex)
        {
            StopTimeOutRewardAdsCoroutine();
            TimeOutRewardAdsCoroutine = TimeOutVideoReward(countIndex);
            StartCoroutine(TimeOutRewardAdsCoroutine);
        }

        void StopTimeOutRewardAdsCoroutine()
        {
            if (TimeOutRewardAdsCoroutine != null)
            {
                StopCoroutine(TimeOutRewardAdsCoroutine);
            }

            TimeOutRewardAdsCoroutine = null;
        }


        void StartTimeOutIntertitialCoroutine()
        {
            StopTimeOutIntertitialCoroutine();
            TimeOutIntertitialCoroutine = TimeOutIntertitial();
            StartCoroutine(TimeOutIntertitialCoroutine);
        }

        void StopTimeOutIntertitialCoroutine()
        {
            if (TimeOutIntertitialCoroutine != null)
            {
                StopCoroutine(TimeOutIntertitialCoroutine);
            }

            TimeOutIntertitialCoroutine = null;
        }
        /************* Analytic Event *************/
        #region Event Analytic

        private Dictionary<string, object> allParamsDics = new Dictionary<string, object>(3);
        public void EventVideoAds_Show(RewardType rewardType, ClickShowAdsType clickShowAdsType)
        {
            allParamsDics.Clear();
            allParamsDics.Add("reward", ConvertTypeReward(rewardType));
            allParamsDics.Add("ClickType", ConvertTypeClick(clickShowAdsType));

            AnalyticService.Instance.LogEvent(AnalyticsEventConst.VideoAds_Show, allParamsDics);
            //tracker
            UserPropertiesTracker.Instance.AddRewardVideoShowCount();
        }

        public void EventVideoAds_Finish(RewardType rewardType, ClickShowAdsType clickShowAdsType)
        {
            allParamsDics.Clear();
            allParamsDics.Add("reward", ConvertTypeReward(rewardType));
            allParamsDics.Add("ClickType", ConvertTypeClick(clickShowAdsType));

            AnalyticService.Instance.LogEvent(AnalyticsEventConst.VideoAds_Finish, allParamsDics);

            //tracker
            UserPropertiesTracker.Instance.AddRewardVideoFinishCount();
        }

        public void EventVideoAds_Click(RewardType rewardType)
        {

            AnalyticService.Instance.LogEvent(AnalyticsEventConst.VideoAds_Click, new Dictionary<string, object> { { "reward", ConvertTypeReward(rewardType) } });

            //tracker
            UserPropertiesTracker.Instance.AddRewardVideoClickCount();
        }

        public void EventFullAds_Show(ClickShowAdsType clickShowAdsType)
        {
            AnalyticService.Instance.LogEvent(AnalyticsEventConst.FullAds_Show, new Dictionary<string, object> { { "ClickType", ConvertTypeClick(clickShowAdsType) } });
            //tracker
            UserPropertiesTracker.Instance.AddInterstitialShowCount();
        }

        public void EventFullAds_Finish(ClickShowAdsType clickShowAdsType)
        {
            AnalyticService.Instance.LogEvent(AnalyticsEventConst.FullAds_Finish, new Dictionary<string, object> { { "ClickType", ConvertTypeClick(clickShowAdsType) } });
            //tracker
            UserPropertiesTracker.Instance.AddInterstitialFinishCount();
        }

        public void EventFullAds_Click()
        {
            AnalyticService.Instance.LogEvent(AnalyticsEventConst.FullAds_Click);
            //tracker
            UserPropertiesTracker.Instance.AddInterstitialClickCount();
        }


        public void EventFullAds_Request()
        {
            AnalyticService.Instance.LogEvent(AnalyticsEventConst.FullAds_Request);
        }

        public void EventFullAds_Request_Success()
        {
            AnalyticService.Instance.LogEvent(AnalyticsEventConst.FullAds_Request_Success);
        }

        public void EventFullAds_Request_Fail(string error)
        {
            AnalyticService.Instance.LogEvent(AnalyticsEventConst.FullAds_Request_Failed, new Dictionary<string, object> { { "Error", error } });
        }

        public void EventFullAds_User_All_Show()
        {
            AnalyticService.Instance.LogEvent(AnalyticsEventConst.fullads_user_all_show);
        }

        public void EventFullAds_Show_Ready()
        {
            AnalyticService.Instance.LogEvent(AnalyticsEventConst.FullAds_Show_Ready);
        }

        public void EventFullAds_Show_NotReady()
        {
            AnalyticService.Instance.LogEvent(AnalyticsEventConst.FullAds_Show_NotReady);
        }

        public void EventFullAds_Show_Failed()
        {
            AnalyticService.Instance.LogEvent(AnalyticsEventConst.FullAds_Show_Failed);
        }

        public void EventBannerAds_Show()
        {
            AnalyticService.Instance.LogEvent(AnalyticsEventConst.bannerads_show);
            //tracker
            UserPropertiesTracker.Instance.AddBannerAdsShowCount();
        }

        public void EventBannerAds_Click()
        {
            AnalyticService.Instance.LogEvent(AnalyticsEventConst.bannerads_click);
            //tracker
            UserPropertiesTracker.Instance.AddBannerAdsClickCount();
        }



        public void EventVideoAds_Request()
        {
            AnalyticService.Instance.LogEvent(AnalyticsEventConst.VideoAds_Request);
        }

        public void EventVideoAds_Show_Ready()
        {
            AnalyticService.Instance.LogEvent(AnalyticsEventConst.VideoAds_Show_Ready);
        }

        public void EventVideoAds_Request_Failed()
        {
            AnalyticService.Instance.LogEvent(AnalyticsEventConst.VideoAds_Request_Failed);
        }

        public void EventVideoAds_Show_Failed()
        {
            AnalyticService.Instance.LogEvent(AnalyticsEventConst.VideoAds_Show_Failed);
        }

        public void EventVideoAds_Show_NotReady()
        {
            AnalyticService.Instance.LogEvent(AnalyticsEventConst.VideoAds_Show_NotReady);
        }

        public void EventVideoAds_End()
        {
            AnalyticService.Instance.LogEvent(AnalyticsEventConst.VideoAds_End);
        }



        public string ConvertTypeReward(RewardType rewardType)
        {
            string reward = "";

            switch (rewardType)
            {
                case RewardType.Ruby:
                    reward = "Ruby";
                    break;
                case RewardType.UnlockSong:
                    reward = "UnlockSong";
                    break;
                case RewardType.Continue:
                    reward = "Continue";
                    break;
            }

            return reward;
        }

        public string ConvertTypeClick(ClickShowAdsType clickShowAdsType)
        {
            string clickType = "";

            switch (clickShowAdsType)
            {
                case ClickShowAdsType.IntertitialAds:
                    clickType = "Intertitial";
                    break;
                case ClickShowAdsType.RewardVideoAds:
                    clickType = "RewardAds";
                    break;
                case ClickShowAdsType.NoRewardCallIntertitial:
                    clickType = "NoRewardCallIntertitial";
                    break;

            }
            return clickType;
        }

        #endregion

    }//end class


}//end namespace
