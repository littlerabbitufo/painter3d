using Amanotes.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class AdsIronSource : AdsProvider
{
    public const string NAME = "IronSource";

    private string APP_KEY = ""; //infinity rush ironsource appkey //temporary use
    const int MAX_INTERSTITIAL_ATTEMPTS = 3;

    public bool initialized = false;
    int nInterstitialAttempts = 0;
    public bool showingInterstitial = false;
    bool requestingInterstitial = false;
    bool showinRewardVideo = false;
    bool requestingRewardVideo = false;
    bool showingBanner = false;
    bool requestingBanner = false;

    public override IEnumerator Initialize(string keyIronsource = "", UserPropertiesTracker userPropertiesTracker = null)
    {
        //set appkey
        APP_KEY = keyIronsource;
        Development.Log("Ads - Initializing IronSource:" + APP_KEY, LogType.Warning);
        initialized = false;
        try
        {
            //Dynamic config example
            IronSourceConfig.Instance.setClientSideCallbacks(true);

            string id = IronSource.Agent.getAdvertiserId();
            Development.Log("Ads - IronSource Advertiser Id : " + id);

            Development.Log("Ads - IronSource validateIntegration");
            IronSource.Agent.validateIntegration();

            Development.Log("Ads - IronSource: Unity version " + IronSource.unityVersion());

            //SDK init
            IronSource.Agent.setUserId(id);

            InitInterstitialAds();

             InitRewardVideoAds();
            if (GameInitialManager.instance.InitSettingData.is_banner_active)
            {
                InitBannerAds();
            }

            // if (!GameInitialManager.instance.InitSettingData.is_intertitial_reward_active)
            // {
            //     InitRewardVideoAds();
            // }
            // else
            // {
            //     Debug.Log("Ads init notthing");
            // }
            OnEnabled();
            initialized = true;

            if (userPropertiesTracker != null)
            {
                //tracker
                UserPropertiesTracker.Instance.SetUID(id);
            }

        }
        catch (System.Exception)
        {
        }
        finally
        {
            OnInitialized();
        }

        yield return new WaitForSeconds(1);
        try
        {
            RequestInterstial();
        }
        catch { }

        // if (GameInitialManager.instance.InitSettingData.is_intertitial_reward_active)
        // {
        //     yield return new WaitForSeconds(1);
        //     InitRewardVideoAds();
        // }
        yield break;
    }


    public override void OnApplicationPaused(bool isPaused)
    {
        try
        {
            IronSource.Agent.onApplicationPause(isPaused);
        }
        catch (System.Exception) { }
    }

    public override void OnEnabled()
    {
        try
        {
        }
        catch (System.Exception) { }
    }

    public override bool IsInterstitialReady()
    {
        return initialized;
    }

    public override bool IsRewardVideoReady()
    {
        return initialized;
    }

    public override bool IsRewardVideoAvailable()
    {
        return IronSource.Agent.isRewardedVideoAvailable();
    }

    public override string Name()
    {
        return NAME;
    }

    public override void ShowInterstitial()
    {
        if (showingInterstitial || requestingInterstitial)
        {
            return;
        }

        if (IronSource.Agent.isInterstitialReady())
        {
            showingInterstitial = false;
            ShowReadyInterstitial();
        }
        else
        {
            RequestInterstial(true);
        }
    }

    public override void ShowRewardVideo()
    {
        if (IronSource.Agent.isRewardedVideoAvailable())
        {
            try
            {
                IronSource.Agent.showRewardedVideo();
            }
            catch (System.Exception)
            {
                OnRewardedVideoAdShowNotReady();
            }
        }
        else
        {
            OnRewardedVideoAdShowNotReady();
        }
    }

    public override bool IsBannerReady()
    {
        throw new System.NotImplementedException();
    }

    public override void ShowbannerAds(AdsBannerSize size, AdsBannerPosition position)
    {
        if (AdHelper.Instance.IsBannerReady())
        {
            try
            {
                Development.Log("[AdsIronSource] show banner");
                IronSourceBannerSize bannerSize = new IronSourceBannerSize(size.Width, size.Height);
                IronSourceBannerPosition bannerPosition = IronSourceBannerPosition.BOTTOM;
                switch (position)
                {
                    case AdsBannerPosition.TOP:
                        bannerPosition = IronSourceBannerPosition.TOP;
                        break;
                    case AdsBannerPosition.BOTTOM:
                        bannerPosition = IronSourceBannerPosition.BOTTOM;
                        break;
                }
                IronSource.Agent.loadBanner(bannerSize, bannerPosition);
            }
            catch
            {
                OnBannerAdLoadFailed();
            }
        }
        else
        {
            OnBannerAdLoadFailed();
        }
    }

    public override void ShowBannerAdsPlacement(AdsBannerSize size, AdsBannerPosition position, string placementName)
    {
        if (AdHelper.Instance.IsBannerReady())
        {
            try
            {
                Development.Log("[AdsIronSource] show banner");
                IronSourceBannerSize bannerSize = new IronSourceBannerSize(size.Width, size.Height);
                IronSourceBannerPosition bannerPosition = IronSourceBannerPosition.BOTTOM;
                switch (position)
                {
                    case AdsBannerPosition.TOP:
                        bannerPosition = IronSourceBannerPosition.TOP;
                        break;
                    case AdsBannerPosition.BOTTOM:
                        bannerPosition = IronSourceBannerPosition.BOTTOM;
                        break;
                }

                IronSource.Agent.loadBanner(bannerSize, bannerPosition, placementName);
            }
            catch
            {
                OnBannerAdLoadFailed();
            }
        }
        else
        {
            OnBannerAdLoadFailed();
        }
    }

    public override void HideBannerAds()
    {
        IronSource.Agent.hideBanner();
    }

    public override void DestroyBannerAds()
    {
        IronSource.Agent.destroyBanner();
    }

    private void ShowReadyInterstitial()
    {
        if (!AdHelper.CanShowInterstitial())
        {
            OnInterstitialRequestFailed();
            return;
        }

        showingInterstitial = false;
        try
        {
            IronSource.Agent.showInterstitial();
            OnInterstitialClicked();
        }
        catch (System.Exception)
        {
            OnInterstitialRequestFailed();
        }
    }


}
