﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Amanotes.Utils;

/// Control for Reward Video Behavior of Ironsrc
public partial class AdsIronSource : AdsProvider
{
    #region local variable
    private bool isInitRewardVideoAds = false;
    #endregion

    #region Public Function
    private void RequestRewardVideo(bool thenShow = false)
    {
        showinRewardVideo = thenShow;
        requestingRewardVideo = true;
        IronSource.Agent.showRewardedVideo();
    }
    #endregion

    public void InitRewardVideoAds()
    {
        if (!isInitRewardVideoAds)
        {
            isInitRewardVideoAds = true;

            //For Interstitial
            IronSource.Agent.init(APP_KEY, IronSourceAdUnits.REWARDED_VIDEO);

            //registercallback
            IronSourceEvents.onRewardedVideoAdOpenedEvent += RewardedVideoAdOpenedEvent;
            IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent;
            IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += RewardedVideoAvailabilityChangedEvent;
            IronSourceEvents.onRewardedVideoAdStartedEvent += RewardedVideoAdStartedEvent;
            IronSourceEvents.onRewardedVideoAdEndedEvent += RewardedVideoAdEndedEvent;
            IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
            IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;
            IronSourceEvents.onRewardedVideoAdClickedEvent += RewardedVideoAdClickedEvent;

        }
    }

    /************* RewardedVideo Event *************/ 
    void RewardedVideoAdOpenedEvent()
    {
        Development.Log("Ads - IronSource rewarded video opened");
        OnRewardVideoOpened();
    }

    void RewardedVideoAdClosedEvent()
    {
        Development.Log("Ads - IronSource rewarded video closed");
        OnRewardVideoClosed();
    }

    void RewardedVideoAvailabilityChangedEvent(bool canShowAd)
    {
        Development.Log("Ads - IronSource rewarded video ready = " + canShowAd);
        OnRewardedVideoAvailabilityChanged();
    }

    void RewardedVideoAdStartedEvent()
    {
        Development.Log("Ads - IronSource rewarded video started");
        OnRewardedVideoAdStarted();
    }

    void RewardedVideoAdEndedEvent()
    {
        Development.Log("Ads - IronSource rewarded video ended");
        OnRewardedVideoAdEnded();
    }

    void RewardedVideoAdRewardedEvent(IronSourcePlacement ssp)
    {
        Development.Log("Ads - IronSource rewarded video rewarded: amount = " + ssp.getRewardAmount() + " name = " + ssp.getRewardName());
        OnRewardedVideoAdRewarded();
    }

    void RewardedVideoAdShowFailedEvent(IronSourceError error)
    {
        Development.Log("Ads - IronSource rewarded video failed, code :  " + error.getCode() + ", description : " + error.getDescription());
        OnRewardedVideoAdShowFailed();
    }

    void RewardedVideoAdClickedEvent(IronSourcePlacement ssp)
    {
        Development.Log("Ads - IronSource rewarded video clicked");
        OnRewardedVideoAdClicked();
    }
}
