﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Amanotes.Utils;
using System;

/// Control for Interstial Behavior of Ironsrc
public partial class AdsIronSource : AdsProvider
{
	#region local variable
	private bool isInitInterstitialAds=false;
	#endregion

	#region Public Function
    public void RequestInterstial(bool thenShow = false)
    {
        showingInterstitial = thenShow;
        nInterstitialAttempts = 0;
        requestingInterstitial = true;
        IronSource.Agent.loadInterstitial();
    }
	#endregion
	
    public void InitInterstitialAds(){
		if(!isInitInterstitialAds){
			isInitInterstitialAds=true;
			//For Interstitial
			IronSource.Agent.init (APP_KEY, IronSourceAdUnits.INTERSTITIAL);

			//registercallback
			IronSourceEvents.onInterstitialAdReadyEvent += OnCallbackInterstitialAdReadyEvent; 
			IronSourceEvents.onInterstitialAdLoadFailedEvent += OnCallbackInterstitialAdLoadFailedEvent;
			IronSourceEvents.onInterstitialAdShowSucceededEvent += OnCallbackInterstitialAdShowSucceededEvent;
			IronSourceEvents.onInterstitialAdShowFailedEvent += OnCallbackInterstitialAdShowFailEvent;
			IronSourceEvents.onInterstitialAdClickedEvent += OnCallbackInterstitialAdClickedEvent;
			IronSourceEvents.onInterstitialAdOpenedEvent += OnCallbackInterstitialAdOpenedEvent;
			IronSourceEvents.onInterstitialAdClosedEvent += OnCallbackInterstitialAdClosedEvent;
			

			// Add Rewarded Interstitial Events
			IronSourceEvents.onInterstitialAdRewardedEvent += OnCallbackInterstitialAdRewardedEvent;
		}
	}

    /************* Intertitial Event *************/ 

	// request(load) ads successful -> this for event 
	void OnCallbackInterstitialAdReadyEvent() 
    {
        Development.Log("Ads - IronSource interstitial ready");
      
        requestingInterstitial = false;
        if (showingInterstitial)
        {
            ShowReadyInterstitial();
        }
		if(OnInterstitialRequestSuccess!=null){
			OnInterstitialRequestSuccess();
		}
    }

	//Invoked when the initialization process has failed.
	//@param description - string - contains information about the failure.
	void OnCallbackInterstitialAdLoadFailedEvent(IronSourceError error) 
    {
        Development.Log("Ads - IronSource interstitial not loaded, code: " + error.getCode() + ", description : " + error.getDescription());
        nInterstitialAttempts++;
        if (nInterstitialAttempts < MAX_INTERSTITIAL_ATTEMPTS)
        {
            IronSource.Agent.loadInterstitial();
        }
        else
        {
            requestingInterstitial = false;
            showingInterstitial = false;
        }

		if(OnInterstitialRequestFailed!=null){ // send event  
			OnInterstitialRequestFailed();
		}
    }

	//Invoked when the ad fails to show.
	//@param description - string - contains information about the failure.
    void OnCallbackInterstitialAdShowFailEvent(IronSourceError error)
    {
        Development.Log("Ads - IronSource interstitial not shown, code :  " + error.getCode() + ", description : " + error.getDescription());
        if(OnInterstitialShowFailed!=null)
		{
			OnInterstitialRequestFailed(); //noted tracking event in callback : fullads_showfailed
		}
		//request again
		RequestInterstial();
    }


	//Invoked right before the Interstitial screen is about to open.
	void OnCallbackInterstitialAdShowSucceededEvent()
    {
        Development.Log("Ads - IronSource interstitial shown");
		if(OnInterstitialShowSuccess!=null) //noted tracking event in callback : fullads_show
        {
			OnInterstitialShowSuccess();
		}
    }

	// Invoked when end user clicked on the interstitial ad
    void OnCallbackInterstitialAdClickedEvent()
    {
        Development.Log("Ads - IronSource interstitial clicked");
        if(OnInterstitialClicked!=null){ //noted tracking event in callback : fullads_click
			OnInterstitialClicked();
		}
    }
	
	//Invoked when the interstitial ad closed and the user goes back to the application screen.
	void OnCallbackInterstitialAdClosedEvent()
    {
        Development.Log("Ads - IronSource interstitial closed");
        if(OnInterstitialClosed!=null) //noted tracking event in callback : fullads_finish
		{
			OnInterstitialClosed();
		}
        RequestInterstial();
    }

	//Invoked when the Interstitial is Ready to shown after load function is called -> rewarded for interstitialads
    void OnCallbackInterstitialAdRewardedEvent()
    {
        Development.Log("Ads - IronSource interstitial rewarded");
		if(OnInterstitialRewareded!=null){
			OnInterstitialRewareded();
		}
    }

	
	void OnCallbackInterstitialAdOpenedEvent()
    {
        Development.Log("Ads - IronSource interstitial opened");
    }

}
