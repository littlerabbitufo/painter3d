﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Amanotes.Utils;

/// Control for Banner Behavior of Ironsrc
public partial class AdsIronSource : AdsProvider
{
    #region local variable
    private bool isIniBannerAds = false;
    private int positionBannerAds = 0;
    #endregion

    #region Public Function
    private void RequestBanner(bool thenShow = false)
    {
        showingBanner = thenShow;
        requestingBanner = true;
        IronSourceBannerSize size = new IronSourceBannerSize(350, 100);
        IronSourceBannerPosition position = IronSourceBannerPosition.BOTTOM;
        switch (positionBannerAds)
        {
            case 0:
                position = IronSourceBannerPosition.BOTTOM;
                break;
            case 1:
                position = IronSourceBannerPosition.TOP;
                break;
            default:
                position = IronSourceBannerPosition.BOTTOM;
                break;

        }
        IronSource.Agent.loadBanner(size, position);
    }
    #endregion

    public void InitBannerAds()
    {
        if (!GameInitialManager.instance.InitSettingData.is_banner_active)
            return;

        if (!isIniBannerAds)
        {
            isIniBannerAds = true;
            //For Interstitial
            IronSource.Agent.init(APP_KEY, IronSourceAdUnits.BANNER);

            //registercallback
            IronSourceEvents.onBannerAdLoadedEvent += OnBannerAdLoadedEvent;
            IronSourceEvents.onBannerAdClickedEvent += OnBannerAdClickEvent;
            IronSourceEvents.onBannerAdLeftApplicationEvent += OnBannerAdLeftApplicationEvent;
            IronSourceEvents.onBannerAdScreenDismissedEvent += OnBannerAdScreenDismissedEvent;
            IronSourceEvents.onBannerAdScreenPresentedEvent += OnBannerAdScreenPresentedEvent;
        }
    }

    /************* Banner Event *************/
    void OnBannerAdLoadedEvent()
    {
        Development.Log("Ads - IronSource banner loaded");
        OnBannerAdLoaded();
    }

    void OnBannerAdClickEvent()
    {
        Development.Log("Ads - IronSource banner click");
        OnBannerAdClicked();
    }

    void OnBannerAdLoadFailedEvent()
    {
        Development.Log("Ads - IronSource banner loaded fail");
        OnBannerAdLoadFailed();
    }

    void OnBannerAdLeftApplicationEvent()
    {
        Development.Log("Ads - IronSource banner left");
        OnBannerAdLeftApplication();
    }

    void OnBannerAdScreenDismissedEvent()
    {
        Development.Log("Ads - IronSource banner dissmiss");
        OnBannerAdScreenDismissed();
    }

    void OnBannerAdScreenPresentedEvent()
    {
        Development.Log("Ads - IronSource banner present");
        OnBannerAdScreenPresented();
    }
}
